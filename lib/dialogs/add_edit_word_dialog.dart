/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/word_list_model.dart';
import 'package:wortschatz/widgets/custom_icon_button.dart';

enum AddEditWordDialogResultAction {
  ActionSave,
  ActionSaveAndNew,
  ActionCancel,
}

class AddEditWordDialogResult {
  final AddEditWordDialogResultAction action;
  final WordEntry word;

  AddEditWordDialogResult(this.action, [this.word]);
}

class AddEditWordDialog extends StatefulWidget {
  final WordEntry word;

  AddEditWordDialog({this.word});

  @override
  State createState() => _AddEditWordDialogState();
}

class _AddEditWordDialogState extends State<AddEditWordDialog> {
  WordListModel listModel;

  TextEditingController controllerWord;
  TextEditingController controllerDescription;

  WordEntry word;
  bool wordValid = true;

  @override
  void initState() {
    super.initState();

    listModel = WordListModel(
        context, Config.wordsDatabase, null, columnId, SortOrder.ascending);

    word = widget.word;

    controllerWord = TextEditingController(text: word?.word);
    controllerDescription = TextEditingController(text: word?.description);

    controllerWord.addListener(() {
      final text = controllerWord.text.trim();
      _validateWord(text);
    });
  }

  @override
  Widget build(BuildContext context) {
    final String heading = word == null ? 'Neues Wort' : 'Wort bearbeiten';

    final FocusNode wordNode = FocusNode();
    final FocusNode descriptionNode = FocusNode();

    var saveAndNew;
    if (word == null) {
      saveAndNew = Padding(
        padding: EdgeInsets.only(top: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: SizedBox(
                width: Config.buttonWidth * 2 + 16,
                child: CustomIconButton(
                  onPressed: () => saveAction(
                        word,
                        controllerWord,
                        controllerDescription,
                        context,
                        AddEditWordDialogResultAction.ActionSaveAndNew,
                      ),
                  icon: Icons.plus_one,
                  child: Text('Speichern und weiteres Wort...'),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      // empty widget
      saveAndNew = Padding(padding: EdgeInsets.all(0));
    }

    return Stack(children: <Widget>[
      ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
              sigmaX: Config.blurSigma, sigmaY: Config.blurSigma),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(color: Colors.transparent),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Material(
                        color: Colors.transparent,
                        child: Text(
                          heading,
                          style: TextStyle(color: Colors.white, fontSize: 34.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 32, right: 32, top: 64),
                      child: Material(
                        color: Colors.transparent,
                        child: TextField(
                          controller: controllerWord,
                          focusNode: wordNode,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () {
                            wordNode.unfocus();
                            FocusScope.of(context)
                                .requestFocus(descriptionNode);
                          },
                          autofocus: true,
                          autocorrect: false,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                          decoration: InputDecoration(
                            fillColor: wordValid ? Colors.green : Colors.red,
                            filled: true,
                            prefixIcon: Icon(Icons.description),
                            hintText: 'Ein Wort eingeben...',
                            contentPadding: const EdgeInsets.all(16),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.0))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 32,
                        right: 32,
                        top: 16,
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: TextField(
                          controller: controllerDescription,
                          focusNode: descriptionNode,
                          textInputAction: TextInputAction.done,
                          onEditingComplete: () => saveAction(word,
                              controllerWord, controllerDescription, context),
                          autofocus: false,
                          autocorrect: true,
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                          ),
                          decoration: InputDecoration(
                            filled: true,
                            prefixIcon: Icon(Icons.description),
                            hintText: 'Eine Beschreibung eingeben...',
                            contentPadding: const EdgeInsets.all(16),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.0))),
                          ),
                        ),
                      ),
                    ),
                    saveAndNew,
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: SizedBox(
                              width: Config.buttonWidth,
                              child: CustomIconButton(
                                onPressed: () => saveAction(
                                    word,
                                    controllerWord,
                                    controllerDescription,
                                    context),
                                icon: Icons.check,
                                child: Text('Speichern'),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: SizedBox(
                              width: Config.buttonWidth,
                              child: CustomIconButton(
                                onPressed: () {
                                  Navigator.pop(
                                      context,
                                      AddEditWordDialogResult(
                                          AddEditWordDialogResultAction
                                              .ActionCancel));
                                },
                                icon: Icons.close,
                                child: Text('Abbrechen'),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      )
    ]);
  }

  void _validState() {
    setState(() {
      wordValid = true;
    });
  }

  void _invalidState() {
    setState(() {
      wordValid = false;
    });
  }

  Future<bool> _validateWord(String text) async {
    if (!await listModel.containsWord(text)) {
      _validState();
      return true;
    } else {
      _invalidState();
      return false;
    }
  }

  void saveAction(WordEntry source, TextEditingController controllerWord,
      TextEditingController controllerDescription, BuildContext context,
      [AddEditWordDialogResultAction result =
          AddEditWordDialogResultAction.ActionSave]) {
    final WordEntry word =
        source == null ? WordEntry.defaults() : WordEntry.from(source);

    word.word = controllerWord.text.trim();
    word.description = controllerDescription.text.trim();

    Navigator.pop(context, AddEditWordDialogResult(result, word));
  }
}

Future<AddEditWordDialogResult> showAddEditWordDialog(BuildContext context,
    {WordEntry word}) {
  return showDialog(
      context: context,
      builder: (context) {
        return AddEditWordDialog(word: word);
      });
}
