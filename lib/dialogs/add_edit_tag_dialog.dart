/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/widgets/custom_icon_button.dart';

enum AddEditTagDialogResultAction {
  ActionSave,
  ActionCancel,
}

class AddEditTagDialogResult {
  final AddEditTagDialogResultAction action;
  final TagEntry tag;

  AddEditTagDialogResult(this.action, [this.tag]);
}

/// Show a dialog that is suitable to enter a tag name and
/// an associated description
Future<AddEditTagDialogResult> showAddEditTagDialog(BuildContext context,
    {TagEntry tag}) async {
  final TextEditingController controllerTag =
      TextEditingController(text: tag?.tag);
  final TextEditingController controllerDescription =
      TextEditingController(text: tag?.description);

  final String heading =
      tag == null ? 'Neues Schlagwort' : 'Schlagwort bearbeiten';

  final FocusNode tagNode = FocusNode();
  final FocusNode descriptionNode = FocusNode();

  var result = await showDialog(
      context: context,
      builder: (context) {
        return ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(
                sigmaX: Config.blurSigma, sigmaY: Config.blurSigma),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(color: Colors.transparent),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Material(
                          color: Colors.transparent,
                          child: Text(
                            heading,
                            style:
                                TextStyle(color: Colors.white, fontSize: 34.0),
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 32, right: 32, top: 64),
                        child: Material(
                          color: Colors.transparent,
                          child: TextField(
                            controller: controllerTag,
                            focusNode: tagNode,
                            textInputAction: TextInputAction.next,
                            onEditingComplete: () {
                              tagNode.unfocus();
                              FocusScope.of(context)
                                  .requestFocus(descriptionNode);
                            },
                            autofocus: true,
                            autocorrect: false,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            decoration: InputDecoration(
                              filled: true,
                              prefixIcon: Icon(Icons.description),
                              hintText: 'Ein Schlagwort eingeben...',
                              contentPadding: const EdgeInsets.all(16),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0))),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 32,
                          right: 32,
                          top: 16,
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: TextField(
                            controller: controllerDescription,
                            focusNode: descriptionNode,
                            textInputAction: TextInputAction.done,
                            onEditingComplete: () => saveAction(
                                controllerTag, controllerDescription, context),
                            autofocus: false,
                            autocorrect: true,
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                            decoration: InputDecoration(
                              filled: true,
                              prefixIcon: Icon(Icons.description),
                              hintText: 'Eine Beschreibung eingeben...',
                              contentPadding: const EdgeInsets.all(16),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0))),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: SizedBox(
                                width: Config.buttonWidth,
                                child: CustomIconButton(
                                  onPressed: () => saveAction(controllerTag,
                                      controllerDescription, context),
                                  icon: Icons.check,
                                  child: Text('Speichern'),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: SizedBox(
                                width: Config.buttonWidth,
                                child: CustomIconButton(
                                  onPressed: () {
                                    Navigator.pop(
                                        context,
                                        Future.value(AddEditTagDialogResult(
                                            AddEditTagDialogResultAction
                                                .ActionCancel)));
                                  },
                                  icon: Icons.close,
                                  child: Text('Abbrechen'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });

  return result;
}

void saveAction(TextEditingController controllerTag,
    TextEditingController controllerDescription, BuildContext context) {
  final TagEntry tag = TagEntry(
    tag: controllerTag.text.trim(),
    description: controllerDescription.text.trim(),
    color: Colors.primaries[Random().nextInt(
      Colors.primaries.length,
    )],
    favorite: false,
  );

  Navigator.pop(
      context,
      Future.value(AddEditTagDialogResult(
          AddEditTagDialogResultAction.ActionSave, tag)));
}
