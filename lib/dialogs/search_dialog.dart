/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/custom_icon_button.dart';

enum SearchDialogResultAction {
  ActionSearch,
  ActionCancel,
}

class SearchDialogResult {
  final SearchDialogResultAction action;
  final String text;

  SearchDialogResult(this.action, [this.text]);
}

Future<SearchDialogResult> showSearchDialog(BuildContext context,
    {String text}) async {
  final TextEditingController controllerText =
      TextEditingController(text: text);

  final String heading = CustomLocalizations.of(context).filterWords;

  var result = await showDialog(
      context: context,
      builder: (context) {
        return ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(
                sigmaX: Config.blurSigma, sigmaY: Config.blurSigma),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(color: Colors.transparent),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Material(
                          color: Colors.transparent,
                          child: Text(
                            heading,
                            style:
                                TextStyle(color: Colors.white, fontSize: 34.0),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 32, right: 32, top: 64),
                        child: Material(
                          color: Colors.transparent,
                          child: TextField(
                            controller: controllerText,
                            textInputAction: TextInputAction.search,
                            onEditingComplete: () {
                              Navigator.pop(
                                  context,
                                  Future.value(SearchDialogResult(
                                      SearchDialogResultAction.ActionSearch,
                                      controllerText.text.trim())));
                            },
                            autofocus: true,
                            autocorrect: false,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            decoration: InputDecoration(
                              filled: true,
                              prefixIcon: Icon(Icons.search),
                              hintText: CustomLocalizations.of(context)
                                  .filterWordsHint,
                              contentPadding: EdgeInsets.all(16),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0))),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8),
                              child: SizedBox(
                                width: Config.buttonWidth,
                                child: CustomIconButton(
                                  onPressed: () {
                                    Navigator.pop(
                                        context,
                                        Future.value(SearchDialogResult(
                                            SearchDialogResultAction
                                                .ActionSearch,
                                            controllerText.text.trim())));
                                  },
                                  icon: Icons.check,
                                  child: Text(
                                      CustomLocalizations.of(context).filter),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8),
                              child: SizedBox(
                                width: Config.buttonWidth,
                                child: CustomIconButton(
                                  onPressed: () {
                                    Navigator.pop(
                                        context,
                                        Future.value(SearchDialogResult(
                                            SearchDialogResultAction
                                                .ActionCancel)));
                                  },
                                  icon: Icons.close,
                                  child: Text(MaterialLocalizations.of(context)
                                      .cancelButtonLabel),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });

  return result;
}
