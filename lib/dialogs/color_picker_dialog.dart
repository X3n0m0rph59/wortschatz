/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/custom_icon_button.dart';

enum ColorPickerResultAction {
  ActionSave,
  ActionCancel,
}

class ColorPickerResult {
  final ColorPickerResultAction action;
  final Color color;

  ColorPickerResult(this.action, [this.color]);
}

Future<ColorPickerResult> showColorPickerDialog(
    BuildContext context, Color initialColor) async {
  Color currentColor;

  final List<MaterialColor> colors = List<MaterialColor>.from(Colors.primaries);
  colors.add(Colors.grey);

  final String heading = CustomLocalizations.of(context).selectColorHeading;

  var result = await showDialog(
      context: context,
      builder: (context) {
        return ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(
                sigmaX: Config.blurSigma, sigmaY: Config.blurSigma),
            child: Material(
              color: Colors.transparent,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(color: Colors.transparent),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Material(
                        color: Colors.transparent,
                        child: Text(
                          heading,
                          style: TextStyle(color: Colors.white, fontSize: 34.0),
                        ),
                      ),
                    ),
                    MaterialColorPicker(
                      selectedColor: initialColor,
                      colors: colors,
                      onColorChange: (Color color) {
                        currentColor = color;

//                      Navigator.pop(
//                          context,
//                          Future.value(ColorPickerResult(
//                              ColorPickerResultAction.ActionSave,
//                              currentColor)));
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: SizedBox(
                              width: Config.buttonWidth,
                              child: CustomIconButton(
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  Navigator.pop(
                                      context,
                                      Future.value(ColorPickerResult(
                                          ColorPickerResultAction.ActionSave,
                                          currentColor)));
                                },
                                icon: Icons.check,
                                child:
                                    Text(CustomLocalizations.of(context).save),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: SizedBox(
                              width: Config.buttonWidth,
                              child: CustomIconButton(
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  Navigator.pop(
                                      context,
                                      Future.value(ColorPickerResult(
                                          ColorPickerResultAction
                                              .ActionCancel)));
                                },
                                icon: Icons.close,
                                child: Text(MaterialLocalizations.of(context)
                                    .cancelButtonLabel),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      });

  return result;
}
