/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/tag_list_model.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/util/edit_history.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/util/util.dart';

enum TagSelectionResultAction {
  ActionClose,
}

class TagSelectionResult {
  final TagSelectionResultAction action;
  final List<TagEntry> tags;

  TagSelectionResult(this.action, [this.tags]);
}

class TagSelectionDialog extends StatefulWidget {
  final WordEntry word;

  TagSelectionDialog({
    Key key,
    @required this.word,
  })  : assert(word != null),
        super(key: key);

  @override
  _TagSelectionDialogState createState() => _TagSelectionDialogState();
}

class _TagSelectionDialogState extends State<TagSelectionDialog> {
  BuildContext context;
  WordEntry word;

//  final GlobalKey<CustomAnimatedListState> _listKey =
//      GlobalKey<CustomAnimatedListState>();

  @override
  void initState() {
    super.initState();

    word = widget.word;

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    return Future.value();
  }

  @override
  Widget build(BuildContext context) {
    final tagList = _buildTagListView(word);

    return SimpleDialog(
      semanticLabel: CustomLocalizations.of(context).tags,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FutureBuilder<Widget>(
              future: tagList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.data != null) {
                  return snapshot.data;
                } else {
                  return Container(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ],
        ),
      ],
    );
  }

  Future<Widget> _buildTagListView(WordEntry word) async {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: await _buildTagsListItems(),
      ),
    );
  }

  Future<List<Widget>> _buildTagsListItems() async {
    final result = <Widget>[];

    final listModelTags = TagListModel(context, Config.wordsDatabase,
        null /*_listKey*/, columnId, SortOrder.ascending);

    final numTags = await listModelTags.length;
    final activeTags = await word.tags;

    final allTags = <TagEntry>[];
    for (int i = 0; i < numTags; i++) {
      allTags.add(await listModelTags[i]);
    }

    for (int i = 0; i < numTags; i++) {
      final TagEntry currentTag = allTags[i];
      final bool checked = isEntryContainedIn(currentTag, activeTags);

      result.add(
        CheckboxListTile(
          value: checked,
          onChanged: (value) async {
            final WordEntry modifiedWord = WordEntry.from(word);

            if (value) {
              (await modifiedWord.tags).add(currentTag);
            } else {
              (await modifiedWord.tags).remove(currentTag);
            }

            setState(() {
              if (value) {
                activeTags.add(currentTag);

                logWordTagAddAction('${word.word}: ${currentTag.tag}', word,
                    modifiedWord, context);
              } else {
                activeTags.removeAt(getIndexOfEntry(currentTag, activeTags));

                logWordTagRemoveAction('${word.word}: ${currentTag.tag}', word,
                    modifiedWord, context);
              }
            });
          },
          // secondary: Icon(Icons.bookmark_border),
          title: Text(
            '${currentTag.tag}',
            style: TextStyle(
              //fontStyle: FontStyle.italic,
              fontWeight:
                  currentTag.favorite ? FontWeight.bold : FontWeight.normal,
              fontSize: 14.0,
              color: currentTag.color,
            ),
          ),
          subtitle: Text(
            '${currentTag.description}',
            style: TextStyle(
              //fontStyle: FontStyle.italic,
              fontWeight:
                  currentTag.favorite ? FontWeight.bold : FontWeight.normal,
              fontSize: 12.0,
            ),
          ),
        ),
      );
    }

    return Future.value(result);
  }
}

Future<TagSelectionResult> showTagSelectionDialog(
    BuildContext context, WordEntry word) {
  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return TagSelectionDialog(word: word);
      });
}
