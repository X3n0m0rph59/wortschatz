/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/util/date_time.dart';
import 'package:wortschatz/util/l10n.dart';

enum TagPropertiesResultAction {
  ActionClose,
}

class TagPropertiesResult {
  final TagPropertiesResultAction action;
  final List<TagEntry> tags;

  TagPropertiesResult(this.action, [this.tags]);
}

class TagPropertiesDialog extends StatefulWidget {
  final TagEntry tag;

  TagPropertiesDialog({
    Key key,
    @required this.tag,
  })  : assert(tag != null),
        super(key: key);

  @override
  _TagPropertiesDialogState createState() => _TagPropertiesDialogState();
}

class _TagPropertiesDialogState extends State<TagPropertiesDialog> {
  TagEntry tag;

//  final GlobalKey<CustomAnimatedListState> _listKey =
//      GlobalKey<CustomAnimatedListState>();

  @override
  void initState() {
    super.initState();

    tag = widget.tag;

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    return Future.value();
  }

  @override
  Widget build(BuildContext context) {
    final tagList = _buildPropertiesView(tag);

    return SimpleDialog(
      semanticLabel: CustomLocalizations.of(context).properties,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FutureBuilder<Widget>(
              future: tagList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.data != null) {
                  return snapshot.data;
                } else {
                  return Container(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ],
        ),
      ],
    );
  }

  Future<Widget> _buildPropertiesView(TagEntry tag) async {
    try {
      final children = <Widget>[
        ListTile(
          title: Text(
            '${tag.tag}',
            style: TextStyle(
              //fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
              color: tag.color,
            ),
          ),
          subtitle: Text(
            '${tag.description}',
            style: TextStyle(
              //fontStyle: FontStyle.italic,
              fontWeight: tag.favorite ? FontWeight.bold : FontWeight.normal,
              fontSize: 12.0,
            ),
          ),
        ),
        ListTile(
          title: Text(
            'Daten',
            style: TextStyle(
              //fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
          subtitle: Text(
            '${CustomLocalizations.of(context).created} ${formatDateTime(tag.createdAt)},\n'
                '${CustomLocalizations.of(context).modified} ${formatDateTime(tag.modifiedAt)}',
            style: TextStyle(
              //fontStyle: FontStyle.italic,
              fontWeight: FontWeight.normal,
              fontSize: 12.0,
            ),
          ),
        ),
      ];

      return SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: children,
        ),
      );
    } catch (e, stacktrace) {
      print(stacktrace);
      print(e);

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[Text('Internal Error!')],
      );
    }
  }
}

Future<TagPropertiesResult> showTagPropertiesDialog(
    BuildContext context, TagEntry tag) {
  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return TagPropertiesDialog(tag: tag);
      });
}
