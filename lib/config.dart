/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

/// Global static configuration values for the app
class Config {
  /// The default UI mode; light or dark
  static const bool defaultDarkUi = true;

  /// The name of the default database profile
  static const String defaultDatabaseProfile = 'default';

  /// Filename of the primary database, default: words.db
  static const String wordsDatabase = 'words.db';
  static const String wordsDatabaseRestore = 'words-restore.db';

  /// Filename of the edit history database, default: history.db
  /// It contains a single table that stores the chronological
  /// edit history for the main database and the dictionary
  static const String editHistoryDatabase = 'history.db';
  static const String editHistoryDatabaseRestore = 'history-restore.db';

  /// Filename of the dictionary database, default: dictionary.db
  /// This database currently only holds a 'words' table,
  /// that gets shipped with the app.
  static const String dictionaryDatabase = 'dictionary.db';
  static const String dictionaryDatabaseRestore = 'dictionary-restore.db';

  /// Dark header image
  static const String assetsHeaderDark = 'assets/images/header_dark.jpg';

  /// Light header image
  static const String assetsHeaderLight = 'assets/images/header_light.jpg';

  /// Default height of MaterialButtons
  static const double defaultButtonHeight = 45.0;

  /// Height of the header-image displayed on each page
  static const double imageHeight = 170.0;

  /// Default height of the diagonal clipper
  static const double clipperHeight = 55.0;

  /// Default top-padding of the header text
  static const double headerPadding = 185.0;

  /// Default top-padding of the header text that is displayed on each page
  static const double simpleHeaderPadding = 20.0;

  /// Default top-padding of the animated list view that is
  /// present on most pages with listable items
  static const double listViewTopPadding = 100.0;

  /// Top-padding of the first list item in an animated list view
  static const double listViewTopInternalPadding = 155.0;

  /// Duration of animations (in milliseconds)
  static const int listAnimationDurationMsecs = 400;

  /// Movement factor of the page header when a list gets scrolled
  static const double headerPaddingFactor = 0.2725;

  /// Movement factor of the profile row when a list gets scrolled
  static const double profileRowPaddingFactor = 2.10;

  /// Number of data sets to show in a diagram on the statistics page
  static const int diagramNumDataSets = 4;

  /// Snack bar durations (short)
  static const int snackBarDurationShortSecs = 4;

  /// Snack bar durations (normal)
  static const int snackBarDurationSecs = 5;

  /// Snack bar durations (long)
  static const int snackBarDurationLongSecs = 6;

  /// Sigma value of background blur filter
  static const double blurSigma = 1.75;

  /// The width of buttons as used in dialog widgets
  static const double buttonWidth = 140.0;
}
