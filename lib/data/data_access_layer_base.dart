/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' show join;
import 'package:sqflite/sqflite.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/util/database_profiles.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/message_bar.dart';

final String dbNameWords = Config.wordsDatabase;
final String dbNameDictionary = Config.dictionaryDatabase;
final String dbNameEditHistory = Config.editHistoryDatabase;

final String columnId = '_id';
final String columnCreatedAt = 'createdAt';
final String columnModifiedAt = 'modifiedAt';

final String columnWordId = 'wordId';
final String columnTagId = 'tagId';

final String tableNameWords = 'words';
final String tableNameTags = 'tags';
final String tableNameWordsTags = 'words_tags';
final String tableNameEditHistory = 'edit_history';

final String columnWord = 'word';
final String columnDescription = 'description';
final String columnColor = 'color';
final String columnFavorite = 'favorite';
final String columnTag = 'tag';

final String columnAction = 'action';
final String columnPayload = 'payload';

enum Tables {
  words,
  tags,
  wordsTags,
  editHistory,
}

enum SortOrder {
  ascending,
  descending,
}

abstract class MappableEntry {
  int id;

  MappableEntry({this.id});

  Map<String, dynamic> toMap();

  MappableEntry.fromMap(Map<String, dynamic> map);

  String toString();
  String toDescription();
  String toJson();
}

abstract class DataAccessLayer<T extends MappableEntry> {
  final String databaseFilePath;
  final Tables table;
  final BuildContext context;

  String sortColumn;
  SortOrder sortOrder;

  Future<Database> database;

  String where, tagsWhere;
  List whereArgs, tagsWhereArgs;

  DataAccessLayer(this.databaseFilePath, this.table, this.context,
      this.sortColumn, this.sortOrder) {
    database = Future(() => open());
  }

  String get currentTableName;

  Future<Database> open() async {
    print('Opening database: $databaseFilePath');

    // await Permission.requestPermissions([PermissionName.Storage]);

    // print('Checking if we need to restore from a backup...');

//    final backupManager = BackupManager();
//    await backupManager.tryPerformPendingRestore();

    DatabaseProfileManager().createDatabaseProfileIfMissing();
    final profileName = await DatabaseProfileManager().currentDatabaseProfile;

    // copy the dictionary.db from assets first, if needed
    if (databaseFilePath.endsWith(dbNameDictionary)) {
      var databasesPath = join(await getDatabasesPath(), profileName);
      var path = join(databasesPath, dbNameDictionary);

      if (!File(path).existsSync()) {
        print('Creating new copy of the $dbNameDictionary from assets...');

        // copy from assets
        ByteData data = await rootBundle.load(join('assets', dbNameDictionary));
        List<int> bytes =
            data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

        await File(path).writeAsBytes(bytes);
      } else {
        print('Found existing database: $dbNameDictionary');
      }

      database = Future.value(
        await openDatabase(
          path,
          onOpen: (Database db) {
            print('Successfuly opened the database');
          },
        ),
      );
    } else if (databaseFilePath.endsWith(dbNameEditHistory)) {
      database = Future.value(
        await openDatabase(
          join(profileName, databaseFilePath),
          version: 1,
          onCreate: (Database db, int version) async {
            print('Creating schema version: $version');

            await db.transaction((txn) async {
              var batch = txn.batch();

              batch.execute('''
                              create table if not exists $tableNameEditHistory ( 
                                $columnId integer primary key autoincrement, 
                                $columnAction integer not null,
                                $columnDescription varchar(64),
                                $columnPayload text,
                                $columnCreatedAt timestamp,
                                $columnModifiedAt timestamp
                                )
                              ''');

              await batch.commit();
            });
          },
          onOpen: (Database db) {
            print('Successfuly opened the database');
          },
          onUpgrade: (Database db, int oldVersion, int newVersion) async {
            print('Upgrading schema version: $oldVersion -> $newVersion');

            if (oldVersion == 1 && newVersion == 2) {
              // never released, debug version only
              // Manual intervention needed
            } else {
              MessageBar.error(
                  title: CustomLocalizations.of(context).error,
                  message: CustomLocalizations.of(context).invalidDatabaseState)
                ..show(context);
            }
          },
        ),
      );
    } else {
      database = Future.value(
        await openDatabase(
          join(profileName, databaseFilePath),
          version: 5,
          onCreate: (Database db, int version) async {
            print('Creating schema version: $version');

            await db.transaction((txn) async {
              var batch = txn.batch();

              batch.execute('''
                              create table if not exists $tableNameWords ( 
                                $columnId integer primary key autoincrement, 
                                $columnWord varchar(64) not null,
                                $columnDescription varchar(64),
                                $columnColor integer not null,
                                $columnFavorite integer not null,
                                $columnCreatedAt timestamp,
                                $columnModifiedAt timestamp
                                )
                              ''');

              batch.execute('''
                              create table if not exists $tableNameTags ( 
                                $columnId integer primary key autoincrement, 
                                $columnTag varchar(64) not null,
                                $columnDescription varchar(64),
                                $columnColor integer not null,
                                $columnFavorite integer not null,
                                $columnCreatedAt timestamp,
                                $columnModifiedAt timestamp
                                )
                              ''');

              batch.execute('''
                              create table if not exists $tableNameWordsTags ( 
                                $columnId integer primary key autoincrement,
                                $columnWordId integer not null, 
                                $columnTagId integer not null,
                                $columnCreatedAt timestamp,
                                $columnModifiedAt timestamp,
                                foreign key ($columnWordId)
                                   references $tableNameWords($columnId)
                                   on update cascade
                                   on delete cascade,                                   
                                foreign key ($columnTagId)
                                   references $tableNameTags($columnId)
                                   on update cascade
                                   on delete cascade
                                )
                              ''');

              await batch.commit();
            });
          },
          onOpen: (Database db) {
            print('Successfuly opened the database');
          },
          onUpgrade: (Database db, int oldVersion, int newVersion) async {
            print('Upgrading schema version: $oldVersion -> $newVersion');

            if (oldVersion == 1 && newVersion == 2) {
              // never released, debug version only
              // Manual intervention needed
            } else if (oldVersion == 2 && newVersion == 3) {
              // never released, debug version only
              // Manual intervention needed
            } else if (oldVersion == 3 && newVersion == 4) {
              // never released, debug version only
              // Manual intervention needed
            } else if (oldVersion < 5 && newVersion == 5) {
              // never released, pre-release/debug version only
              await db.transaction((txn) async {
                var batch = txn.batch();

                batch.execute(
                    'alter table $tableNameWords add column $columnCreatedAt timestamp');
                batch.execute(
                    'alter table $tableNameWords add column $columnModifiedAt timestamp');

                batch.execute(
                    'alter table $tableNameTags add column $columnCreatedAt timestamp');
                batch.execute(
                    'alter table $tableNameTags add column $columnModifiedAt timestamp');

                batch.execute(
                    'alter table $tableNameWordsTags add column $columnCreatedAt timestamp');
                batch.execute(
                    'alter table $tableNameWordsTags add column $columnModifiedAt timestamp');

                await batch.commit();
              });
            } else {
              MessageBar.error(
                  title: CustomLocalizations.of(context).error,
                  message: CustomLocalizations.of(context).invalidDatabaseState)
                ..show(context);
            }
          },
        ),
      );
    }

    return database;
  }

  void close() async {
//    final db = await database;
//
//    await db.close();
  }

  Future<void> primeCaches() {
    return Future.value();
  }

  void setFilter(Map<String, dynamic> filter) {
    where = '';
    whereArgs = List();

    filter.forEach((k, v) {
      where += '$k like ?';
      whereArgs.add(v);
    });

    tagsWhere = '';
    tagsWhereArgs?.clear();
  }

  void setTagFilter(List<String> filter) {
    tagsWhere = '';
    tagsWhereArgs = List();

    filter.forEach((v) {
      tagsWhere += 'tags.$columnTag like ?';
      tagsWhereArgs.add(v);
    });

    where = '';
    whereArgs?.clear();
  }

  Future<int> getNumEntries() async {
    final db = await database;

    List<Map> maps;

    if (tagsWhereArgs != null && tagsWhereArgs.length > 0) {
      // filter by tags
      await db.transaction((txn) async {
        final sql =
            'select count(distinct $tableNameWords.$columnId) from $tableNameWords inner join '
            '$tableNameWordsTags on $tableNameWords.$columnId = '
            '$tableNameWordsTags.$columnWordId inner join $tableNameTags on '
            '$tableNameWordsTags.$columnTagId = $tableNameTags.$columnId where '
            '$tableNameTags.$columnTag like ?';
        final args = tagsWhereArgs;

        maps = await txn.rawQuery(sql, args);
      });

      return Future.value(
          maps.first['count(distinct $tableNameWords.$columnId)']);
    } else {
      // filter by other columns
      await db.transaction((txn) async {
        var w;
        if (where != null && where.isNotEmpty) w = where;

        maps = await txn.query(
          currentTableName,
          columns: [
            'count($columnId)',
          ],
          where: w,
          whereArgs: whereArgs,
        );
      });

      return Future.value(maps.first['count($columnId)']);
    }
  }
}
