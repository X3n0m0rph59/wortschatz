/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/data_access_layer_edit_history.dart';
import 'package:wortschatz/data/edit_history_entry.dart';
import 'package:wortschatz/widgets/custom_animated_list.dart';
import 'package:wortschatz/widgets/edit_history_row.dart';

const STRIDE_SIZE = 1;

class EditHistoryListModel {
  final BuildContext context;
  final String database;

  final String sortColumn;
  final SortOrder sortOrder;

  EditHistoryListModel(this.context, this.database, this.listKey,
      this.sortColumn, this.sortOrder)
      : dal = DataAccessLayerEditHistory(
            database, context, sortColumn, sortOrder);

  final DataAccessLayerEditHistory dal;

  GlobalKey<CustomAnimatedListState> listKey;

  CustomAnimatedListState get _animatedList => listKey?.currentState;

  void dispose() {
    dal.close();
  }

  Future<EditHistoryEntry> insert(int index, EditHistoryEntry item) async {
//    var result = Map<int, TagEntry>();
//
//    for (int i = 0; i < index; i++) {
//      result[i] = cachedItems[i];
//    }
//
//    result[index] = item;
//
//    for (int i = index + 1; i > await length; i++) {
//      result[i + 1] = cachedItems[i];
//    }
//
//    cachedItems = result;

//    cachedItems.clear();

    _animatedList?.insertItem(index, duration: Duration(milliseconds: 200));

    return dal.insert(item);
  }

  Future<void> remove(EditHistoryEntry entry, index) async {
    await dal.deleteById(entry.id);

    // avoid division by zero
    var len = await length;
    if (len == 0) len = 1;

    _animatedList?.removeItem(
        index,
        (BuildContext context, Animation<double> animation) => EditHistoryRow(
              entry: entry,
              animation: animation,
            ),
        duration: Duration(
            milliseconds:
                (Config.listAnimationDurationMsecs + 200 * (index / len))
                    .toInt()));

    return Future.value();
  }

//  Future<EditHistoryEntry> removeAt(int index) async {
//    final EditHistoryEntry removedItem = cachedItems.remove(index);
//    await dal.deleteById(removedItem.id);
//
//    if (removedItem != null) {
//      _animatedList.removeItem(
//          index,
//          (context, animation) => TagRow(
//                entry: removedItem,
//                animation: animation,
//              ),
//          duration: Duration(
//              milliseconds: (Config.listAnimationDurationMsecs + 200 * (index / (await length))).toInt()));
//    }
//
//    return Future.value(removedItem);
//  }

  Future<int> get length => dal.getNumEntries();

  Future<EditHistoryEntry> operator [](int index) => _getAt(index);

  Future<EditHistoryEntry> _getAt(int index) async {
//    var entry = cachedItems[index];
//
//    if (entry == null) {
//      print('Cache miss at: $index');
//
//      final cache = await dal.getEntries(index, STRIDE_SIZE);
//
//      var cnt = 0;
//      for (int i = index; i < (index + cache.length); i++) {
//        cachedItems[i] = cache[cnt];
//        cnt++;
//      }
//    } else {
//      // print('Cache hit!');
//    }
//
//    entry = cachedItems[index];

    return Future.value(dal.getEntry(index));
  }

  Future<EditHistoryEntry> getById(int id) async {
    return dal.getEntryById(id);
  }

  Future<int> update(EditHistoryEntry entry) async {
//    cachedItems.clear();
    return dal.update(entry);
  }

  void setFilter(Map<String, dynamic> filter) {
    dal.setFilter(filter);
//    cachedItems.clear();
  }

  Future<void> undoAction(EditHistoryEntry entry) async {
    throw Exception('Not implemented');
  }

  Future<List<EditsPerDay>> getEditsPerDayStats(int numDays) async {
    final maps = await dal.getEditsPerDayStats();

    var result = <EditsPerDay>[];
    maps.getRange(max(0, maps.length - numDays), maps.length).forEach((e) {
      final val = EditsPerDay(
        DateTime.parse(e['date']),
        e['count'],
      );

      result.add(val);
    });

    return result;
  }

  // int indexOf(WordEntry item) => items.indexOf(item);
}
