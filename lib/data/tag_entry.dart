/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/util/util.dart';

class TagEntry extends MappableEntry {
  String tag;
  String description;
  Color color;
  bool favorite;
  DateTime createdAt, modifiedAt;

  @override
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnTag: tag,
      columnDescription: description,
      columnColor: color.value,
      columnFavorite: favorite ? 1 : 0,
      columnCreatedAt: createdAt?.toIso8601String(),
      columnModifiedAt: modifiedAt?.toIso8601String(),
    };

    if (id != null) {
      map[columnId] = id;
    } else {
      print('Warning: tag.id == null');
    }

    return map;
  }

  TagEntry({
    id,
    this.tag,
    this.description,
    this.color,
    this.favorite,
    this.createdAt,
    this.modifiedAt,
  }) : super(id: id);

  factory TagEntry.from(TagEntry other) => TagEntry(
        id: other.id,
        tag: other.tag,
        description: other.description,
        color: other.color,
        favorite: other.favorite,
        createdAt: other.createdAt,
        modifiedAt: other.modifiedAt,
      );

  factory TagEntry.defaults() => TagEntry(
        id: null,
        tag: '',
        description: '',
        color: Colors.lightBlue,
        favorite: false,
        createdAt: DateTime.now(),
        modifiedAt: DateTime.now(),
      );

  TagEntry.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    tag = map[columnTag];
    description = map[columnDescription];
    if (map[columnColor] == null)
      color = Colors.white;
    else
      color = Color(map[columnColor]);
    favorite = intToBool(map[columnFavorite]);
    createdAt = DateTime.parse(map[columnCreatedAt]);
    modifiedAt = DateTime.parse(map[columnModifiedAt]);
  }

  @override
  String toString() =>
      "TagEntry('$id', '$tag', '$description', '${color.value}', '$favorite', "
      "'$createdAt', '$modifiedAt')";

  String toDescription() => "$tag - $description";

  String toJson() =>
      '{ "id": $id, "tag": "$tag", "description": "$description", '
      '"color": ${color.value}, "favorite": $favorite, '
      '"createdAt": "${createdAt?.toIso8601String()}", '
      '"modifiedAt": "${modifiedAt?.toIso8601String()}" }';

  TagEntry.fromJson(Map<String, dynamic> json)
      : tag = json['tag'],
        description = json['description'],
        color = Color(json['color']),
        favorite = json['favorite'],
        createdAt = json['createdAt'] == 'null'
            ? null
            : DateTime.parse(json['createdAt']),
        modifiedAt = json['modifiedAt'] == 'null'
            ? null
            : DateTime.parse(json['modifiedAt']),
        super(id: json['id']);
}
