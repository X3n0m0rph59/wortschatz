/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/exceptions/invalid_entry_type.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/words_tags_entry.dart';

class DataAccessLayerWords extends DataAccessLayer<WordEntry> {
  bool onlyDupesMode = false;

  DataAccessLayerWords(String databaseFilePath, BuildContext context,
      String sortColumn, SortOrder sortOrder)
      : super(databaseFilePath, Tables.words, context, sortColumn, sortOrder);

  @override
  String get currentTableName => tableNameWords;

  List<String> get defaultColumns => <String>[
        columnId,
        columnWord,
        columnDescription,
        columnColor,
        columnFavorite,
        columnCreatedAt,
        columnModifiedAt,
      ];

  Future<WordEntry> insert(WordEntry entry) async {
    print('Inserting: ${entry.toString()}');

    final db = await database;

    await db.transaction((txn) async {
      entry.createdAt = DateTime.now();
      entry.modifiedAt = DateTime.now();

      entry.id = await txn.insert(currentTableName, entry.toMap());
    });

    return entry;
  }

  @override
  Future<int> getNumEntries() async {
    final db = await database;

    List<Map> maps;

    if (tagsWhereArgs != null && tagsWhereArgs.length > 0) {
      // filter by tags
      await db.transaction((txn) async {
        final sql =
            'select count(distinct $tableNameWords.$columnId) from $tableNameWords inner join '
            '$tableNameWordsTags on $tableNameWords.$columnId = '
            '$tableNameWordsTags.$columnWordId inner join $tableNameTags on '
            '$tableNameWordsTags.$columnTagId = $tableNameTags.$columnId where '
            '$tableNameTags.$columnTag like ?';
        final args = tagsWhereArgs;

        maps = await txn.rawQuery(sql, args);
      });

      return Future.value(
          maps.first['count(distinct $tableNameWords.$columnId)']);
    }
    if (onlyDupesMode) {
      await db.transaction((txn) async {
        final sql =
            'select count(*) as count from (select count($tableNameWords.$columnId) from $tableNameWords '
            'group by $tableNameWords.$columnWord '
            'having (count(*) > 1))';
        final args = tagsWhereArgs;

        maps = await txn.rawQuery(sql, args);
      });

      return Future.value(maps.first['count']);
    } else {
      // filter by other columns
      await db.transaction((txn) async {
        var w;
        if (where != null && where.isNotEmpty) w = where;

        maps = await txn.query(
          currentTableName,
          columns: [
            'count($columnId)',
          ],
          where: w,
          whereArgs: whereArgs,
        );
      });

      return Future.value(maps.first['count($columnId)']);
    }
  }

//  Future<WordEntry> getEntry(int index) async {
//    final db = await database;
//
//    List<Map> maps;
//
//    await db.transaction((txn) async {
//      var w;
//      if (where != null && where.isNotEmpty) w = where;
//
//      maps = await txn.query(
//        currentTableName,
//        columns: defaultColumns,
//        offset: index,
//        limit: 1,
//        orderBy: '$sortColumn ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
//        where: w,
//        whereArgs: whereArgs,
//      );
//    });
//
//    if (maps.length > 0) {
//      if (table == Tables.words) {
//        return Future.value(WordEntry.fromMap(maps.first));
//      } else {
//        throw InvalidEntryTypeException();
//      }
//    } else {
//      return null;
//    }
//  }

  Future<List<WordEntry>> getEntries(int startIndex, int count) async {
    final db = await database;

    List<Map> maps;

    if (tagsWhereArgs != null && tagsWhereArgs.length > 0) {
      // filter by tags
      await db.transaction((txn) async {
        final sql =
            'select distinct $tableNameWords.$columnId, $tableNameWords.$columnWord, '
            '$tableNameWords.$columnDescription, $tableNameWords.$columnColor, '
            '$tableNameWords.$columnFavorite, $tableNameWords.$columnCreatedAt, '
            '$tableNameWords.$columnModifiedAt from $tableNameWords inner join '
            '$tableNameWordsTags on $tableNameWords.$columnId = '
            '$tableNameWordsTags.$columnWordId inner join $tableNameTags on '
            '$tableNameWordsTags.$columnTagId = $tableNameTags.$columnId where '
            '$tableNameTags.$columnTag like ? '
            'order by $tableNameWords.$sortColumn '
            'limit $count offset $startIndex';

        final args = tagsWhereArgs;

        maps = await txn.rawQuery(sql, args);
      });
    } else if (onlyDupesMode) {
      // return only rows that are duplicates
      await db.transaction((txn) async {
        final sql =
            'select $tableNameWords.$columnId, $tableNameWords.$columnWord, '
            '$tableNameWords.$columnDescription, $tableNameWords.$columnColor, '
            '$tableNameWords.$columnFavorite, $tableNameWords.$columnCreatedAt, '
            '$tableNameWords.$columnModifiedAt from $tableNameWords '
            'group by $tableNameWords.$columnWord '
            'having (count(*) > 1) '
            'limit $count offset $startIndex';

        final args = tagsWhereArgs;

        maps = await txn.rawQuery(sql, args);
      });
    } else {
      await db.transaction((txn) async {
        var w;
        if (where != null && where.isNotEmpty) w = where;

        maps = await txn.query(
          currentTableName,
          columns: defaultColumns,
          offset: startIndex,
          limit: count,
          orderBy:
              '$sortColumn ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
          where: w,
          whereArgs: whereArgs,
        );
      });
    }

    var result = List<WordEntry>();
    maps.forEach((e) {
      if (table == Tables.words) {
        var entry = WordEntry.fromMap(e);
        entry.tags = getTags(entry.id);

        result.add(entry);
      } else {
        throw InvalidEntryTypeException();
      }
    });

    return Future.value(result);
  }

  Future<List<TagEntry>> getTags(int wordId) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
//    List<Map> maps = await txn.query(
//      '$tableNameWordsTags,$tableNameTags',
//      columns: <String>[
//        '$tableNameTags.$columnId as $columnId',
//        '$tableNameTags.$columnTag as $columnTag',
//        '$tableNameTags.$columnDescription as $columnDescription',
//        '$tableNameTags.$columnColor as $columnColor',
//        '$tableNameTags.$columnFavorite as $columnFavorite',
//      ],
//      // orderBy: '_id',
//      where: '$tableNameWordsTags.$columnTagId = $tableNameTags.$columnId',
//      whereArgs: <dynamic>[
//        // '$wordId',
//      ],
//    );

      final query =
          'select distinct $tableNameTags.* from $tableNameTags left join '
          '$tableNameWordsTags on $tableNameTags.$columnId = $tableNameWordsTags.$columnTagId '
          'where $tableNameWordsTags.$columnWordId = ?';

      maps = await txn.rawQuery(query, [wordId]);
    });

    var result = List<TagEntry>();
    maps.forEach((e) {
      result.add(TagEntry.fromMap(e));
    });

    return Future.value(result);
  }

  Future<WordEntry> getEntryById(int id) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        where: '$columnId = ?',
        whereArgs: [id],
      );
    });

    if (maps.length > 0) {
      if (table == Tables.words) {
        final entry = WordEntry.fromMap(maps.first);
        entry.tags = getTags(entry.id);

        return Future.value(entry);
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<WordEntry> getEntryByWord(String word) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        where: '$columnWord = ?',
        whereArgs: [word],
      );
    });

    if (maps.length > 0) {
      if (table == Tables.words) {
        return Future.value(WordEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<int> deleteById(int id) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      result =
          txn.delete(currentTableName, where: '$columnId = ?', whereArgs: [id]);
    });

    return result;
  }

  Future update(WordEntry entry) async {
    assert(entry.id != null);

    final db = await database;

    var result;

    await db.transaction((txn) async {
      var batch = txn.batch();

      entry.modifiedAt = DateTime.now();

      batch.update(currentTableName, entry.toMap(),
          where: '$columnId = ?', whereArgs: [entry.id]);

      // first, delete stale associated tags...
      batch.delete(tableNameWordsTags,
          where: '$columnWordId = ?', whereArgs: [entry.id]);

      // ...then insert valid tags
      final tags = await entry.tags ?? <TagEntry>[];
      for (final tag in tags) {
        final WordsTagsEntry e =
            WordsTagsEntry(wordId: entry.id, tagId: tag.id);

        // TODO: preserve dates on existing entries
        e.createdAt = DateTime.now();
        e.modifiedAt = DateTime.now();

        batch.insert(tableNameWordsTags, e.toMap());
      }

      result = await batch.commit();
    });

    return result;
  }

  Future<List<Map<String, dynamic>>> getWordsPerDayStats() async {
    final sql =
        'select count($columnId) as count, substr($columnCreatedAt,0,11) as date '
        'from $tableNameWords group by substr($columnCreatedAt,0,11) order by date asc';

    final db = await database;

    var result;
    await db.transaction((txn) async {
      result = await txn.rawQuery(sql);
    });

    return result;
  }
}

class WordsPerDay {
  final DateTime date;
  final int wordCount;

  WordsPerDay(this.date, this.wordCount);
}
