/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/exceptions/invalid_entry_type.dart';
import 'package:wortschatz/data/words_tags_entry.dart';

class DataAccessLayerWordsTags extends DataAccessLayer<WordsTagsEntry> {
  DataAccessLayerWordsTags(String databaseFilePath, BuildContext context,
      String sortColumn, SortOrder sortOrder)
      : super(
            databaseFilePath, Tables.wordsTags, context, sortColumn, sortOrder);

  @override
  String get currentTableName => tableNameWordsTags;

  List<String> get defaultColumns => <String>[
        columnId,
        columnWordId,
        columnTagId,
        columnCreatedAt,
        columnModifiedAt,
      ];

  Future<WordsTagsEntry> insert(WordsTagsEntry entry) async {
    print('Inserting: ${entry.toString()}');

    final db = await database;

    await db.transaction((txn) async {
      entry.createdAt = DateTime.now();
      entry.modifiedAt = DateTime.now();

      entry.id = await txn.insert(currentTableName, entry.toMap());
    });

    return entry;
  }

  Future<WordsTagsEntry> getEntry(int index) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      var w;
      if (where != null && where.isNotEmpty) w = where;

      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        offset: index,
        limit: 1,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: w,
        whereArgs: whereArgs,
      );
    });

    if (maps.length > 0) {
      if (table == Tables.tags) {
        return Future.value(WordsTagsEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<List<WordsTagsEntry>> getEntries(int startIndex, int count) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      var w;
      if (where != null && where.isNotEmpty) w = where;

      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        offset: startIndex,
        limit: count,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: w,
        whereArgs: whereArgs,
      );
    });

    var result = List<WordsTagsEntry>();
    maps.forEach((e) {
      if (table == Tables.tags) {
        result.add(WordsTagsEntry.fromMap(e));
      } else {
        throw InvalidEntryTypeException();
      }
    });

    if (maps.length > 0) {
      return Future.value(result);
    } else {
      return null;
    }
  }

  Future<List<WordsTagsEntry>> getEntriesByWordId(int wordId) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: '$columnWordId = ?',
        whereArgs: [wordId],
      );
    });

    var result = List<WordsTagsEntry>();
    maps.forEach((e) {
      if (table == Tables.tags) {
        result.add(WordsTagsEntry.fromMap(e));
      } else {
        throw InvalidEntryTypeException();
      }
    });

    if (maps.length > 0) {
      return Future.value(result);
    } else {
      return null;
    }
  }

  Future<WordsTagsEntry> getEntryById(int id) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        where: '$columnId = ?',
        whereArgs: [id],
      );
    });

    if (maps.length > 0) {
      if (table == Tables.tags) {
        return Future.value(WordsTagsEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<int> deleteById(int id) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      result =
          txn.delete(currentTableName, where: '$columnId = ?', whereArgs: [id]);
    });

    return result;
  }

  Future<int> update(WordsTagsEntry entry) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      entry.modifiedAt = DateTime.now();

      result = txn.update(currentTableName, entry.toMap(),
          where: '$columnId = ?', whereArgs: [entry.id]);
    });

    return result;
  }
}
