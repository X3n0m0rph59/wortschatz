/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:wortschatz/data/data_access_layer_base.dart';

class WordsTagsEntry extends MappableEntry {
  int wordId;
  int tagId;
  DateTime createdAt, modifiedAt;

  @override
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnWordId: wordId,
      columnTagId: tagId,
      columnCreatedAt: createdAt?.toIso8601String(),
      columnModifiedAt: modifiedAt?.toIso8601String(),
    };

    if (id != null) {
      map[columnId] = id;
    }

    return map;
  }

  WordsTagsEntry({
    this.wordId,
    this.tagId,
    this.createdAt,
    this.modifiedAt,
  }) : super();

  WordsTagsEntry.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    wordId = map[columnWordId];
    tagId = map[columnTagId];
    createdAt = DateTime.parse(map[columnCreatedAt]);
    modifiedAt = DateTime.parse(map[columnModifiedAt]);
  }

  @override
  String toString() =>
      "WordsTagsEntry('$id', '$wordId', '$tagId', '$createdAt', '$modifiedAt')'";

  String toDescription() => null;

  String toJson() => '{ "_id": $id, "wordId": $wordId, "tagId": $tagId, '
      '"createdAt": "${createdAt?.toIso8601String()}", '
      '"modifiedAt": "${modifiedAt?.toIso8601String()}" }';
}
