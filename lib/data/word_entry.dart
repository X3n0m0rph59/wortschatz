/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/util/util.dart';

class WordEntry extends MappableEntry {
  String word;
  String description;
  Color color;
  bool favorite;
  DateTime createdAt, modifiedAt;
  Future<List<TagEntry>> tags;

  @override
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnWord: word,
      columnDescription: description,
      columnColor: color.value,
      columnFavorite: favorite ? 1 : 0,
      columnCreatedAt: createdAt?.toIso8601String(),
      columnModifiedAt: modifiedAt?.toIso8601String(),
    };

    if (id != null) {
      map[columnId] = id;
    } else {
      print('Warning: word.id == null');
    }

    return map;
  }

  WordEntry({
    id,
    this.word,
    this.description,
    this.color,
    this.favorite,
    this.tags,
    this.createdAt,
    this.modifiedAt,
  }) : super(id: id);

  factory WordEntry.from(WordEntry other) => WordEntry(
        id: other.id,
        word: other.word,
        description: other.description,
        color: other.color,
        favorite: other.favorite,
        tags: cloneList(other.tags),
        createdAt: other.createdAt,
        modifiedAt: other.modifiedAt,
      );

  factory WordEntry.defaults() => WordEntry(
        id: null,
        word: '',
        description: '',
        color: Colors.deepOrange,
        favorite: false,
        tags: Future.value(<TagEntry>[]),
        createdAt: DateTime.now(),
        modifiedAt: DateTime.now(),
      );

  WordEntry.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    word = map[columnWord];
    description = map[columnDescription];
    if (map[columnColor] == null)
      color = Colors.white;
    else
      color = Color(map[columnColor]);
    favorite = intToBool(map[columnFavorite]);
    createdAt = DateTime.parse(map[columnCreatedAt]);
    modifiedAt = DateTime.parse(map[columnModifiedAt]);
  }

  @override
  String toString() =>
      "WordEntry('$id', '$word', '$description', '${color.value}', "
      "'$favorite', '$createdAt', '$modifiedAt')";

  String toDescription() => "$word - $description";

  String toJson() =>
      '{ "id": $id, "word": "$word", "description": "$description", '
      '"color": ${color.value}, "favorite": $favorite, '
      '"createdAt": "${createdAt?.toIso8601String()}", '
      '"modifiedAt": "${modifiedAt?.toIso8601String()}" }';

  WordEntry.fromJson(Map<String, dynamic> json)
      : word = json['word'],
        description = json['description'],
        color = Color(json['color']),
        favorite = json['favorite'],
        createdAt = json['createdAt'] == 'null'
            ? null
            : DateTime.parse(json['createdAt']),
        modifiedAt = json['modifiedAt'] == 'null'
            ? null
            : DateTime.parse(json['modifiedAt']),
        super(id: json['id']);
}
