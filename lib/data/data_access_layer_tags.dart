/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/exceptions/invalid_entry_type.dart';
import 'package:wortschatz/data/tag_entry.dart';

class DataAccessLayerTags extends DataAccessLayer<TagEntry> {
  DataAccessLayerTags(String databaseFilePath, BuildContext context,
      String sortColumn, SortOrder sortOrder)
      : super(databaseFilePath, Tables.tags, context, sortColumn, sortOrder);

  @override
  String get currentTableName => tableNameTags;

  List<String> get defaultColumns => <String>[
        columnId,
        columnTag,
        columnDescription,
        columnColor,
        columnFavorite,
        columnCreatedAt,
        columnModifiedAt,
      ];

  Future<TagEntry> insert(TagEntry entry) async {
    print('Inserting: ${entry.toString()}');

    final db = await database;

    await db.transaction((txn) async {
      entry.createdAt = DateTime.now();
      entry.modifiedAt = DateTime.now();

      entry.id = await txn.insert(currentTableName, entry.toMap());
    });

    return entry;
  }

  Future<TagEntry> getEntry(int index) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      var w;
      if (where != null && where.isNotEmpty) w = where;

      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        offset: index,
        limit: 1,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: w,
        whereArgs: whereArgs,
      );
    });

    if (maps.length > 0) {
      if (table == Tables.tags) {
        return Future.value(TagEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<List<TagEntry>> getEntries(int startIndex, int count) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      var w;
      if (where != null && where.isNotEmpty) w = where;

      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        offset: startIndex,
        limit: count,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: w,
        whereArgs: whereArgs,
      );
    });

    var result = List<TagEntry>();
    maps.forEach((e) {
      if (table == Tables.tags) {
        result.add(TagEntry.fromMap(e));
      } else {
        throw InvalidEntryTypeException();
      }
    });

    return Future.value(result);
  }

  Future<TagEntry> getEntryById(int id) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        where: '$columnId = ?',
        whereArgs: [id],
      );
    });

    if (maps.length > 0) {
      if (table == Tables.tags) {
        return Future.value(TagEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<int> deleteById(int id) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      result =
          txn.delete(currentTableName, where: '$columnId = ?', whereArgs: [id]);
    });

    return result;
  }

  Future<int> update(TagEntry entry) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      entry.modifiedAt = DateTime.now();

      result = txn.update(currentTableName, entry.toMap(),
          where: '$columnId = ?', whereArgs: [entry.id]);
    });

    return result;
  }

  Future<List<Map<String, dynamic>>> getTagsPerDayStats() async {
    final sql =
        'select count($columnId) as count, substr($columnCreatedAt,0,11) as date '
        'from $tableNameTags group by substr($columnCreatedAt,0,11) order by date asc';

    final db = await database;

    var result;
    await db.transaction((txn) async {
      result = await txn.rawQuery(sql);
    });

    return result;
  }
}

class TagsPerDay {
  final DateTime date;
  final int tagCount;

  TagsPerDay(this.date, this.tagCount);
}
