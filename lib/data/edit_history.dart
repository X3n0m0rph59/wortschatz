/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/edit_history_entry.dart';
import 'package:wortschatz/data/edit_history_list_model.dart';
import 'package:wortschatz/data/exceptions/invalid_entry_type.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/tag_list_model.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/word_list_model.dart';
import 'package:wortschatz/exceptions/invalid_state_exception.dart';
import 'package:wortschatz/exceptions/runtime_error.dart';
import 'package:wortschatz/util/database.dart';
import 'package:wortschatz/util/edit_history.dart';
import 'package:wortschatz/util/l10n.dart';

class EditHistoryAction {
  static const int wordAdded = 0;
  static const int wordEdited = 1;
  static const int wordDeleted = 2;

  static const int wordTagChanged = 3;
  static const int wordColorChanged = 4;
  static const int wordFavoriteChanged = 5;

  static const int wortschatzRangeStart = 0;
  static const int wortschatzRangeEnd = 5;

  static const int wordAddedDictionary = 6;
  static const int wordEditedDictionary = 7;
  static const int wordDeletedDictionary = 8;

  static const int wordTagChangedDictionary = 9;
  static const int wordColorChangedDictionary = 10;
  static const int wordFavoriteChangedDictionary = 11;

  static const int dictionaryRangeStart = 6;
  static const int dictionaryRangeEnd = 11;

  static const int tagAdded = 12;
  static const int tagEdited = 13;
  static const int tagDeleted = 14;

  static const int tagColorChanged = 15;
  static const int tagFavoriteChanged = 16;

  static const int wordTagAdded = 17;
  static const int wordTagRemoved = 18;

  static const _colors = [
    // Wortschatz
    Colors.green, // Wort hinzugefügt
    Colors.greenAccent, // Wort bearbeitet
    Colors.red, // Wort gelöscht
    Colors.deepOrange, // Verknüpfte Schlagwörter geändert
    Colors.tealAccent, // Farbe geändert
    Colors.orange, // Favorit geändert

    // Dictionary
    Colors.green, // Wort hinzugefügt
    Colors.greenAccent, // Wort bearbeitet
    Colors.red, // Wort gelöscht
    Colors.deepOrange, // Verknüpfte Schlagwörter geändert
    Colors.tealAccent, // Farbe geändert
    Colors.orange, // Favorit geändert

    // Tags
    Colors.indigo, // Schlagwort hinzugefügt
    Colors.indigoAccent, // Schlagwort bearbeitet
    Colors.pink, // Schlagwort gelöscht
    Colors.tealAccent, // Farbe geändert
    Colors.orange, // Favorit geändert

    Colors.indigoAccent, // Schlagwort Verknüpfung hinzugefügt
    Colors.pinkAccent, // Schlagwort Verknüpfung entfernt
  ];

  final int action;
  final BuildContext context;

  EditHistoryAction(this.action, this.context);

  String actionToString(int action) {
    var _strings = [
      // Wortschatz
      CustomLocalizations.of(context).wordAdded,
      CustomLocalizations.of(context).wordEdited,
      CustomLocalizations.of(context).wordDeleted,
      CustomLocalizations.of(context).tagsModified,
      CustomLocalizations.of(context).wordColorChanged,
      CustomLocalizations.of(context).wordFavoriteChanged,

      CustomLocalizations.of(context).wordAddedDictionary,
      CustomLocalizations.of(context).wordEditedDictionary,
      CustomLocalizations.of(context).wordDeletedDictionary,
      CustomLocalizations.of(context).tagsModifiedDictionary,
      CustomLocalizations.of(context).wordColorChangedDictionary,
      CustomLocalizations.of(context).wordFavoriteChangedDictionary,

      CustomLocalizations.of(context).tagAdded,
      CustomLocalizations.of(context).tagEdited,
      CustomLocalizations.of(context).tagDeleted,
      CustomLocalizations.of(context).tagColorChanged,
      CustomLocalizations.of(context).tagFavoriteChanged,

      CustomLocalizations.of(context).tagAssociated,
      CustomLocalizations.of(context).tagUnAssociated,
    ];

    return _strings[action];
  }

  @override
  String toString() {
    return actionToString(action);
  }

  Color toColor() {
    return _colors[action];
  }
}

class EditActionLogger {
  static final EditActionLogger _instance = EditActionLogger._internal();

  factory EditActionLogger([context]) {
    context = context;

    return _instance;
  }

  EditActionLogger._internal() {
    listModel = EditHistoryListModel(context, Config.editHistoryDatabase, null,
        columnId, SortOrder.ascending);
  }

  var context;

  EditHistoryListModel listModel;

  void logAction(
      String description, int action, LogPayload payload, context) async {
    print('Action: ${EditHistoryAction(action, context).toString()}');

    final EditHistoryEntry entry = EditHistoryEntry(
      action: action,
      description: description,
      payload: payload,
    );

    await listModel.insert(0, entry);
  }
}

enum PayloadType {
  wordEntry,
  tagEntry,
}

final Map<int, PayloadType> payloadTypeMap = <int, PayloadType>{
  0: PayloadType.wordEntry,
  1: PayloadType.tagEntry,
};

class LogPayload {
  MappableEntry before, now;

  LogPayload(this.before, this.now);

  @override
  String toString() =>
      "LogPayload('${before?.toString()}', '${now?.toString()}')";

  String toJson() {
    int type;

    if (before is WordEntry || now is WordEntry) {
      type = 0;
    } else if (before is TagEntry || now is TagEntry) {
      type = 1;
    } else
      throw InvalidEntryTypeException();

    return '{ "type": $type, "before": ${before?.toJson()}, "now": ${now?.toJson()}}';
  }

  LogPayload.fromJson(Map<String, dynamic> json) {
    try {
      final PayloadType payloadType = payloadTypeMap[json['type']];

      switch (payloadType) {
        case PayloadType.wordEntry:
          before = json['before'] == null
              ? null
              : WordEntry.fromJson(json['before']);
          now = json['now'] == null ? null : WordEntry.fromJson(json['now']);
          break;

        case PayloadType.tagEntry:
          before =
              json['before'] == null ? null : TagEntry.fromJson(json['before']);
          now = json['now'] == null ? null : TagEntry.fromJson(json['now']);
          break;

        default:
          throw InvalidEntryTypeException();
      }
    } catch (e) {
      print('Could not load JSON data');

      throw e;
    }
  }
}

Future<void> undoAction(EditHistoryEntry entry, BuildContext context) async {
  var database;

  if (entry.action >= EditHistoryAction.wortschatzRangeStart &&
      entry.action <= EditHistoryAction.wortschatzRangeEnd)
    database = Config.wordsDatabase;
  else if (entry.action >= EditHistoryAction.dictionaryRangeEnd &&
      entry.action <= EditHistoryAction.dictionaryRangeEnd)
    database = Config.dictionaryDatabase;
  else
    throw InvalidStateException('Incorrect action range value detected');

  switch (entry.action) {
    case EditHistoryAction.wordAdded:
    case EditHistoryAction.wordAddedDictionary:
      // word has been added: remove the word again
      final listModelWords =
          WordListModel(context, database, null, columnId, SortOrder.ascending);

      assert(entry.payload.now is WordEntry);
      final WordEntry word = entry.payload.now as WordEntry;
      final WordEntry currentWord = await listModelWords.getById(word.id);

      final rowsAffected =
          (await listModelWords.remove(currentWord, null)) ?? 0;

      if (rowsAffected > 0) {
        logDeleteWordAction(
            '${CustomLocalizations.of(context).undoWordAdded} ${currentWord.word}',
            currentWord,
            null,
            context);
      } else {
        throw RuntimeError('Could not delete dataset');
      }
      break;

    case EditHistoryAction.wordEdited:
    case EditHistoryAction.wordEditedDictionary:
    case EditHistoryAction.wordColorChanged:
    case EditHistoryAction.wordColorChangedDictionary:
    case EditHistoryAction.wordFavoriteChanged:
    case EditHistoryAction.wordFavoriteChangedDictionary:
    case EditHistoryAction.wordTagChanged:
    case EditHistoryAction.wordTagChangedDictionary:

    case EditHistoryAction.wordTagAdded:
    case EditHistoryAction.wordTagRemoved:

      // word has been modified: restore the previous version
      final listModelWords =
          WordListModel(context, database, null, columnId, SortOrder.ascending);

      assert(entry.payload.before is WordEntry);
      assert(entry.payload.now is WordEntry);

      final WordEntry before = entry.payload.before as WordEntry;
      final WordEntry currentWord = await listModelWords.getById(before.id);

      var rowsAffected;

      switch (entry.action) {
        case EditHistoryAction.wordEdited:
          final WordEntry tmp = WordEntry.from(currentWord);

          tmp.word = before.word;
          tmp.description = before.description;

          rowsAffected = (await listModelWords.update(tmp)) ?? <int>[0];

          if (atLeastOneRowAffected(rowsAffected)) {
            logEditWordAction(
                '${CustomLocalizations.of(context).undoWordEdited} ${tmp.word}',
                currentWord,
                tmp,
                context);
          } else {
            throw RuntimeError('Could not update dataset');
          }
          break;

        case EditHistoryAction.wordColorChanged:
          final WordEntry tmp = WordEntry.from(currentWord);

          tmp.color = before.color;

          rowsAffected = (await listModelWords.update(tmp)) ?? <int>[0];

          if (atLeastOneRowAffected(rowsAffected)) {
            logChangeWordColorAction(
                '${CustomLocalizations.of(context).undoWordColorChanged} ${tmp.word}',
                currentWord,
                tmp,
                context);
          } else {
            throw RuntimeError('Could not update dataset');
          }
          break;

        case EditHistoryAction.wordFavoriteChanged:
          final WordEntry tmp = WordEntry.from(currentWord);

          tmp.favorite = before.favorite;

          rowsAffected = (await listModelWords.update(tmp)) ?? <int>[0];

          if (atLeastOneRowAffected(rowsAffected)) {
            logChangeWordFavoriteAction(
                '${CustomLocalizations.of(context).undoWordFavoriteChanged} ${tmp.word}',
                currentWord,
                tmp,
                context);
          } else {
            throw RuntimeError('Could not update dataset');
          }
          break;

        case EditHistoryAction.wordTagChanged:
          // Disable this for now since we handle each tag change separately
//          final WordEntry tmp = WordEntry.from(currentWord);
//
//          tmp.tags = before.tags;
//
//          rowsAffected = (await listModelWords.update(tmp)) ?? <int>[0];
//
//          if (atLeastOneRowAffected(rowsAffected)) {
//            logChangeWordTagAction(
//                '${CustomLocalizations.of(context).undoWordTagChanged} ${tmp.word}',
//                currentWord,
//                tmp,
//                context);
//          } else {
//            throw RuntimeError('Could not update dataset');
//          }

          throw InvalidStateException();
          break;

        case EditHistoryAction.wordTagAdded:
//          final WordEntry tmp = WordEntry.from(currentWord);
//
//          tmp.tags = before.tags;
//
//          rowsAffected = (await listModelWords.update(tmp)) ?? <int>[0];
//
//          if (atLeastOneRowAffected(rowsAffected)) {
//            logAddTagAction('${CustomLocalizations.of(context).undoWordTagAdded} ${tmp.word}',
//                currentWord, tmp, context);
//          } else {
//            throw RuntimeError('Could not update dataset');
//          }
          break;

        case EditHistoryAction.wordTagRemoved:
//          final WordEntry tmp = WordEntry.from(currentWord);
//
//          tmp.tags = before.tags;
//
//          rowsAffected = (await listModelWords.update(tmp)) ?? <int>[0];
//
//          if (atLeastOneRowAffected(rowsAffected)) {
//            logChangeWordTagAction(
//                '${CustomLocalizations.of(context).undoWordTagRemoved} ${tmp.word}',
//                currentWord,
//                tmp,
//                context);
//          } else {
//            throw RuntimeError('Could not update dataset');
//          }
          break;

        default:
          throw InvalidStateException();
      }
      break;

    case EditHistoryAction.wordDeleted:
    case EditHistoryAction.wordDeletedDictionary:
      // word has been deleted: re-add the word again
      final listModelWords =
          WordListModel(context, database, null, columnId, SortOrder.ascending);

      assert(entry.payload.before is WordEntry);
      final WordEntry word = entry.payload.before as WordEntry;

      final result = await listModelWords.insert(null, word);

      if (result != null) {
        logAddWordAction(
            '${CustomLocalizations.of(context).undoWordDeleted} ${result.word}',
            null,
            result,
            context);
      } else {
        throw RuntimeError('Could not add dataset');
      }
      break;

    case EditHistoryAction.tagAdded:
      // tag has been added: remove the tag again
      final listModelTags = TagListModel(
          context, Config.wordsDatabase, null, columnId, SortOrder.ascending);

      assert(entry.payload.now is TagEntry);
      final TagEntry tag = entry.payload.now as TagEntry;
      final TagEntry currentTag = await listModelTags.getById(tag.id);

      final rowsAffected = (await listModelTags.remove(currentTag, null)) ?? 0;

      if (rowsAffected > 0) {
        logDeleteTagAction(
            '${CustomLocalizations.of(context).undoTagAdded} ${currentTag.tag}',
            currentTag,
            null,
            context);
      } else {
        throw RuntimeError('Could not delete dataset');
      }
      break;

    case EditHistoryAction.tagEdited:
    case EditHistoryAction.tagColorChanged:
    case EditHistoryAction.tagFavoriteChanged:
      // tag has been modified: restore the previous version
      final listModelTags = TagListModel(
          context, Config.wordsDatabase, null, columnId, SortOrder.ascending);

      assert(entry.payload.before is TagEntry);
      assert(entry.payload.now is TagEntry);

      final TagEntry before = entry.payload.before as TagEntry;
      final TagEntry currentTag = await listModelTags.getById(before.id);

      var rowsAffected;

      switch (entry.action) {
        case EditHistoryAction.tagEdited:
          final TagEntry tmp = TagEntry.from(currentTag);

          tmp.tag = before.tag;
          tmp.description = before.description;

          rowsAffected = (await listModelTags.update(tmp)) ?? <int>[0];

          if (atLeastOneRowAffected(rowsAffected)) {
            logEditTagAction('Rückgängig: Schlagwort bearbeitet: ${tmp.tag}',
                currentTag, tmp, context);
          } else {
            throw RuntimeError('Could not update dataset');
          }
          break;

        case EditHistoryAction.tagColorChanged:
          final TagEntry tmp = TagEntry.from(currentTag);

          tmp.color = before.color;

          rowsAffected = (await listModelTags.update(tmp)) ?? 0;

          if (rowsAffected > 0) {
            logChangeTagColorAction('Rückgängig: Farbe geändert: ${tmp.tag}',
                currentTag, tmp, context);
          } else {
            throw RuntimeError('Could not update dataset');
          }
          break;

        case EditHistoryAction.tagFavoriteChanged:
          final TagEntry tmp = TagEntry.from(currentTag);

          tmp.favorite = before.favorite;

          rowsAffected = (await listModelTags.update(tmp)) ?? <int>[0];

          if (atLeastOneRowAffected(rowsAffected)) {
            logChangeTagFavoriteAction(
                'Rückgängig: Favorit geändert: ${tmp.tag}',
                currentTag,
                tmp,
                context);
          } else {
            throw RuntimeError('Could not update dataset');
          }
          break;

        default:
          throw InvalidStateException();
      }
      break;

    case EditHistoryAction.tagDeleted:
      // tag has been deleted: re-add the tag again
      final listModelTags = TagListModel(
          context, Config.wordsDatabase, null, columnId, SortOrder.ascending);

      assert(entry.payload.before is TagEntry);
      final TagEntry tag = entry.payload.before as TagEntry;

      final result = await listModelTags.insert(null, tag);

      if (result != null) {
        logAddTagAction('Rückgängig: Schlagwort gelöscht: ${result.tag}', null,
            result, context);
      } else {
        throw RuntimeError('Could not add dataset');
      }
      break;

    default:
      throw InvalidStateException();
  }
}
