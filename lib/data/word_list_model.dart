/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/data_access_layer_words.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/widgets/custom_animated_list.dart';
import 'package:wortschatz/widgets/word_row.dart';

const STRIDE_SIZE = 1;

class WordListModel {
  final BuildContext context;
  final String database;

  String sortColumn;
  SortOrder sortOrder;
  bool onlyDupesMode;

  bool cacheValid = false;
  int cachedLength;
  Map<int, WordEntry> cachedItems = Map<int, WordEntry>();

  WordListModel(this.context, this.database, this.listKey, this.sortColumn,
      this.sortOrder)
      : dal = DataAccessLayerWords(database, context, sortColumn, sortOrder);

  final DataAccessLayerWords dal;

  GlobalKey<CustomAnimatedListState> listKey;

  CustomAnimatedListState get _animatedList => listKey?.currentState;

  void dispose() {
    dal.close();
  }

  void invalidateCache() {
    cacheValid = false;
    cachedItems.clear();
  }

  void setSortColumn(String sortColumn) {
    this.sortColumn = sortColumn;
    dal.sortColumn = sortColumn;

    invalidateCache();
  }

  void setSortOrder(SortOrder sortOrder) {
    this.sortOrder = sortOrder;
    dal.sortOrder = sortOrder;

    invalidateCache();
  }

  void showOnlyDupes(bool onlyDupesMode) {
    this.onlyDupesMode = onlyDupesMode;
    dal.onlyDupesMode = onlyDupesMode;

    invalidateCache();
  }

  Future<WordEntry> insert(int index, WordEntry item) async {
//    var result = Map<int, WordEntry>();
//
//    for (int i = 0; i < index; i++) {
//      result[i] = cachedItems[i];
//    }
//
//    result[index] = item;
//
//    for (int i = index + 1; i > await length; i++) {
//      result[i + 1] = cachedItems[i];
//    }
//
//    cachedItems = result;

    invalidateCache();

    _animatedList?.insertItem(index,
        duration: Duration(milliseconds: Config.listAnimationDurationMsecs));

    return dal.insert(item);
  }

  Future<int> remove(WordEntry word, index) async {
    // avoid division by zero
    var len = await length;
    if (len == 0) len = 1;

    _animatedList?.removeItem(
        index,
        (BuildContext context, Animation<double> animation) => WordRow(
              word: word,
              animation: animation,
            ),
        duration: Duration(
            milliseconds:
                (Config.listAnimationDurationMsecs + 200 * (index / len))
                    .toInt()));

    invalidateCache();

    final result = await dal.deleteById(word.id);

    return result;
  }

//  Future<WordEntry> removeAt(int index) async {
//    final WordEntry removedItem = cachedItems.remove(index);
//    await dal.deleteById(removedItem.id);
//
//    cachedItems.clear();
//
//    if (removedItem != null) {
//      _animatedList?.removeItem(
//          index,
//          (context, animation) => WordRow(
//                word: removedItem,
//                animation: animation,
//              ),
//          duration: Duration(
//              milliseconds: (Config.listAnimationDurationMsecs + 200 * (index / (await length))).toInt()));
//    }
//
//    return Future.value(removedItem);
//  }

  Future<int> get length async {
    if (cacheValid) {
      // print('Cache hit: length');
      return Future.value(cachedLength);
    } else {
      // print('Cache miss: length');

      cachedLength = await dal.getNumEntries();
      cacheValid = true;

      return Future.value(cachedLength);
    }
  }

  Future<WordEntry> operator [](int index) => _getAt(index);

  Future<WordEntry> _getAt(int index) async {
    var entry = cachedItems[index];

    if (entry == null) {
      // print('Cache miss at: $index');

      final cache = await dal.getEntries(index, STRIDE_SIZE);

      var cnt = 0;
      for (int i = index; i < (index + cache.length); i++) {
        cachedItems[i] = cache[cnt];
        cnt++;
      }
    } else {
      // print('Cache hit!');
    }

    entry = cachedItems[index];

    return Future.value(entry);
  }

  Future<WordEntry> getById(int id) async {
    return dal.getEntryById(id);
  }

  Future update(WordEntry word) async {
    invalidateCache();

    return dal.update(word);
  }

  void setFilter(Map<String, dynamic> filter) {
    dal.setFilter(filter);

    invalidateCache();
  }

  void setTagFilter(List<String> filter) {
    dal.setTagFilter(filter);

    invalidateCache();
  }

  Future<bool> containsWord(String word) async {
    if ((await dal.getEntryByWord(word)) != null)
      return true;
    else
      return false;
  }

  Future<List<WordsPerDay>> getWordsPerDayStats(int numDays) async {
    final maps = await dal.getWordsPerDayStats();

    var result = <WordsPerDay>[];
    maps.getRange(max(0, maps.length - numDays), maps.length).forEach((e) {
      final val = WordsPerDay(
        DateTime.parse(e['date']),
        e['count'],
      );

      result.add(val);
    });

    return result;
  }

  // int indexOf(WordEntry item) => items.indexOf(item);
}
