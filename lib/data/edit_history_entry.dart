/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/edit_history.dart';

class EditHistoryEntry extends MappableEntry {
  int action;
  String description;
  LogPayload payload;
  Color color;
  DateTime createdAt, modifiedAt;

  @override
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnAction: action,
      columnDescription: description,
      columnPayload: payload.toJson(),
      columnCreatedAt: createdAt.toIso8601String(),
      columnModifiedAt: modifiedAt.toIso8601String(),
    };

    if (id != null) {
      map[columnId] = id;
    }

    return map;
  }

  EditHistoryEntry({
    this.action,
    this.description,
    this.payload,
    this.color,
    this.createdAt,
    this.modifiedAt,
  }) : super();

  factory EditHistoryEntry.from(EditHistoryEntry other) => EditHistoryEntry(
        action: other.action,
        description: other.description,
        payload: other.payload,
        color: other.color,
        createdAt: other.createdAt,
        modifiedAt: other.modifiedAt,
      );

  EditHistoryEntry.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    action = map[columnAction];
    description = map[columnDescription];
    try {
      payload = LogPayload.fromJson(jsonDecode(map[columnPayload]));
    } catch (e) {
      print('Payload could not be recovered: ${e.toString()}');
      payload = null;
    }
    if (map[columnColor] == null)
      color = Colors.blueGrey[300];
    else
      color = Color(map[columnColor]);
    createdAt = DateTime.parse(map[columnCreatedAt]);
    modifiedAt = DateTime.parse(map[columnModifiedAt]);
  }

  @override
  String toString() =>
      "EditHistoryEntry('$id', '$action', '$description', '${payload.toString()}', "
      "'${color?.value}', '$createdAt', '$modifiedAt')";

  String toDescription() => "$action- $description";

  String toJson() => '{ "id": $id, "action": "$action", '
      '"description": "$description", "payload": ${payload.toJson()}, '
      '"color": ${color?.value}, '
      '"createdAt": "${createdAt.toIso8601String()}", '
      '"modifiedAt": "${modifiedAt.toIso8601String()}" }';

  EditHistoryEntry.fromJson(Map<String, dynamic> json)
      : action = json['action'],
        description = json['description'],
        payload = LogPayload.fromJson(json['payload']),
        color = Color(json['color']),
        createdAt = DateTime.parse(json['createdAt']),
        modifiedAt = DateTime.parse(json['modifiedAt']),
        super(id: json['id']);
}
