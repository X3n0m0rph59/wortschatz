/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wortschatz/data/data_access_layer_tags.dart';
import 'package:wortschatz/data/data_access_layer_words.dart';
import 'package:wortschatz/data/data_access_layer_words_tags.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/words_tags_entry.dart';
import 'package:wortschatz/widgets/message_bar.dart';

Future<void> addExampleContentWords(
    DataAccessLayerWords dal, BuildContext context) async {
  await dal.insert(WordEntry(
    word: 'Etymologie',
    description: 'Die Herkunft eines Wortes',
    color: Colors.pink,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Zucchini',
    description: 'Ein Gemüse',
    color: Colors.blue,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Nämlich',
    description: 'Wer nämlich mit h schreibt, ist dämlich',
    color: Colors.deepOrange,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Desoxyribonukleinsäure',
    description: 'DNS',
    color: Colors.orange,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Wechselwirkungsmechanismus',
    description: 'Abstrakter Begriff',
    color: Colors.green,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Chrysantheme',
    description: 'Eine Pflanze',
    color: Colors.redAccent,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Akquisition',
    description: 'Eine Erwerbung',
    color: Colors.pinkAccent,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Atmosphäre',
    description: 'Griechisches Wort',
    color: Colors.indigoAccent,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'aufwendig',
    description: 'Empfohlene Schreibweise',
    color: Colors.purpleAccent,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Chrysantheme',
    description: 'Eine Pflanze',
    color: Colors.redAccent,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Amurtiger',
    description: 'Unterart der Tiger',
    color: Colors.white,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordEntry(
    word: 'Clementine',
    description: 'Mandarinenähnliche Zitrusfrucht',
    color: Colors.deepOrangeAccent,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  MessageBar.info(title: 'Info', message: 'Beispieldaten wurden hinzugefügt')
    ..show(context);

  return Future.value();
}

Future<void> addExampleContentTags(
    DataAccessLayerTags dal, BuildContext context) async {
  await dal.insert(TagEntry(
    tag: 'Beispiel',
    description: 'Beispiel Inhalt',
    color: Colors.pink,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Zu Lernen',
    description: 'Auf der zu lernen Liste',
    color: Colors.blue,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Fremdsprache',
    description: 'Wort stammt nicht aus der deutschen Sprache',
    color: Colors.deepOrange,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Eingedeutscht',
    description: 'Wort war ursprünglich nicht deutsch',
    color: Colors.orange,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Deutsch',
    description: 'Wort stammt aus der deutschen Sprache',
    color: Colors.green,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Wichtig',
    description: 'Wichtiger Eintrag',
    color: Colors.redAccent,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Unwichtig',
    description: 'Eintrag mit niedriger Priorität',
    color: Colors.grey,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Problemwort',
    description: 'Mehr Training notwendig',
    color: Colors.pinkAccent,
    favorite: true,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(TagEntry(
    tag: 'Importiert',
    description: 'Eintrag wurde importiert',
    color: Colors.lightGreenAccent,
    favorite: false,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  return Future.value();
}

Future<void> addExampleContentWordsTags(
    DataAccessLayerWordsTags dal, BuildContext context) async {
  await dal.insert(WordsTagsEntry(
    wordId: 1,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 2,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 3,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 4,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 5,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 6,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 7,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 8,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 9,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 10,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 11,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 12,
    tagId: 1,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 1,
    tagId: 3,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 3,
    tagId: 2,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 4,
    tagId: 6,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 6,
    tagId: 4,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 7,
    tagId: 2,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  await dal.insert(WordsTagsEntry(
    wordId: 9,
    tagId: 2,
    createdAt: DateTime.now(),
    modifiedAt: DateTime.now(),
  ));

  return Future.value();
}
