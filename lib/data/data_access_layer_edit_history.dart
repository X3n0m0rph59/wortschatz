/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/edit_history_entry.dart';
import 'package:wortschatz/data/exceptions/invalid_entry_type.dart';

class DataAccessLayerEditHistory extends DataAccessLayer<EditHistoryEntry> {
  DataAccessLayerEditHistory(String databaseFilePath, BuildContext context,
      String sortColumn, SortOrder sortOrder)
      : super(databaseFilePath, Tables.editHistory, context, sortColumn,
            sortOrder);

  @override
  String get currentTableName => tableNameEditHistory;

  List<String> get defaultColumns => <String>[
        columnId,
        columnAction,
        columnDescription,
        columnPayload,
        columnCreatedAt,
        columnModifiedAt,
      ];

  Future<EditHistoryEntry> insert(EditHistoryEntry entry) async {
    print('Inserting: ${entry.toString()}');

    final db = await database;

    await db.transaction((txn) async {
      entry.createdAt = DateTime.now();
      entry.modifiedAt = DateTime.now();

      entry.id = await txn.insert(currentTableName, entry.toMap());
    });

    return entry;
  }

  Future<EditHistoryEntry> getEntry(int index) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      var w;
      if (where != null && where.isNotEmpty) w = where;

      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        offset: index,
        limit: 1,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: w,
        whereArgs: whereArgs,
      );
    });

    if (maps.length > 0) {
      if (table == Tables.editHistory) {
        return Future.value(EditHistoryEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<List<EditHistoryEntry>> getEntries(int startIndex, int count) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      var w;
      if (where != null && where.isNotEmpty) w = where;

      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        offset: startIndex,
        limit: count,
        orderBy: '_id ${sortOrder == SortOrder.ascending ? 'ASC' : 'DESC'}',
        where: w,
        whereArgs: whereArgs,
      );
    });

    var result = List<EditHistoryEntry>();
    maps.forEach((e) {
      if (table == Tables.editHistory) {
        result.add(EditHistoryEntry.fromMap(e));
      } else {
        throw InvalidEntryTypeException();
      }
    });

    return Future.value(result);
  }

  Future<EditHistoryEntry> getEntryById(int id) async {
    final db = await database;

    List<Map> maps;

    await db.transaction((txn) async {
      maps = await txn.query(
        currentTableName,
        columns: defaultColumns,
        where: '$columnId = ?',
        whereArgs: [id],
      );
    });

    if (maps.length > 0) {
      if (table == Tables.editHistory) {
        return Future.value(EditHistoryEntry.fromMap(maps.first));
      } else {
        throw InvalidEntryTypeException();
      }
    } else {
      return null;
    }
  }

  Future<int> deleteById(int id) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      result =
          txn.delete(currentTableName, where: '$columnId = ?', whereArgs: [id]);
    });

    return result;
  }

  Future<int> update(EditHistoryEntry entry) async {
    final db = await database;

    var result;
    await db.transaction((txn) async {
      entry.modifiedAt = DateTime.now();

      result = txn.update(currentTableName, entry.toMap(),
          where: '$columnId = ?', whereArgs: [entry.id]);
    });

    return result;
  }

  Future<List<Map<String, dynamic>>> getEditsPerDayStats() async {
    final sql =
        'select count($columnId) as count, substr($columnCreatedAt,0,11) as date '
        'from $tableNameEditHistory group by substr($columnCreatedAt,0,11) order by date asc';

    final db = await database;

    var result;
    await db.transaction((txn) async {
      result = await txn.rawQuery(sql);
    });

    return result;
  }
}

class EditsPerDay {
  final DateTime date;
  final int wordCount;

  EditsPerDay(this.date, this.wordCount);
}
