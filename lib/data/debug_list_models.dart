/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/widgets/custom_animated_list.dart';
import 'package:wortschatz/widgets/word_row.dart';

class DebugWordListModel {
  Map<int, WordEntry> _items = Map<int, WordEntry>();

  GlobalKey<CustomAnimatedListState> listKey;

  CustomAnimatedListState get _animatedList => listKey.currentState;

  DebugWordListModel(this.listKey) {
    for (int i = 0; i < 10; i++) {
      _items[i] = WordEntry(
        // id: i,
        word: 'Word $i',
        description: 'description $i',
        color: Colors.primaries[i],
        favorite: true,
        tags: Future.value(<TagEntry>[]),
      );
    }
  }

  Future<void> initialize() async {
    return Future.value();
  }

  Future<WordEntry> insert(int index, WordEntry item) async {
    _items[index] = item;
    _animatedList.insertItem(index,
        duration: Duration(milliseconds: Config.listAnimationDurationMsecs));

    return Future.value(item);
  }

  Future<void> remove(WordEntry word, index) async {
    _items.remove(word.id);

    _animatedList.removeItem(
        index,
        (BuildContext context, Animation<double> animation) => WordRow(
              word: word,
              animation: animation,
            ),
        duration: Duration(
            milliseconds: (Config.listAnimationDurationMsecs +
                    200 * (index / (await length)))
                .toInt()));

    return Future.value();
  }

//  Future<WordEntry> removeAt(int index) async {
//    final WordEntry removedItem = cachedItems.remove(index);
//    await dal.deleteById(removedItem.id);
//
//    cachedItems.clear();
//
//    if (removedItem != null) {
//      _animatedList.removeItem(
//          index,
//          (context, animation) => WordRow(
//                word: removedItem,
//                animation: animation,
//              ),
//          duration: Duration(
//              milliseconds: (150 + 200 * (index / (await length))).toInt()));
//    }
//
//    return Future.value(removedItem);
//  }

  Future<int> get length async => _items.length;

  Future<WordEntry> operator [](int index) => _getAt(index);

  Future<WordEntry> _getAt(int index) async {
    var entry = _items[index];

    return Future.value(entry);
  }

  Future<int> update(WordEntry word) async {
    return Future.value(0);
  }

  void setFilter(Map<String, dynamic> filter) {}

// int indexOf(WordEntry item) => items.indexOf(item);

  WordEntry getAt(int index) {
    return _items[index];
  }
}
