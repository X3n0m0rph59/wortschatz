/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/dialogs/add_edit_db_profile_dialog.dart';
import 'package:wortschatz/exceptions/value_error.dart';
import 'package:wortschatz/pages/main_page.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/database_profiles.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/custom_icon_button.dart';
import 'package:wortschatz/widgets/custom_tool_icon_button.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';
import 'package:wortschatz/widgets/message_bar.dart';

class DatabaseSelectorPage extends StatefulWidget {
  DatabaseSelectorPage({Key key}) : super(key: key);

  @override
  _DatabaseSelectorPageState createState() => _DatabaseSelectorPageState();
}

class _DatabaseSelectorPageState extends State<DatabaseSelectorPage>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  final sharedPreferences = SharedPreferences.getInstance();

  var databaseProfiles = DatabaseProfileManager().getDatabaseProfiles();
  String _databaseProfile;

  @override
  void initState() {
    super.initState();

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    _databaseProfile = await DatabaseProfileManager().currentDatabaseProfile;

    return Future.value();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: buildDrawer(context, DrawerItemIndex.databaseSelectorPage),
        body: Builder(
          // Create an inner BuildContext so that methods
          // can refer to the Scaffold with Scaffold.of().
          builder: (BuildContext context) {
            return BackgroundImage(
              assetName: 'assets/images/background.png',
              child: SingleChildScrollView(
                child: Stack(
                  children: <Widget>[
                    _buildImage(),
                    _buildTopHeader(context),
                    _buildProfileRow(),
                    _buildBottomPart(),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> _onWillPop() {
    return Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => MainPage()), (p) => false);
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Positioned.fill(
        bottom: null,
        child: ClipPath(
          clipper: DiagonalClipper(),
          child: Image.asset(
            assetName,
            fit: BoxFit.cover,
            height: _imageHeight,
          ),
        ),
      );
    });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                'Datenbank Manager',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    return Padding(
      padding: EdgeInsets.only(top: _imageHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          _buildContent(),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: EdgeInsets.only(left: 64, right: 16, top: 16),
      child: FutureBuilder<List<String>>(
        future: databaseProfiles,
        builder: (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            return Column(
              children: <Widget>[
                Column(
                  children: _buildDatabaseProfileList(snapshot.data),
                ),
                ButtonBar(
                  children: <Widget>[
                    CustomIconButton(
                        icon: Icons.add,
                        onPressed: () {
                          _addProfile();
                        }),
                  ],
                ),
              ],
            );
          } else {
            return Text('Wird geladen...');
          }
        },
      ),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 64.0, top: Config.simpleHeaderPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Datenbank Profil',
            style: TextStyle(fontSize: 34.0),
          ),
          Text(
            'Ermöglicht die Nutzung mehrerer verschiedener Datenbanken '
            'mit jeweils eigenem Wortschatz, Schlagwörtern, Historie sowie eigenem Wörterbuch',
            style: TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildDatabaseProfileList(List<String> profiles) {
    final result = List<Widget>();

    for (final profile in profiles) {
      if (profile == _databaseProfile) {
        result.add(
          InkWell(
            onLongPress: () => _onProfileLongPress(profile),
            child: RadioListTile<String>(
              isThreeLine: false,
              title: Text(profile),
              value: profile,
              groupValue: _databaseProfile,
              onChanged: (String value) {
                setState(() {
                  DatabaseProfileManager().setCurrentDatabaseProfile(value);
                  _databaseProfile = value;
                });
              },
            ),
          ),
        );
      } else {
        result.add(
          InkWell(
            onLongPress: () => _onProfileLongPress(profile),
            child: RadioListTile<String>(
              isThreeLine: false,
              title: Text(profile),
              secondary: ButtonBar(
                alignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  CustomToolIconButton(
                    child: Icon(Icons.remove),
                    onPressed: () async {
                      bool result = await showDialog<bool>(
                            context: context,
                            barrierDismissible: false, // user must tap button!
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('Profil wirklich löschen?'),
                                content: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      Text(
                                          'Möchtest du wirklich das Profil $profile endgültig löschen? '
                                          'Diese Änderung kann nicht rückgängig gemacht werden!'),
                                    ],
                                  ),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Abbrechen'),
                                    onPressed: () {
                                      Navigator.of(context).pop(false);
                                    },
                                  ),
                                  FlatButton(
                                    child: Text('Löschen'),
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                    },
                                  ),
                                ],
                              );
                            },
                          ) ??
                          false;

                      if (result) {
                        setState(() {
                          DatabaseProfileManager()
                              .deleteDatabaseProfile(profile);
                          databaseProfiles =
                              DatabaseProfileManager().getDatabaseProfiles();
                        });
                      }
                    },
                  ),
                ],
              ),
              value: profile,
              groupValue: _databaseProfile,
              onChanged: (String value) {
                setState(() {
                  DatabaseProfileManager().setCurrentDatabaseProfile(value);
                  _databaseProfile = value;
                });
              },
            ),
          ),
        );
      }
    }

    return result;
  }

  void _onProfileLongPress(String profile) async {
    final result =
        await showAddEditDatabaseProfileDialog(context, profileName: profile);

    try {
      if (result.action ==
          AddEditDatabaseProfileDialogResultAction.ActionSave) {
        final newProfileName = sanitizeProfileName(result.profileName);

        await DatabaseProfileManager()
            .renameDatabaseProfile(profile, newProfileName);
        await DatabaseProfileManager()
            .setCurrentDatabaseProfile(newProfileName);

        setState(() {
          _databaseProfile = newProfileName;
          databaseProfiles = DatabaseProfileManager().getDatabaseProfiles();
        });
      }
    } on ValueError catch (e) {
      MessageBar.warning(
          title: 'Warnung',
          message: 'Ungültige Eingabe wurde ignoriert. ${e.message}')
        ..show(context);
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Profil konnte nicht umbenannt werden')
        ..show(context);
    }
  }

  void _addProfile() async {
    final result = await showAddEditDatabaseProfileDialog(context);

    try {
      if (result.action ==
          AddEditDatabaseProfileDialogResultAction.ActionSave) {
        final profileName = sanitizeProfileName(result.profileName);

        await DatabaseProfileManager().addDatabaseProfile(profileName);
        await DatabaseProfileManager().setCurrentDatabaseProfile(profileName);

        setState(() {
          _databaseProfile = profileName;
          databaseProfiles = DatabaseProfileManager().getDatabaseProfiles();
        });
      }
    } on ValueError catch (e) {
      MessageBar.warning(
          title: 'Warnung',
          message: 'Ungültige Eingabe wurde ignoriert. ${e.message}')
        ..show(context);
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Profil konnte nicht angelegt werden')
        ..show(context);
    }
  }
}
