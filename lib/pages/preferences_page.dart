/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/exceptions/runtime_error.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/custom_animated_list.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';

class PreferencesPage extends StatefulWidget {
  PreferencesPage({Key key}) : super(key: key);

  @override
  _PreferencesPageState createState() => _PreferencesPageState();
}

class _PreferencesPageState extends State<PreferencesPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  final sharedPreferences = SharedPreferences.getInstance();

  final scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    return Future.value();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context, DrawerItemIndex.preferencesPage),
      body: Builder(
        // Create an inner BuildContext so that methods
        // can refer to the Scaffold with Scaffold.of().
        builder: (BuildContext context) {
          return BackgroundImage(
            assetName: 'assets/images/background.png',
            child: Stack(
              children: <Widget>[
                _buildImage(),
                _buildTopHeader(context),
                _buildProfileRow(),
                _buildBottomPart(),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Positioned.fill(
        bottom: null,
        child: ClipPath(
          clipper: DiagonalClipper(),
          child: Image.asset(
            assetName,
            fit: BoxFit.cover,
            height: _imageHeight,
          ),
        ),
      );
    });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                CustomLocalizations.of(context).headerPreferences,
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    return Padding(
      padding: EdgeInsets.only(top: _imageHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          _buildContent(),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: EdgeInsets.only(left: 64, right: 16, top: 16),
      child: FutureBuilder<SharedPreferences>(
        future: sharedPreferences,
        builder:
            (BuildContext context, AsyncSnapshot<SharedPreferences> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
//              final TextEditingController controllerUserName =
//                  TextEditingController(
//                      text: snapshot.data.getString('username'));

            return SizedBox(
              height: (MediaQuery.of(context).size.height -
                  (Config.listViewTopPadding +
                      Config.listViewTopInternalPadding +
                      Config.simpleHeaderPadding)),
              child: CustomAnimatedList(
                controller: scrollController,
                initialItemCount: 1,
                itemBuilder:
                    (BuildContext context, int index, Animation animation) {
                  switch (index) {
                    case 0:
                      return FutureBuilder(
                          future: _getOption(index),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.connectionState ==
                                    ConnectionState.done &&
                                snapshot.data != null) {
                              return CheckboxListTile(
                                value: snapshot.data,
                                title: Text(CustomLocalizations.of(context)
                                    .enableDarkMode),
                                subtitle: Text(CustomLocalizations.of(context)
                                    .restartRequired),
                                onChanged: (bool value) =>
                                    _setOption(index, value),
                              );
                            } else {
                              return Container();
                            }
                          });
                  }
                },
              ),
            );
          } else {
            return Text(CustomLocalizations.of(context).loading);
          }
        },
      ),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 64.0, top: Config.simpleHeaderPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            CustomLocalizations.of(context).headerPreferences,
            style: TextStyle(fontSize: 34.0),
          ),
          Text(
            CustomLocalizations.of(context).subHeaderPreferences,
            style: TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ],
      ),
    );
  }

  Future<bool> _getOption(int index) async {
    switch (index) {
      case 0:
        var prefs = await sharedPreferences;

        return prefs.getBool('dark_ui_mode') ?? Config.defaultDarkUi;
        break;
    }

    throw RuntimeError('Invalid state');
  }

  void _setOption(int index, bool value) async {
    switch (index) {
      case 0:
        var prefs = await sharedPreferences;
        prefs.setBool('dark_ui_mode', value);

        DynamicTheme.of(context).setBrightness(
          value ? Brightness.dark : Brightness.light,
        );
        break;
    }

    setState(() {});
  }
}
