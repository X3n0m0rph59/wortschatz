/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/word_list_model.dart';
import 'package:wortschatz/dialogs/add_edit_word_dialog.dart';
import 'package:wortschatz/dialogs/color_picker_dialog.dart';
import 'package:wortschatz/dialogs/search_dialog.dart';
import 'package:wortschatz/dialogs/word_properties_dialog.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/animation.dart';
import 'package:wortschatz/util/edit_history.dart';
import 'package:wortschatz/util/format.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/animated_menu_fab.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/custom_animated_list.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';
import 'package:wortschatz/widgets/message_bar.dart';
import 'package:wortschatz/widgets/word_row.dart';

class DictionaryPage extends StatefulWidget {
  DictionaryPage({Key key}) : super(key: key);

  @override
  _DictionaryPageState createState() => _DictionaryPageState();
}

class _DictionaryPageState extends State<DictionaryPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<CustomAnimatedListState> _listKey =
      GlobalKey<CustomAnimatedListState>();

  var _headerKey = GlobalKey();
  var _clipperKey = GlobalKey();

  double _headerPadding = Config.headerPadding;
  double _headerOpacity;
  double _clipperHeight = 0.0;

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  final _listViewScrollController = ScrollController();

  WordListModel listModel;

  bool showOnlyFavorites = false;

  final sharedPreferences = SharedPreferences.getInstance();

  String lastCachedWord;

  Map<String, dynamic> filter = Map<String, dynamic>();
  List<String> filterTags = List<String>();

  WordEntry lastRemovedWord;
  MessageBar lastMessageBar;

  @override
  void initState() {
    super.initState();

    listModel = WordListModel(context, Config.dictionaryDatabase, _listKey,
        columnId, SortOrder.ascending);

    _listViewScrollController.addListener(() => _onListScrolled());

    initAsyncState();
  }

  void _onListScrolled() {
    final _scrollOffset = _listViewScrollController.offset;

    if (_clipperKey != null && _clipperKey.currentState != null) {
      _clipperKey.currentState.setState(() {
        _clipperHeight = offsetPosition(_scrollOffset,
                    _listViewScrollController.position.viewportDimension, 15.0)
                .abs() *
            Config.clipperHeight;
      });
    }

    if (_headerKey != null && _headerKey.currentState != null) {
      _headerKey.currentState.setState(() {
        _headerPadding = max(
                Config.headerPaddingFactor,
                1.0 -
                    offsetPosition(
                            _scrollOffset,
                            _listViewScrollController
                                .position.viewportDimension,
                            3.0)
                        .abs()) *
            Config.headerPadding;

        _headerOpacity = max(
            0.0,
            1.0 -
                offsetPosition(
                        _scrollOffset,
                        _listViewScrollController.position.viewportDimension,
                        5.0)
                    .abs());
      });
    }
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    listModel.setFilter(filter);

    return Future.value();
  }

  @override
  void dispose() {
    listModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: buildDrawer(context, DrawerItemIndex.dictionaryPage),
        resizeToAvoidBottomPadding: false,
        body: Builder(
          // Create an inner BuildContext so that methods
          // can refer to the Scaffold with Scaffold.of().
          builder: (BuildContext context) {
            return BackgroundImage(
              assetName: 'assets/images/background.png',
              child: Stack(
                children: <Widget>[
                  _buildLine(),
                  _buildBottomPart(),
                  _buildImage(),
                  _buildTopHeader(context),
                  _buildProfileRow(),
                  _buildFab(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> _onWillPop() {
    if (filter.length > 0 || filterTags.length > 0) {
      setState(() {
        filter.clear();
        listModel.setFilter(filter);

        filterTags.clear();
        listModel.setTagFilter(filterTags);
      });

      return Future.value(false);
    } else
      return Future.value(true);
  }

  Widget _buildFab() {
    return Positioned(
        top: _imageHeight - 100.0,
        right: -40.0,
        child: AnimatedMenuFab(
          isOpened: false,
          onFavoritesClick: _toggleShowOnlyFavorites,
          onAddClick: _addNewWordDialog,
//        onFilterTagsClick: _setFilterTags,
          onSearchClick: _setFilter,
        ));
  }

  void _addNewWordDialog() async {
    bool repeat = false;

    do {
      var result = await showAddEditWordDialog(context);
      repeat = result.action == AddEditWordDialogResultAction.ActionSaveAndNew;

      if (result != null &&
          (result.action == AddEditWordDialogResultAction.ActionSave ||
              result.action ==
                  AddEditWordDialogResultAction.ActionSaveAndNew)) {
        if (result.word.word.trim().length > 0) {
          await listModel.insert(0, result.word);

          logAddDictionaryWordAction(
              '${result.word.word}', null, result.word, context);
        } else {
          MessageBar.warning(
              title: CustomLocalizations.of(context).warning,
              message: CustomLocalizations.of(context).invalidInputIgnored,
              duration: Duration(seconds: Config.snackBarDurationShortSecs))
            ..show(context);
        }
      } else if (result == null ||
          result.word == null ||
          result.word.word.trim().length == 0 ||
          result.action == AddEditWordDialogResultAction.ActionCancel) {
        break;
      }
    } while (repeat);
  }

  void _editWordDialog(WordEntry word, StateSetter setState) async {
    final WordEntry before = WordEntry.from(word);

    var result = await showAddEditWordDialog(context, word: word);
    if (result != null &&
        result.action == AddEditWordDialogResultAction.ActionSave) {
      if (result.word.word.trim().length > 0) {
        word.word = result.word.word;
        word.description = result.word.description;

        await listModel.update(word);

        logEditDictionaryWordAction(
            '${result.word.word} - ${result.word.description}',
            before,
            result.word,
            context);

        setState(() {});
      } else {
        MessageBar.warning(
            title: CustomLocalizations.of(context).warning,
            message: CustomLocalizations.of(context).invalidInputIgnored,
            duration: Duration(seconds: Config.snackBarDurationShortSecs))
          ..show(context);
      }
    }
  }

  void _colorPickerDialog(WordEntry word, StateSetter setState) async {
    final WordEntry before = WordEntry.from(word);

    var result = await showColorPickerDialog(context, word.color);
    if (result != null && result.action == ColorPickerResultAction.ActionSave) {
      setState(() {
        word.color = result.color;
      });

      await listModel.update(word);

      logChangeDictionaryWordColorAction(
          '${word.word} - ${result.color.toString()}', before, word, context);
    }
  }

  void _wordPropertiesDialog(WordEntry word, StateSetter setState) async {
    await showWordPropertiesDialog(context, word);
  }

//  void _tagSelectionDialog(WordEntry word, StateSetter setState) async {
//    await showTagSelectionDialog(context, word);
//
//    await listModel.update(word);
//
//    // disable this for now, since we log each tag action separately
//    // logChangeDictionaryWordTagAction('${word.word}', word, context);
//
//    setState(() {});
//  }

  void _toggleShowOnlyFavorites() {
    showOnlyFavorites = !showOnlyFavorites;

    setState(() {
      filter.clear();

      if (showOnlyFavorites) {
        filter = Map<String, dynamic>();
        filter[columnFavorite] = showOnlyFavorites ? '1' : '0';
      }

      listModel.setFilter(filter);
    });
  }

  bool _toggleFavorite(WordEntry word, StateSetter setState) {
    final WordEntry before = WordEntry.from(word);

    word.favorite = !word.favorite;
    listModel.update(word);

    logChangeDictionaryWordFavoriteAction(
        '${word.word}: ${boolToString(word.favorite)}', before, word, context);

    return word.favorite;
  }

  void _deleteWord(int index, WordEntry word, StateSetter _setState) async {
    await listModel.remove(word, index);

    logDeleteDictionaryWordAction('${word.word}', word, null, context);

    lastRemovedWord = word;

    lastMessageBar = MessageBar.infoWithAction(
      title: CustomLocalizations.of(context).info,
      message: CustomLocalizations.of(context).wordHasBeenDeleted,
      duration: Duration(seconds: Config.snackBarDurationLongSecs),
      onAction: () => _undoDeleteWord(),
      actionChild: Text(
        CustomLocalizations.of(context).undo,
        style: TextStyle(
          color: Colors.grey[200],
        ),
      ),
    )..show(context);

    setState(() {});
  }

  void _undoDeleteWord() async {
    if (lastRemovedWord != null) {
      await listModel.insert(0, lastRemovedWord);

      logAddDictionaryWordAction(
          '${CustomLocalizations.of(context).undoWordDeleted} ${lastRemovedWord.word}',
          null,
          lastRemovedWord,
          context);
    }

    setState(() {
      lastRemovedWord = null;
    });

    lastMessageBar.dismiss();
  }

//  void _setFilterTags() async {
//    var result = await showSearchByTagDialog(context, text: '');
//    if (result != null &&
//        result.action == SearchByTagDialogResultAction.ActionSearch) {
//      var text = result.text;
//
//      setState(() {
//        filterTags = <String>['%$text%'];
//
//        listModel.setTagFilter(filterTags);
//      });
//    }
//  }

  void _setFilter() async {
    var result = await showSearchDialog(context, text: '');
    if (result != null &&
        result.action == SearchDialogResultAction.ActionSearch) {
      var text = result.text;

      setState(() {
        filter.clear();

        filter = Map<String, dynamic>();
        filter[columnWord] = '%$text%';
        // filter[columnDescription] = '%$text%';

        listModel.setFilter(filter);
      });
    }
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        key: _clipperKey,
        builder: (BuildContext context, StateSetter setState) {
          return Positioned.fill(
            bottom: null,
            child: ClipPath(
              clipper: DiagonalClipper(height: _clipperHeight),
              child: Image.asset(
                assetName,
                fit: BoxFit.cover,
                height: _imageHeight,
              ),
            ),
          );
        });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                CustomLocalizations.of(context).headerDictionary,
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    final wordList = _buildWordList();

    return Stack(
      children: <Widget>[
        FutureBuilder<Widget>(
          future: wordList,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.data != null) {
              return snapshot.data;
            } else {
              return Container();
            }
          },
        ),
        _buildHeader(),
      ],
    );
  }

  Future<Text> _buildLabelText(offset) async {
    final len = await listModel.length;

    final item = await listModel[((len - 1) *
            ((offset - Config.listViewTopInternalPadding) / (len * 0.74)) /
            100)
        .round()];
    final text = item?.word;

    lastCachedWord = text;

    return Text(text, style: TextStyle(color: Colors.white));
  }

  Future<Widget> _buildWordList() async {
    final len = await listModel.length;

    return Padding(
      padding: EdgeInsets.only(top: Config.listViewTopPadding),
      child: DraggableScrollbar.semicircle(
        // alwaysVisibleScrollThumb: true,
        backgroundColor: Colors.pink,
        scrollbarTimeToFade: Duration(milliseconds: 1250),
        labelTextBuilder: (double offset) {
          final labelText = _buildLabelText(offset);

          return FutureBuilder<Text>(
            future: labelText,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.data != null) {
                return snapshot.data;
              } else {
                return Text(lastCachedWord ??= '',
                    style: TextStyle(color: Colors.white));

                // return Text('...', style: TextStyle(color: Colors.white));
              }
            },
          );
        },
        labelConstraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width - 50,
          maxHeight: 50,
        ),
        controller: _listViewScrollController,
        child: CustomAnimatedList(
          // primary: true,
          padding: EdgeInsets.only(top: Config.listViewTopInternalPadding),
          initialItemCount: len,
          // INFO: Always set itemExtent to improve performance on huge lists
          itemExtent: 74,
          key: _listKey,
          controller: _listViewScrollController,
          itemBuilder: (context, index, animation) {
            var result = listModel[index];

            return FutureBuilder<WordEntry>(
              future: result,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.data != null) {
                  return StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return WordRow(
                      word: snapshot.data,
                      animation: animation,
                      onTap: (word) => _wordPropertiesDialog(word, setState),
                      onLongPress: (word) => _editWordDialog(word, setState),
                      onEditAction: (word) => _editWordDialog(word, setState),
                      onChangeColorAction: (word) =>
                          _colorPickerDialog(word, setState),
//                    onEditTagsAction: (word) =>
//                        _tagSelectionDialog(word, setState),
                      onFavoriteAction: (word) =>
                          _toggleFavorite(word, setState),
                      onDeleteAction: (word) =>
                          _deleteWord(index, word, setState),
                    );
                  });
                } else {
                  return StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return WordRow.waiting(
                      animation: animation,
                    );
                  });
                }
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildHeader() {
    final len = listModel.length;
    final String filterHint = filter.length > 0 || filterTags.length > 0
        ? CustomLocalizations.of(context).filtered
        : '';

    return FutureBuilder<int>(
      key: _headerKey,
      future: len,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          final String text = snapshot.data == 1
              ? CustomLocalizations.of(context).wordOne
              : CustomLocalizations.of(context).wordMany;

          return Padding(
            padding: EdgeInsets.only(left: 64.0, top: _headerPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: _headerOpacity ??= 1.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            CustomLocalizations.of(context).listTitleDictionary,
                            style: TextStyle(fontSize: 34.0),
                          ),
                          Text(
                            '${formatNumber(snapshot.data)} $text $filterHint',
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        } else {
          return Text('');
        }
      },
    );
  }

  Widget _buildLine() {
    return Positioned(
      top: 0.0,
      bottom: 0.0,
      left: 32.0,
      child: Container(
        width: 2.0,
        color: Theme.of(context).brightness == Brightness.dark
            ? Colors.grey[800]
            : Colors.grey[300],
      ),
    );
  }
}
