/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/exceptions/import_export_error.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';
import 'package:wortschatz/widgets/message_bar.dart';

class UserProfilePage extends StatefulWidget {
  UserProfilePage({Key key}) : super(key: key);

  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  final sharedPreferences = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    return Future.value();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context, DrawerItemIndex.profilePage),
      body: Builder(
        // Create an inner BuildContext so that methods
        // can refer to the Scaffold with Scaffold.of().
        builder: (BuildContext context) {
          return BackgroundImage(
            assetName: 'assets/images/background.png',
            child: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  _buildImage(),
                  _buildTopHeader(context),
                  _buildProfileRow(),
                  _buildBottomPart(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Positioned.fill(
        bottom: null,
        child: ClipPath(
          clipper: DiagonalClipper(),
          child: Image.asset(
            assetName,
            fit: BoxFit.cover,
            height: _imageHeight,
          ),
        ),
      );
    });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                'Benutzerprofil',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
//        onTap: () => Navigator.push(
//              context,
//              MaterialPageRoute(builder: (context) => UserProfilePage()),
//            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    return Padding(
      padding: EdgeInsets.only(top: _imageHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          _buildContent(),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return FutureBuilder<SharedPreferences>(
      future: sharedPreferences,
      builder:
          (BuildContext context, AsyncSnapshot<SharedPreferences> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          final TextEditingController controllerUserName =
              TextEditingController(
                  text: snapshot.data.getString('username') ?? '');

          controllerUserName.selection = TextSelection(
              baseOffset: 0,
              extentOffset: snapshot.data.getString('username')?.length);

          return Column(
            children: <Widget>[
              Center(
                child: GestureDetector(
                  onTap: () => _selectAvatar(),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: FutureBuilder(
                        future: _avatarImage,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.done &&
                              snapshot.data != null) {
                            return CircleAvatar(
                              backgroundImage: snapshot.data,
                              minRadius: 59,
                              maxRadius: 59,
                            );
                          } else {
                            final image =
                                AssetImage('assets/images/avatar.png');

                            return CircleAvatar(
                              backgroundImage: image,
                              minRadius: 59,
                              maxRadius: 59,
                            );
                          }
                        }),
                  ),
                ),
              ),
              Form(
                key: _formKey,
                autovalidate: true,
                child: Padding(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        maxLength: 32,
                        maxLengthEnforced: true,
                        validator: (text) {
                          if (text.length < 1)
                            return 'Ungültiger Benutzername';
                          else
                            return "";
                        },
                        onEditingComplete: () {
                          setState(() {
                            snapshot.data
                                .setString('username', controllerUserName.text);
                          });
                        },
                        controller: controllerUserName,
                        autofocus: true,
                        autocorrect: true,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          // labelText: 'Benutzername',
                          filled: true,
                          prefixIcon: Icon(Icons.person),
                          hintText: 'Benutzername',
                          contentPadding: EdgeInsets.all(16),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(16.0))),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        } else {
          return Text('Wird geladen...');
        }
      },
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 64.0, top: Config.simpleHeaderPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Benutzerprofil',
            style: TextStyle(fontSize: 34.0),
          ),
          Text(
            'Informationen über den aktuellen Benutzer',
            style: TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ],
      ),
    );
  }

  void _selectAvatar() async {
    _avatarImage = _importAvatar();

    setState(() {});
  }

  Future<ImageProvider> _importAvatar() async {
    try {
      final FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: ['png', 'jpg'],
        allowedMimeTypes: <String> ['image/*'],
        invalidFileNameSymbols: ['/'],
      );

      final path = await FlutterDocumentPicker.openDocument(params: params);

      final prefs = await sharedPreferences;
      prefs.setString('avatar_file', path);

      if (path != null && path.isNotEmpty) {
        return FileImage(File(path));
      } else {
        return null;
      }
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Konnte das Bild nicht importieren')
        ..show(context);

      throw ImportExportError('Could not import data');
    }
  }
}
