/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/tag_list_model.dart';
import 'package:wortschatz/dialogs/add_edit_tag_dialog.dart';
import 'package:wortschatz/dialogs/color_picker_dialog.dart';
import 'package:wortschatz/dialogs/search_tag_dialog.dart';
import 'package:wortschatz/dialogs/tag_properties_dialog.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/animation.dart';
import 'package:wortschatz/util/edit_history.dart';
import 'package:wortschatz/util/format.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/animated_menu_fab.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/custom_animated_list.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';
import 'package:wortschatz/widgets/message_bar.dart';
import 'package:wortschatz/widgets/tag_row.dart';

class TagsPage extends StatefulWidget {
  TagsPage({Key key}) : super(key: key);

  @override
  _TagsPageState createState() => _TagsPageState();
}

class _TagsPageState extends State<TagsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<CustomAnimatedListState> _listKey =
      GlobalKey<CustomAnimatedListState>();

  var _headerKey = GlobalKey();
  var _clipperKey = GlobalKey();

  double _headerPadding = Config.headerPadding;
  double _headerOpacity;
  double _clipperHeight = 0.0;

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  final _listViewScrollController = ScrollController();

  TagListModel listModel;

  bool showOnlyFavorites = false;

  final sharedPreferences = SharedPreferences.getInstance();

  String lastCachedTag;

  Map<String, dynamic> filter = Map<String, dynamic>();

  TagEntry lastRemovedTag;
  MessageBar lastMessageBar;

  @override
  void initState() {
    super.initState();

    listModel = TagListModel(
        context, Config.wordsDatabase, _listKey, columnId, SortOrder.ascending);

    _listViewScrollController.addListener(() => _onListScrolled());

    initAsyncState();
  }

  Future<void> initAsyncState() async {
//    if (await FlutterAppBadger.isAppBadgeSupported())
//      FlutterAppBadger.updateBadgeCount(1);

    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    listModel.setFilter(filter);

    return Future.value();
  }

  @override
  void dispose() {
    listModel.dispose();

    super.dispose();
  }

  void _onListScrolled() {
    final _scrollOffset = _listViewScrollController.offset;

    if (_clipperKey != null && _clipperKey.currentState != null) {
      _clipperKey.currentState.setState(() {
        _clipperHeight = offsetPosition(_scrollOffset,
                    _listViewScrollController.position.viewportDimension, 15.0)
                .abs() *
            Config.clipperHeight;
      });
    }

    if (_headerKey != null && _headerKey.currentState != null) {
      _headerKey.currentState.setState(() {
        _headerPadding = max(
                Config.headerPaddingFactor,
                1.0 -
                    offsetPosition(
                            _scrollOffset,
                            _listViewScrollController
                                .position.viewportDimension,
                            3.0)
                        .abs()) *
            Config.headerPadding;

        _headerOpacity = max(
            0.0,
            1.0 -
                offsetPosition(
                        _scrollOffset,
                        _listViewScrollController.position.viewportDimension,
                        5.0)
                    .abs());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: buildDrawer(context, DrawerItemIndex.tagsPage),
        resizeToAvoidBottomPadding: false,
        body: Builder(
          // Create an inner BuildContext so that methods
          // can refer to the Scaffold with Scaffold.of().
          builder: (BuildContext context) {
            return BackgroundImage(
              assetName: 'assets/images/background.png',
              child: Stack(
                children: <Widget>[
                  _buildLine(),
                  _buildBottomPart(),
                  _buildImage(),
                  _buildTopHeader(context),
                  _buildProfileRow(),
                  _buildFab(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> _onWillPop() {
    if (filter.length > 0) {
      setState(() {
        filter.clear();
        listModel.setFilter(filter);
      });

      return Future.value(false);
    } else
      return Future.value(true);
  }

  Widget _buildFab() {
    return Positioned(
        top: _imageHeight - 100.0,
        right: -40.0,
        child: AnimatedMenuFab(
          isOpened: false,
          onFavoritesClick: _toggleShowOnlyFavorites,
          onAddClick: _addNewTagDialog,
          onSearchClick: _setFilter,
        ));
  }

  void _addNewTagDialog() async {
    var result = await showAddEditTagDialog(context);
    if (result != null &&
        result.action == AddEditTagDialogResultAction.ActionSave) {
      if (result.tag.tag.trim().length > 0) {
        await listModel.insert(0, result.tag);

        logAddTagAction('${result.tag.tag}', null, result.tag, context);
      } else {
        MessageBar.warning(
            title: CustomLocalizations.of(context).warning,
            message: CustomLocalizations.of(context).invalidInputIgnored,
            duration: Duration(seconds: Config.snackBarDurationShortSecs))
          ..show(context);
      }
    }
  }

  void _editTagDialog(TagEntry tag, StateSetter setState) async {
    final TagEntry before = TagEntry.from(tag);

    var result = await showAddEditTagDialog(context, tag: tag);
    if (result != null &&
        result.action == AddEditTagDialogResultAction.ActionSave) {
      if (result.tag.tag.trim().length > 0) {
        tag.tag = result.tag.tag;
        tag.description = result.tag.description;

        await listModel.update(tag);

        logEditTagAction('${result.tag.tag} - ${result.tag.description}',
            before, result.tag, context);

        setState(() {});
      } else {
        MessageBar.warning(
            title: CustomLocalizations.of(context).warning,
            message: CustomLocalizations.of(context).invalidInputIgnored,
            duration: Duration(seconds: Config.snackBarDurationShortSecs))
          ..show(context);
      }
    }
  }

  void _colorPickerDialog(TagEntry tag, StateSetter setState) async {
    final TagEntry before = TagEntry.from(tag);

    var result = await showColorPickerDialog(context, tag.color);
    if (result != null && result.action == ColorPickerResultAction.ActionSave) {
      setState(() {
        tag.color = result.color;
      });

      await listModel.update(tag);

      logChangeTagColorAction(
          '${tag.tag} - ${result.color.toString()}', before, tag, context);
    }
  }

  void _tagPropertiesDialog(TagEntry tag, StateSetter setState) async {
    await showTagPropertiesDialog(context, tag);
  }

  bool _toggleFavorite(TagEntry tag, StateSetter setState) {
    final TagEntry before = TagEntry.from(tag);

    tag.favorite = !tag.favorite;
    listModel.update(tag);

    logChangeTagFavoriteAction(
        '${tag.tag}: ${boolToString(tag.favorite)}', before, tag, context);

    return tag.favorite;
  }

  void _deleteTag(int index, TagEntry tag, StateSetter _setState) async {
    await listModel.remove(tag, index);

    logDeleteTagAction('${tag.tag}', tag, null, context);

    lastRemovedTag = tag;

    lastMessageBar = MessageBar.infoWithAction(
      title: CustomLocalizations.of(context).info,
      message: CustomLocalizations.of(context).tagHasBeenDeleted,
      duration: Duration(seconds: Config.snackBarDurationLongSecs),
      onAction: () => _undoDeleteTag(),
      actionChild: Text(
        CustomLocalizations.of(context).undo,
        style: TextStyle(
          color: Colors.grey[200],
        ),
      ),
    )..show(context);

    setState(() {});
  }

  void _undoDeleteTag() async {
    if (lastRemovedTag != null) {
      await listModel.insert(0, lastRemovedTag);

      logAddTagAction(
          '${CustomLocalizations.of(context).undoTagDeleted} ${lastRemovedTag.tag}',
          null,
          lastRemovedTag,
          context);
    }

    setState(() {
      lastRemovedTag = null;
    });

    lastMessageBar.dismiss();
  }

  void _toggleShowOnlyFavorites() {
    showOnlyFavorites = !showOnlyFavorites;

    setState(() {
      if (showOnlyFavorites) {
        filter = Map<String, dynamic>();
        filter[columnFavorite] = showOnlyFavorites ? '1' : '0';
      } else {
        filter.remove(columnFavorite);
      }

      listModel.setFilter(filter);
    });
  }

  void _setFilter() async {
    var result = await showSearchTagDialog(context, text: '');
    if (result != null &&
        result.action == SearchTagDialogResultAction.ActionSearch) {
      var text = result.text;

      setState(() {
        filter = Map<String, dynamic>();
        filter[columnTag] = '%$text%';
        // filter[columnDescription] = '%$text%';

        listModel.setFilter(filter);
      });
    }
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        key: _clipperKey,
        builder: (BuildContext context, StateSetter setState) {
          return Positioned.fill(
            bottom: null,
            child: ClipPath(
              clipper: DiagonalClipper(height: _clipperHeight),
              child: Image.asset(
                assetName,
                fit: BoxFit.cover,
                height: _imageHeight,
              ),
            ),
          );
        });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                'Schlagwörter',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    final tagList = _buildTagList();

    return Stack(
      children: <Widget>[
        FutureBuilder<Widget>(
          future: tagList,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.data != null) {
              return snapshot.data;
            } else {
              return Container();
            }
          },
        ),
        _buildHeader(),
      ],
    );
  }

  Future<Text> _buildLabelText(offset) async {
    final len = await listModel.length;

    final item = await listModel[((len - 1) *
            ((offset - Config.listViewTopInternalPadding) / (len * 0.59)) /
            100)
        .round()];
    final text = item?.tag;

    lastCachedTag = text;

    return Text(text, style: TextStyle(color: Colors.white));
  }

  Future<Widget> _buildTagList() async {
    final len = await listModel.length;

    return Padding(
      padding: EdgeInsets.only(top: Config.listViewTopPadding),
      child: DraggableScrollbar.semicircle(
        // alwaysVisibleScrollThumb: true,
        backgroundColor: Colors.pink,
        scrollbarTimeToFade: Duration(milliseconds: 1250),
        labelTextBuilder: (double offset) {
          final labelText = _buildLabelText(offset);

          return FutureBuilder<Text>(
            future: labelText,
            builder: (BuildContext context, AsyncSnapshot<Text> snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.data != null) {
                return snapshot.data;
              } else {
                return Text(lastCachedTag ??= '',
                    style: TextStyle(color: Colors.white));
              }
            },
          );
        },
        labelConstraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width - 50,
          maxHeight: 50,
        ),
        controller: _listViewScrollController,
        child: CustomAnimatedList(
          // primary: true,
          padding: EdgeInsets.only(top: Config.listViewTopInternalPadding),
          initialItemCount: len,
          itemExtent: 59,
          key: _listKey,
          controller: _listViewScrollController,
          itemBuilder: (context, index, animation) {
            var result = listModel[index];

            return FutureBuilder<TagEntry>(
              future: result,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.data != null) {
                  return StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return TagRow(
                      tag: snapshot.data,
                      animation: animation,
                      onTap: (tag) => _tagPropertiesDialog(tag, setState),
                      onLongPress: (tag) => _editTagDialog(tag, setState),
                      onEditAction: (tag) => _editTagDialog(tag, setState),
                      onChangeColorAction: (tag) =>
                          _colorPickerDialog(tag, setState),
                      // onArchiveAction: (tag) {},
                      onFavoriteAction: (tag) => _toggleFavorite(tag, setState),
                      onDeleteAction: (tag) => _deleteTag(index, tag, setState),
                    );
                  });
                } else {
                  return StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return TagRow.waiting(
                      animation: animation,
                    );
                  });
                }
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildHeader() {
    final len = listModel.length;
    final String filterHint =
        filter.length > 0 ? CustomLocalizations.of(context).filtered : '';

    return FutureBuilder<int>(
      key: _headerKey,
      future: len,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          final String text = snapshot.data == 1
              ? CustomLocalizations.of(context).entryOne
              : CustomLocalizations.of(context).entryMany;

          return Padding(
            padding: EdgeInsets.only(left: 64.0, top: _headerPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: _headerOpacity ??= 1.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            CustomLocalizations.of(context).headerTags,
                            style: TextStyle(fontSize: 34.0),
                          ),
                          Text(
                            '${formatNumber(snapshot.data)} $text $filterHint',
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        } else {
          return Text('');
        }
      },
    );
  }

  Widget _buildLine() {
    return Positioned(
      top: 0.0,
      bottom: 0.0,
      left: 32.0,
      child: Container(
        width: 2.0,
        color: Theme.of(context).brightness == Brightness.dark
            ? Colors.grey[800]
            : Colors.grey[300],
      ),
    );
  }

  String getCurrentDateText() {
    return DateFormat.yMMMMEEEEd().format(DateTime.now());
  }
}
