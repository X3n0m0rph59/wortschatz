/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';

import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path/path.dart' show basename, basenameWithoutExtension, join;
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/exceptions/import_export_error.dart';
import 'package:wortschatz/exceptions/share_error.dart';
import 'package:wortschatz/pages/main_page.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/database.dart';
import 'package:wortschatz/util/database_profiles.dart';
import 'package:wortschatz/util/zip_file.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/custom_icon_button.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';
import 'package:wortschatz/widgets/message_bar.dart';

class ToolsPage extends StatefulWidget {
  ToolsPage({Key key}) : super(key: key);

  @override
  _ToolsPageState createState() => _ToolsPageState();
}

class _ToolsPageState extends State<ToolsPage> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  final sharedPreferences = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    return Future.value();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context, DrawerItemIndex.toolsPage),
      body: Builder(
        // Create an inner BuildContext so that methods
        // can refer to the Scaffold with Scaffold.of().
        builder: (BuildContext context) {
          return BackgroundImage(
            assetName: 'assets/images/background.png',
            child: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  _buildImage(),
                  _buildTopHeader(context),
                  _buildProfileRow(),
                  _buildBottomPart(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Positioned.fill(
        bottom: null,
        child: ClipPath(
          clipper: DiagonalClipper(),
          child: Image.asset(
            assetName,
            fit: BoxFit.cover,
            height: _imageHeight,
          ),
        ),
      );
    });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                'Werkzeuge',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    return Padding(
      padding: EdgeInsets.only(top: _imageHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          _buildContent(),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: EdgeInsets.only(left: 64, right: 16, top: 16),
      child: FutureBuilder<SharedPreferences>(
        future: sharedPreferences,
        builder:
            (BuildContext context, AsyncSnapshot<SharedPreferences> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
//              final TextEditingController controllerUserName =
//                  TextEditingController(
//                      text: snapshot.data.getString('username'));

            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudUploadAlt,
                    child: Text('Volles Backup erstellen'),
                    onPressed: () => _exportFullBackup(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudDownloadAlt,
                    child: Text('Backup zurückspielen'),
                    onPressed: () => _importFullBackup(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0, bottom: 0.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Wortschatz',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        Text(
                          'Enthält den kompletten Wortschatz der aktuellen Datenbank '
                          '(Profil) sowie alle Schlagwörter (nicht jedoch die '
                          'Historie und das Wörterbuch)',
                          style: TextStyle(color: Colors.grey, fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudUploadAlt,
                    child: Text('Wortschatz exportieren'),
                    onPressed: () => _exportDatabase(Config.wordsDatabase,
                        ['words', 'tags', 'words_tags', 'android_metadata']),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudDownloadAlt,
                    child: Text('Wortschatz importieren'),
                    onPressed: () => _importDatabase(Config.wordsDatabase,
                        ['words', 'tags', 'words_tags', 'android_metadata']),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudDownloadAlt,
                    child: Text('Wortschatz vereinigen'),
                    onPressed: () => _mergeDatabase(Config.wordsDatabase),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0, bottom: 0.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Wörterbuch',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        Text(
                          'Enthält ausschließlich das Wörterbuch aus der '
                          'aktuellen Datenbank (Profil)',
                          style: TextStyle(color: Colors.grey, fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudUploadAlt,
                    child: Text('Wörterbuch exportieren'),
                    onPressed: () => _exportDatabase(Config.dictionaryDatabase,
                        ['words', 'tags', 'words_tags', 'android_metadata']),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cloudDownloadAlt,
                    child: Text('Wörterbuch importieren'),
                    onPressed: () => _importDatabase(Config.dictionaryDatabase,
                        ['words', 'tags', 'words_tags', 'android_metadata']),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0, bottom: 0.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Sonstige Werkzeuge',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        Text(
                          'Aktuelle Datenbank (Profil) optimieren',
                          style: TextStyle(color: Colors.grey, fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomIconButton(
                    icon: FontAwesomeIcons.cogs,
                    child: Text('Datenbank optimieren'),
                    onPressed: () => _optimizeDatabases(),
                  ),
                ),
              ],
            );
          } else {
            return Text('Wird geladen...');
          }
        },
      ),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 64.0, top: Config.simpleHeaderPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Werkzeuge',
            style: TextStyle(fontSize: 34.0),
          ),
          Text(
            'Werkzeuge zum Verwalten der aktuellen Datenbank (Profil)',
            style: TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ],
      ),
    );
  }

//  void _shareDatabaseFile(String filename, context) async {
//    MessageBar messageBar;
//
//    try {
//      final String fileComponent =
//          '${basenameWithoutExtension(filename)}-${DateTime.now().toIso8601String()}.db';
//
//      messageBar = MessageBar.infoWithProgress(
//          title: 'Info', message: 'Datenbank wird exportiert...')
//        ..show(context);
//
//      // we need to sleep here, to wait for the snackbar to appear
//      await Future.delayed(const Duration(seconds: 2), () {});
//
//      await Share.shareFile(File(filename),
//          subject: fileComponent, mimeType: 'application/x-sqlite3');
//
//      messageBar.dismiss();
//    } catch (e) {
//      messageBar.dismiss();
//
//      MessageBar.error(
//          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
//        ..show(context);
//
//      throw ShareError('Could not share data');
//    }
//  }

  void _exportDatabase(String databaseName, List<String> tables) async {
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);

    final String date = DateTime.now().toIso8601String();
    final String dbBasename = basename(databaseName);
    final String outFile = join((await getTemporaryDirectory()).path,
        'wortschatz-$dbBasename-backup-$date.zip');

    MessageBar messageBar;

    try {
      final profileName = await DatabaseProfileManager().currentDatabaseProfile;
      final String dbDir = join(await getDatabasesPath(), profileName);

      final Directory outDir = Directory(
        join(
          (await getTemporaryDirectory()).path,
          'export-tmp-${DateTime.now().toIso8601String()}',
        ),
      );

      messageBar = MessageBar.infoWithProgress(
          title: 'Info', message: 'Datenbank wird exportiert...')
        ..show(context);

      // we need to sleep here, to wait for the snackbar to appear
      await Future.delayed(const Duration(seconds: 2), () {});

      outDir.createSync(recursive: true);

      final String dbPath = join(dbDir, databaseName);

      final String dbBackupPath = join(outDir.path, Config.wordsDatabase);

      await copyDatabase(dbPath, dbBackupPath, tables);

      final srcFiles = <String>[
        dbBackupPath,
      ];

      createZipArchive(outFile, srcFiles);

      messageBar.dismiss();
    } catch (e) {
      print(e);

      messageBar?.dismiss();

      MessageBar.error(
          title: 'Fehler', message: 'Datenbank konnte nicht exportiert werden')
        ..show(context);

      throw ImportExportError('Could not zip data');
    }

    try {
      final String fileComponent = basename(outFile);

      await Share.shareFile(File(outFile),
          subject: fileComponent, mimeType: 'application/zip');
    } catch (e) {
      print(e);

      MessageBar.error(
          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
        ..show(context);

      throw ShareError('Could not share data');
    }
  }

  void _importDatabase(String databaseName, List<String> tables) async {
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);

    MessageBar messageBar;

    try {
      final FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: ['zip'],
//        allowedUtiTypes: ['com.x3n0m0rph59.wortschatz'],
//        allowedMimeType: 'application/zip',
//        invalidFileNameSymbols: ['/'],
      );

      final path = await FlutterDocumentPicker.openDocument(params: params);

      if (path != null) {
        try {
          final profileName =
              await DatabaseProfileManager().currentDatabaseProfile;
          final String dbDir = join(await getDatabasesPath(), profileName);

          final Directory outDir = Directory(
            join(
              (await getTemporaryDirectory()).path,
              'import-tmp-${DateTime.now().toIso8601String()}',
            ),
          );

          messageBar = MessageBar.infoWithProgress(
              title: 'Info', message: 'Datenbank wird importiert...')
            ..show(context);

          // we need to sleep here, to wait for the snackbar to appear
          await Future.delayed(const Duration(seconds: 2), () {});

          outDir.createSync(recursive: true);
          unzipArchive(path, outDir.path);

          final String dbPath = join(dbDir, databaseName);
          final String dbBackupPath = join(outDir.path, databaseName);

          await importDatabase(dbBackupPath, dbPath, tables);

          messageBar.dismiss();

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => MainPage()),
              (p) => false);
        } catch (e) {
          print(e);

          messageBar?.dismiss();

          MessageBar.error(
              title: 'Fehler',
              message: 'Datenbank konnte nicht importiert werden')
            ..show(context);

          throw ImportExportError('Could not import database');
        }
      }
    } catch (e) {
      print(e);

      messageBar?.dismiss();

      MessageBar.error(
          title: 'Fehler', message: 'Datenbank konnte nicht importiert werden')
        ..show(context);

      throw ImportExportError('Could not import database');
    }
  }

  void _mergeDatabase(String databaseName) async {
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);

    MessageBar messageBar;

    try {
      final FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: ['zip'],
//        allowedUtiTypes: ['com.x3n0m0rph59.wortschatz'],
//        allowedMimeType: 'application/zip',
//        invalidFileNameSymbols: ['/'],
      );

      final path = await FlutterDocumentPicker.openDocument(params: params);

      if (path != null) {
        try {
          final profileName =
              await DatabaseProfileManager().currentDatabaseProfile;
          final String dbDir = join(await getDatabasesPath(), profileName);

          final Directory outDir = Directory(
            join(
              (await getTemporaryDirectory()).path,
              'import-tmp-${DateTime.now().toIso8601String()}',
            ),
          );

          messageBar = MessageBar.infoWithProgress(
              title: 'Info', message: 'Datenbank wird vereinigt...')
            ..show(context);

          // we need to sleep here, to wait for the snackbar to appear
          await Future.delayed(const Duration(seconds: 2), () {});

          outDir.createSync(recursive: true);
          unzipArchive(path, outDir.path);

          final String dbPath = join(dbDir, databaseName);
          final String dbBackupPath = join(outDir.path, databaseName);

          mergeDatabases(dbPath, dbBackupPath, (index, total, currentWord) {
            messageBar.controller.animateTo(total + 1 / index + 1);
          });

          messageBar.dismiss();

          MessageBar.info(
              title: 'Info', message: 'Datenbanken erfolgreich vereinigt')
            ..show(context);

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => MainPage()),
              (p) => false);
        } catch (e) {
          print(e);

          messageBar?.dismiss();

          MessageBar.error(
              title: 'Fehler',
              message: 'Datenbank konnte nicht vereinigt werden')
            ..show(context);

          throw ImportExportError('Could not import database');
        }
      }
    } catch (e) {
      print(e);

      messageBar?.dismiss();

      MessageBar.error(
          title: 'Fehler', message: 'Datenbank konnte nicht vereinigt werden')
        ..show(context);

      throw ImportExportError('Could not import database');
    }
  }

  void _optimizeDatabases() async {
    try {
      final MessageBar messageBar = MessageBar.infoWithProgress(
          title: 'Info', message: 'Datenbanken werden optimiert...')
        ..show(context);

      // we need to sleep here, to wait for the snackbar to appear
      await Future.delayed(const Duration(seconds: 2), () {});

      optimizeDatabase(Config.wordsDatabase);
      optimizeDatabase(Config.editHistoryDatabase);
      optimizeDatabase(Config.dictionaryDatabase);

      messageBar.dismiss();

      MessageBar.info(title: 'Info', message: 'Optimierung abgeschlossen')
        ..show(context);
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
        ..show(context);
    }
  }

  void _exportFullBackup() async {
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);

    final String date = DateTime.now().toIso8601String();
    final String outFile = join((await getTemporaryDirectory()).path,
        'wortschatz-full-backup-$date.zip');

    MessageBar messageBar;

    try {
      final profileName = await DatabaseProfileManager().currentDatabaseProfile;
      final String dbDir = join(await getDatabasesPath(), profileName);

      final Directory outDir = Directory(
        join(
          (await getTemporaryDirectory()).path,
          'backup-tmp-${DateTime.now().toIso8601String()}',
        ),
      );

      messageBar = MessageBar.infoWithProgress(
          title: 'Info', message: 'Backup wird erstellt...')
        ..show(context);

      // we need to sleep here, to wait for the snackbar to appear
      await Future.delayed(const Duration(seconds: 2), () {});

      outDir.createSync(recursive: true);

      final String wordsPath = join(dbDir, Config.wordsDatabase);
      final String editHistoryPath = join(dbDir, Config.editHistoryDatabase);
      final String dictionaryPath = join(dbDir, Config.dictionaryDatabase);

      final String wordsBackupPath = join(outDir.path, Config.wordsDatabase);
      final String editHistoryBackupPath =
          join(outDir.path, Config.editHistoryDatabase);
      final String dictionaryBackupPath =
          join(outDir.path, Config.dictionaryDatabase);

      await copyDatabase(
          wordsPath, wordsBackupPath, ['words', 'tags', 'words_tags']);
      await copyDatabase(
          editHistoryPath, editHistoryBackupPath, ['edit_history']);
      await copyDatabase(dictionaryPath, dictionaryBackupPath,
          ['words', 'tags', 'words_tags']);

      final srcFiles = <String>[
        wordsBackupPath,
        editHistoryBackupPath,
        dictionaryBackupPath,
      ];

      createZipArchive(outFile, srcFiles);

      messageBar.dismiss();
    } catch (e) {
      print(e);

      messageBar?.dismiss();

      MessageBar.error(
          title: 'Fehler', message: 'Backup konnte nicht erstellt werden')
        ..show(context);

      throw ImportExportError('Could not zip data');
    }

    try {
      final String fileComponent = basename(outFile);

      await Share.shareFile(File(outFile),
          subject: fileComponent, mimeType: 'application/zip');
    } catch (e) {
      print(e);

      MessageBar.error(
          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
        ..show(context);

      throw ShareError('Could not share data');
    }
  }

  void _importFullBackup() async {
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);

    MessageBar messageBar;

    try {
      final FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: ['zip'],
//        allowedUtiTypes: ['com.x3n0m0rph59.wortschatz'],
//        allowedMimeType: 'application/zip',
//        invalidFileNameSymbols: ['/'],
      );

      final inFile = await FlutterDocumentPicker.openDocument(params: params);

      if (inFile != null) {
        final Directory outDir = Directory(
          join(
            (await getTemporaryDirectory()).path,
            basenameWithoutExtension(inFile),
          ),
        );

        messageBar = MessageBar.infoWithProgress(
            title: 'Info', message: 'Backup wird importiert...')
          ..show(context);

        // we need to sleep here, for the snackbar to appear
        await Future.delayed(const Duration(seconds: 2), () {});

        outDir.createSync(recursive: true);
        unzipArchive(inFile, outDir.path);

        final profileName =
            await DatabaseProfileManager().currentDatabaseProfile;
        final String dstPath = join(await getDatabasesPath(), profileName);

        final String wordsPath = join(dstPath, Config.wordsDatabase);
        final String editHistoryPath =
            join(dstPath, Config.editHistoryDatabase);
        final String dictionaryPath = join(dstPath, Config.dictionaryDatabase);

        final String wordsBackupPath = join(outDir.path, Config.wordsDatabase);
        final String editHistoryBackupPath =
            join(outDir.path, Config.editHistoryDatabase);
        final String dictionaryBackupPath =
            join(outDir.path, Config.dictionaryDatabase);

        await importDatabase(
            wordsBackupPath, wordsPath, ['words', 'tags', 'words_tags']);
        await importDatabase(
            editHistoryBackupPath, editHistoryPath, ['edit_history']);
        await importDatabase(dictionaryBackupPath, dictionaryPath,
            ['words', 'tags', 'words_tags']);

        messageBar.dismiss();

        MessageBar.info(
            title: 'Info', message: 'Backup erfolgreich importiert!')
          ..show(context);

        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) => MainPage()), (p) => false);
      }
    } catch (e) {
      print(e);

      messageBar?.dismiss();

      MessageBar.error(
          title: 'Fehler', message: 'Konnte das Backup nicht importieren')
        ..show(context);

      throw ImportExportError('Could not import data');
    }
  }
}
