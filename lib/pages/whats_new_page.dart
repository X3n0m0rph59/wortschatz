/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_whatsnew/flutter_whatsnew.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget whatsNew(context) {
  const textScaleFactor = 1.0;
  const textScaleFactorHeading = textScaleFactor * 1.15;
  const contentPadding = 15.0;

  return WhatsNewPage(
    buttonColor: Colors.pinkAccent,
    title: Text(
      'Neuigkeiten',
      textScaleFactor: textScaleFactor,
      textAlign: TextAlign.center,
      style: const TextStyle(
        // Text Style Needed to Look like iOS 11
        fontSize: 22.0,
        fontWeight: FontWeight.bold,
      ),
    ),
    buttonText: Text(
      'Weiter',
      textScaleFactor: textScaleFactor,
      style: const TextStyle(color: Colors.white),
    ),
    onButtonPressed: () {
      Navigator.pop(context);
    },
    items: <ListTile>[
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'Neues in Version 0.0.61+1:',
          textScaleFactor: textScaleFactorHeading,
          style: TextStyle(fontWeight: FontWeight.bold),
        ), //Title is the only Required Item
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'Weitere Änderungen',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Alle Abhängigkeiten aktualisiert',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'Neues in Version 0.0.60+1:',
          textScaleFactor: textScaleFactorHeading,
          style: TextStyle(fontWeight: FontWeight.bold),
        ), //Title is the only Required Item
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.handsHelping),
        title: Text(
          'Neue Funktion hinzugefügt:\nOnboarding',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Beim ersten Start der App wird jetzt eine Einführung angezeigt',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.palette),
        title: Text(
          'UI überarbeitet',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Neue Hintergrundmuster und Farbpalette angepasst',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'Neues in Version 0.0.59+1:',
          textScaleFactor: textScaleFactorHeading,
          style: TextStyle(fontWeight: FontWeight.bold),
        ), //Title is the only Required Item
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.safari),
        title: Text(
          'Neue Funktion hinzugefügt:\nNeuigkeiten',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Du wirst ab jetzt über Änderungen an der App informiert, wenn du die '
              'App nach einem Update zum ersten mal neu startest',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.fileAlt),
        title: Text(
          'Neue Funktion hinzugefügt:\nWörter aus dem Wortschatz entdecken',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Entdecke Wörter aus deinem Wortschatz erneut. Zu erreichen über das '
          'Drawer Hauptmenü -> Entdecken',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.infoCircle),
        title: Text(
          'Neue Funktion hinzugefügt:\nMarquee Scroller',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Überlanger Text wird jetzt nach Art eines Laufbandes gescrollt, '
          'anstatt einfach abgeschnitten zu werden',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'UI Texte überarbeitet',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Es wurden einige Texte in der App korrigiert',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'UI Layout teilweise überarbeitet',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Es wurden einige UI Layout Elemente in der App überarbeitet um mehr '
          'Anzeigefläche zu gewinnen',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'Fehlerbehebungen',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Es wurden einige kleinere Fehler in der App korrigiert',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
      ListTile(
        contentPadding: EdgeInsets.all(contentPadding),
        leading: const Icon(FontAwesomeIcons.solidStar),
        title: Text(
          'Weitere Änderungen',
          textScaleFactor: textScaleFactor,
        ), //Title is the only Required Item
        subtitle: Text(
          'Alle Abhängigkeiten aktualisiert, '
          'Unterstützung für Android 9 hinzugefügt sowie '
          'Übersetzungen: de, en aktualisiert',
          textScaleFactor: textScaleFactor,
        ),
        onTap: () {
          // Navigator.of(context).pushNamed("/settings");
        },
      ),
    ], //Required
  );
}
