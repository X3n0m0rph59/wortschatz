/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wave/config.dart' as WaveConfig;
import 'package:wave/wave.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/word_list_model.dart';
import 'package:wortschatz/dialogs/add_edit_word_dialog.dart';
import 'package:wortschatz/dialogs/search_by_tag_dialog.dart';
import 'package:wortschatz/dialogs/tag_selection_dialog.dart';
import 'package:wortschatz/exceptions/intent_launch_error.dart';
import 'package:wortschatz/exceptions/share_error.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/date_time.dart';
import 'package:wortschatz/util/edit_history.dart';
import 'package:wortschatz/util/format.dart';
import 'package:wortschatz/util/l10n.dart';
import 'package:wortschatz/widgets/animated_menu_fab.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';
import 'package:wortschatz/widgets/favorite_widget.dart';
import 'package:wortschatz/widgets/marquee_widget.dart';
import 'package:wortschatz/widgets/message_bar.dart';

class DiscoverPage extends StatefulWidget {
  DiscoverPage({Key key}) : super(key: key);

  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  var _headerKey = GlobalKey();
  GlobalKey<AnimatedMenuFabState> _fabKey;

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  WordListModel listModel;
  Future<List<int>> selectedWords;

  bool showOnlyFavorites = false;
  Map<String, dynamic> filter = Map<String, dynamic>();
  List<String> filterTags = List<String>();

  WordEntry lastRemovedWord;
  MessageBar lastMessageBar;

  final sharedPreferences = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    listModel = WordListModel(
        context, Config.wordsDatabase, null, columnId, SortOrder.ascending);

    selectedWords = selectWordIndices();

    initAsyncState();
  }

  Future<List<int>> selectWordIndices() async {
    var rng = new Random();
    var max = await listModel.length;

    const num_cards = 8;

    var l = List<int>();

    if (max <= num_cards)
      return List.generate(num_cards, (idx) => rng.nextInt(max));
    else {
      int cnt = 0,
          iter = 0;

      do {
        var r = rng.nextInt(max);

        if (!l.contains(r)) {
          l.add(r);
          cnt++;
        }

        // prevent infinite loops
        if (iter++ > num_cards * 2)
          break;
      } while (cnt < num_cards);

      return l;
    }
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    return Future.value();
  }

  @override
  void dispose() {
    listModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: buildDrawer(context, DrawerItemIndex.discoverPage),
        body: Builder(
          // Create an inner BuildContext so that methods
          // can refer to the Scaffold with Scaffold.of().
          builder: (BuildContext context) {
            return BackgroundImage(
              assetName: 'assets/images/background.png',
              child: Stack(
                children: <Widget>[
                  _buildImage(),
                  _buildTopHeader(context),
                  _buildProfileRow(),
                  _buildFab(),
                  _buildBottomPart(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> _onWillPop() {
    if (_scaffoldKey.currentState.isDrawerOpen) {
      return Future.value(true);
    } else {
      if (filter.length > 0 || filterTags.length > 0 || showOnlyFavorites) {
        setState(() {
          filter.clear();
          listModel.setFilter(filter);

          filterTags.clear();
          listModel.setTagFilter(filterTags);

          listModel.setSortColumn(columnId);
          listModel.setSortOrder(SortOrder.descending);

          showOnlyFavorites = false;
        });

        // _reloadData();

        return Future.value(false);
      } else {
        return Future.value(true);
      }
    }
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Positioned.fill(
        bottom: null,
        child: ClipPath(
          clipper: DiagonalClipper(),
          child: Image.asset(
            assetName,
            fit: BoxFit.cover,
            height: _imageHeight,
          ),
        ),
      );
    });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                CustomLocalizations.of(context).discoverWords,
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildFab() {
    return Positioned(
        top: _imageHeight - 100.0,
        right: -40.0,
        child: AnimatedMenuFab(
          key: _fabKey,
          isOpened: false,
          onFavoritesClick: _toggleShowOnlyFavorites,
//          onSortClick: _toggleSortMode,
          onFilterTagsClick: _setFilterTags,
//          onSearchClick: _setFilter,
//          onSpecialClick: _toggleShowOnlyDupes,
          onReloadClick: _reloadData,
        ));
  }

  Widget _buildBottomPart() {
    return Padding(
      padding: EdgeInsets.only(top: _imageHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          _buildContent(),
        ],
      ),
    );
  }

  void _toggleShowOnlyFavorites() {
    showOnlyFavorites = !showOnlyFavorites;

    setState(() {
      filter.clear();

      if (showOnlyFavorites) {
        filter = Map<String, dynamic>();
        filter[columnFavorite] = showOnlyFavorites ? '1' : '0';
      }

      listModel.setFilter(filter);
    });

    _reloadData();
  }

  void _setFilterTags() async {
    var result = await showSearchByTagDialog(context, text: '');
    if (result != null &&
        result.action == SearchByTagDialogResultAction.ActionSearch) {
      var text = result.text;

      setState(() {
        filterTags = <String>['%$text%'];

        listModel.setTagFilter(filterTags);
      });
    }
  }

  void _reloadData() {
    setState(() {
      selectedWords = selectWordIndices();
    });
  }

  bool _toggleFavorite(WordEntry word) {
    final WordEntry before = WordEntry.from(word);

    word.favorite = !word.favorite;
    listModel.update(word);

    logChangeWordFavoriteAction(
        '${word.word}: ${boolToString(word.favorite)}', before, word, context);

    return word.favorite;
  }

  void _searchWord(WordEntry word) async {
    final url = Uri.encodeFull('https://www.google.com/search?q=${word.word}');

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      MessageBar.error(
          title: 'Fehler',
          message: 'Browser Intent konnte nicht gestartet werden')
        ..show(context);

      throw IntentLaunchError('Could not launch url: "$url"');
    }
  }

  void _editWord(WordEntry word) async {
    final WordEntry before = WordEntry.from(word);

    var result = await showAddEditWordDialog(context, word: word);
    if (result != null &&
        result.action == AddEditWordDialogResultAction.ActionSave) {
      if (result.word.word.trim().length > 0) {
        word.word = result.word.word;
        word.description = result.word.description;

        await listModel.update(word);

        logEditWordAction('${result.word.word} - ${result.word.description}',
            before, result.word, context);

        // setState(() {});
      } else {
        MessageBar.warning(
            title: CustomLocalizations.of(context).warning,
            message: CustomLocalizations.of(context).invalidInputIgnored,
            duration: Duration(seconds: Config.snackBarDurationShortSecs))
          ..show(context);
      }
    }
  }

  void _shareWord(WordEntry word) async {
    try {
      await Share.share('Ein Wort aus meinem Wortschatz: ${word.word}');
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
        ..show(context);

      throw ShareError('Could not share data');
    }
  }

  void _deleteWord(WordEntry word) async {
    await listModel.remove(word, 0);

    logDeleteWordAction('${word.word}', word, null, context);

    lastRemovedWord = word;

    lastMessageBar = MessageBar.infoWithAction(
      title: CustomLocalizations.of(context).info,
      message: CustomLocalizations.of(context).wordHasBeenDeleted,
      duration: Duration(seconds: Config.snackBarDurationLongSecs),
      onAction: () => _undoDeleteWord(),
      actionChild: Text(
        CustomLocalizations.of(context).undo,
        style: TextStyle(
          color: Colors.grey[200],
        ),
      ),
    )..show(context);

    setState(() {});
  }

  void _undoDeleteWord() async {
    if (lastRemovedWord != null) {
      await listModel.insert(0, lastRemovedWord);

      logAddWordAction(
          '${CustomLocalizations.of(context).undoWordDeleted} ${lastRemovedWord.word}',
          null,
          lastRemovedWord,
          context);
    }

    setState(() {
      lastRemovedWord = null;
    });

    lastMessageBar.dismiss();
  }

  void _editTags(WordEntry word) async {
    await showTagSelectionDialog(context, word);

    await listModel.update(word);

    // disable this for now, since we log each tag action separately
    // logChangeWordTagAction('${word.word}', word, context);

    setState(() {});
  }

  _buildTagsList(WordEntry word) {
    return MarqueeWidget(
      child: FutureBuilder<List<TagEntry>>(
        future: word.tags,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            final len = snapshot.data.length;
            if (len > 0) {
              var tagsList = <Widget>[];

              snapshot.data.forEach((t) {
                tagsList.add(
                  Icon(
                    FontAwesomeIcons.tag,
                    size: 8,
                    color: Colors.grey,
                  ),
                );

                tagsList.add(
                  Padding(
                    padding: const EdgeInsets.only(left: 2, right: 4),
                    child: Text(
                      '${t.tag}',
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontWeight:
                            t.favorite ? FontWeight.bold : FontWeight.normal,
                        fontSize: 12.0,
                        color: t.color,
                      ),
                    ),
                  ),
                );
              });

              return Padding(
                padding: EdgeInsets.only(top: 4, left: 0),
                child: Row(
                  children: tagsList,
                ),
              );
            } else {
              return Padding(
                padding: EdgeInsets.only(top: 4, left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Keine Schlagwörter verknüpft',
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 12.0,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              );
            }
          } else {
            return Container(
              width: 0,
              height: 0,
            );
          }
        },
      ),
    );
  }

  List<Widget> _buildActions(WordEntry word) {
    return <Widget>[
      IconSlideAction(
        caption: 'Suchen',
        color: Colors.green,
        icon: FontAwesomeIcons.search,
        onTap: () => _searchWord(word),
      ),
      IconSlideAction(
        caption: 'Bearbeiten',
        color: Colors.blue,
        icon: FontAwesomeIcons.edit,
        onTap: () => _editWord(word),
      ),
      IconSlideAction(
        caption: 'Teilen',
        color: Colors.indigo,
        icon: FontAwesomeIcons.shareAlt,
        onTap: () => _shareWord(word),
      ),
    ];
  }

  List<Widget> _buildSecondaryActions(WordEntry word) {
    return <Widget>[
      IconSlideAction(
        caption: 'Schlagwörter',
        color: Colors.indigo,
        icon: FontAwesomeIcons.tags,
        onTap: () => _editTags(word),
      ),
      IconSlideAction(
        caption: 'Löschen',
        color: Colors.red,
        icon: FontAwesomeIcons.trash,
        onTap: () => _deleteWord(word),
      ),
    ];
  }

  Widget _buildContent() {
    final clientHeight =
        MediaQuery.of(context).size.height - (Config.clipperHeight + 240);

    return Padding(
      padding: EdgeInsets.only(left: 0, top: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FutureBuilder<List<int>>(
            future: selectedWords,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.data != null) {
                final selectedWords = snapshot.data;

                return CarouselSlider(
                  enableInfiniteScroll: true,
                  height: clientHeight,
                  items: selectedWords.map((i) {
                    var result = listModel[i];

                    return FutureBuilder<WordEntry>(
                        future: result,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.done &&
                              snapshot.data != null) {
                            final WordEntry word = snapshot.data;

                            return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: ClipRRect(
                                borderRadius: new BorderRadius.circular(12.0),
                                child: Slidable(
                                  direction: Axis.vertical,
                                  actions: _buildActions(word),
                                  secondaryActions:
                                      _buildSecondaryActions(word),
                                  actionPane: SlidableDrawerActionPane(),
                                  actionExtentRatio: 0.25,
                                  child: GestureDetector(
                                    onTap: () => _searchWord(word),
                                    onLongPress: () => _editWord(word),
                                    child: Stack(
                                      children: <Widget>[
                                        WaveWidget(
                                          config: WaveConfig.CustomConfig(
                                            gradients: [
                                              [
                                                Colors.yellow,
                                                Color(0x55FFEB3B)
                                              ],
                                              [
                                                Colors.orange,
                                                Color(0x66FF9800)
                                              ],
                                              [
                                                Colors.red[800],
                                                Color(0x77E57373)
                                              ],
                                              [Colors.red, Color(0xEEF44336)],
                                            ],
                                            durations: [
                                              6000,
                                              10800,
                                              19440,
                                              35000,
                                            ],
                                            heightPercentages: [
                                              0.60,
                                              0.70,
                                              0.75,
                                              0.80
                                            ],
                                            blur: MaskFilter.blur(
                                                BlurStyle.solid, 25),
                                            gradientBegin: Alignment.bottomLeft,
                                            gradientEnd: Alignment.topRight,
                                          ),
                                          waveAmplitude: 0.0,
                                          backgroundColor: Colors.blue[800],
                                          size: Size(
                                            double.infinity,
                                            double.infinity,
                                          ),
                                        ),
                                        Center(
                                          child: Stack(
                                            children: <Widget>[
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(top: 24),
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.max,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.all(16),
                                                      child: MarqueeWidget(
                                                        child: AutoSizeText(
                                                          word.word,
                                                          style: TextStyle(
                                                              fontSize: 36.0),
                                                          minFontSize: 8.0,
                                                          maxLines: 1,
                                                          overflow:
                                                              TextOverflow.fade,
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 16, right: 16),
                                                      child: AutoSizeText(
                                                        word.description,
                                                        style: TextStyle(
                                                            fontSize: 16.0),
                                                        minFontSize: 8.0,
                                                        maxLines: 5,
                                                        overflow:
                                                            TextOverflow.fade,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.all(16),
                                                      child: GestureDetector(
                                                          onTap: () =>
                                                              _editTags(word),
                                                          child: _buildTagsList(
                                                              word)),
                                                    ),
                                                    FavoriteWidget(
                                                      favorite: word.favorite,
                                                      onTap: () =>
                                                          _toggleFavorite(word),
                                                      enabled: true,
                                                    ),
                                                    Expanded(
                                                      child: Align(
                                                        alignment: Alignment
                                                            .bottomCenter,
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  bottom: 12),
                                                          child: AutoSizeText(
                                                            '${CustomLocalizations.of(context).created} ${formatDateTime(word.createdAt)},\n'
                                                            '${CustomLocalizations.of(context).modified} ${formatDateTime(word.modifiedAt)}',
                                                            style: TextStyle(
                                                                fontSize: 10.0),
                                                            minFontSize: 6.0,
                                                            maxLines: 4,
                                                            overflow:
                                                                TextOverflow
                                                                    .fade,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          } else {
                            return Container();
                          }
                        });
                  }).toList(),
                );
              } else {
                return Container();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    final len = listModel.length;
    final String filterHint = filter.length > 0 || filterTags.length > 0
        ? CustomLocalizations.of(context).filtered
        : '';

    return FutureBuilder<int>(
      key: _headerKey,
      future: len,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Padding(
            padding:
                EdgeInsets.only(left: 64.0, top: Config.simpleHeaderPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Entdecken',
                          style: TextStyle(fontSize: 34.0),
                        ),
                        Text(
                          'Wortschatz neu entdecken $filterHint',
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
        } else {
          return Text('');
        }
      },
    );
  }
}
