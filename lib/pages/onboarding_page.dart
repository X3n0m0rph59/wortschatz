/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

Widget onboarding(context) {
  final pages = [
    PageViewModel(
        pageColor: Colors.blue,
        title: Text(
          'Willkommen!',
          style: TextStyle(fontSize: 22),
        ),
        body: Text(
          'Dies ist eine App zum Erfassen, Erkunden und Erweitern deines Wortschatzes.\n\n'
          'Swipe nach links um fortzufahren...',
          style: TextStyle(fontSize: 20),
        ),
        mainImage: Image.asset(
          'assets/images/onboarding_1.png',
          alignment: Alignment.center,
        )),
    PageViewModel(
        pageColor: Colors.deepOrange,
        title: Text(
          'Nutze zahlreiche tolle Funktionen',
          style: TextStyle(fontSize: 22),
        ),
        body: Text(
          'Erfasse alle Wörter aus deinem Wortschatz unter \'Mein Wortschatz\'.\n\n'
          'Du kannst deine erfassten Wörter mit beliebig vielen Schlagwörtern versehen!',
          style: TextStyle(fontSize: 20),
        ),
        mainImage: Image.asset(
          'assets/images/onboarding_2.png',
          alignment: Alignment.center,
        )),
    PageViewModel(
        pageColor: Colors.orange,
        title: Text(
          'Integriertes Wörterbuch',
          style: TextStyle(fontSize: 22),
        ),
        body: Text(
          'Unter der Ansicht \'Wörterbuch\' findest du ein deutsches Wörterbuch.\n\n'
          'Dieses Wörterbuch wird auch von der integrierten Rechtschreibprüfung verwendet.',
          style: TextStyle(fontSize: 18),
        ),
        mainImage: Image.asset(
          'assets/images/onboarding_3.png',
          alignment: Alignment.center,
        )),
    PageViewModel(
        pageColor: Colors.green,
        title: Text(
          'Entdecken Ansicht',
          style: TextStyle(fontSize: 22),
        ),
        body: Text(
          'Schaue regelmäßig in der \'Entdecken\' Ansicht vorbei um deinen bestehenden '
              'Wortschatz neu zu entdecken. Durch Tippen auf eine der Karten, gelangst du direkt zur Google Suche!',
          style: TextStyle(fontSize: 20),
        ),
        mainImage: Image.asset(
          'assets/images/onboarding_1.png',
          alignment: Alignment.center,
        )),
    PageViewModel(
        pageColor: Colors.pink,
        title: Text(
          'Erweiterte Funktionen',
          style: TextStyle(fontSize: 22),
        ),
        body: Text(
          'Es gibt viele weitere Ansichten, wie z.B. \'Statistiken\' '
              'oder \'Werkzeuge\'!\n\nSchaue dich jetzt einfach in der App weiter um, viel Spaß!',
          style: TextStyle(fontSize: 20),
        ),
        mainImage: Image.asset(
          'assets/images/onboarding_3.png',
          alignment: Alignment.center,
        )),
//    PageViewModel(
//        pageColor: Colors.blue,
//        title: Text(
//          'Fertig',
//          style: TextStyle(fontSize: 22),
//        ),
//        body: Text(
//          'Viel Spaß mit dieser App!',
//          style: TextStyle(fontSize: 20),
//        ),
//        mainImage: Image.asset(
//          'assets/images/header_light.jpg',
//          height: 285.0,
//          width: 285.0,
//          alignment: Alignment.center,
//        )),
  ];

  return IntroViewsFlutter(
    pages,
    doneText: Text('Erledigt'),
    nextText: Text('Weiter'),
    skipText: Text('Ende'),
    backText: Text('Zurück'),
    showSkipButton: false,
    showNextButton: false,
    onTapDoneButton: () {
      Navigator.of(context).pop();
    },
    pageButtonTextStyles: TextStyle(
      color: Colors.white,
      fontSize: 18.0,
    ),
  );
}
