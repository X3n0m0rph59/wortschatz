/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/data_access_layer_edit_history.dart';
import 'package:wortschatz/data/data_access_layer_tags.dart';
import 'package:wortschatz/data/data_access_layer_words.dart';
import 'package:wortschatz/data/edit_history_list_model.dart';
import 'package:wortschatz/data/tag_list_model.dart';
import 'package:wortschatz/data/word_list_model.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/date_time.dart';
import 'package:wortschatz/util/format.dart';
import 'package:wortschatz/widgets/background_image.dart';
import 'package:wortschatz/widgets/diagonal_clipper.dart';
import 'package:wortschatz/widgets/drawer.dart';

class StatisticsPage extends StatefulWidget {
  StatisticsPage({Key key}) : super(key: key);

  @override
  _StatisticsPageState createState() => _StatisticsPageState();
}

class _StatisticsPageState extends State<StatisticsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<ImageProvider> _avatarImage;
  final double _imageHeight = Config.imageHeight;

  WordListModel listModel, listModelDictionary;
  TagListModel listModelTags;
  EditHistoryListModel listModelEditHistory;

  final sharedPreferences = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    listModel = WordListModel(
        context, Config.wordsDatabase, null, columnId, SortOrder.ascending);
    listModelDictionary = WordListModel(context, Config.dictionaryDatabase,
        null, columnId, SortOrder.ascending);

    listModelTags = TagListModel(
        context, Config.wordsDatabase, null, columnId, SortOrder.ascending);

    listModelEditHistory = EditHistoryListModel(context,
        Config.editHistoryDatabase, null, columnId, SortOrder.ascending);

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    var prefs = await sharedPreferences;
    final String path = prefs.getString('avatar_file');

    if (path != null && path.isNotEmpty)
      setState(() {
        _avatarImage = Future.value(FileImage(File(path)));
      });

    return Future.value();
  }

  @override
  void dispose() {
    listModel.dispose();
    listModelDictionary.dispose();
    listModelTags.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context, DrawerItemIndex.statisticsPage),
      body: Builder(
        // Create an inner BuildContext so that methods
        // can refer to the Scaffold with Scaffold.of().
        builder: (BuildContext context) {
          return BackgroundImage(
            assetName: 'assets/images/background.png',
            child: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  _buildImage(),
                  _buildTopHeader(context),
                  _buildProfileRow(),
                  _buildBottomPart(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildImage() {
    final String assetName = Theme.of(context).brightness == Brightness.dark
        ? Config.assetsHeaderDark
        : Config.assetsHeaderLight;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Positioned.fill(
        bottom: null,
        child: ClipPath(
          clipper: DiagonalClipper(),
          child: Image.asset(
            assetName,
            fit: BoxFit.cover,
            height: _imageHeight,
          ),
        ),
      );
    });
  }

  Widget _buildTopHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 32.0),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(Icons.menu, size: 32.0, color: Colors.white),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                'Statistiken',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
//          Icon(Icons.linear_scale, color: Colors.white),
        ],
      ),
    );
  }

  Widget _buildProfileRow() {
    return Padding(
      padding: EdgeInsets.only(
          left: 32.0, top: (_imageHeight / Config.profileRowPaddingFactor)),
      child: GestureDetector(
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserProfilePage()),
            ),
        child: Row(
          children: <Widget>[
            FutureBuilder(
                future: _avatarImage,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return CircleAvatar(
                      backgroundImage: snapshot.data,
                      minRadius: 24,
                      maxRadius: 24,
                    );
                  } else {
                    return CircleAvatar(
                      minRadius: 24.0,
                      maxRadius: 24.0,
                      backgroundImage: AssetImage('assets/images/avatar.png'),
                    );
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FutureBuilder<SharedPreferences>(
                    future: sharedPreferences,
                    builder: (BuildContext context,
                        AsyncSnapshot<SharedPreferences> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapshot.data.getString('username'),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              snapshot.data.getString('database_profile'),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        );
                      } else {
                        return Text('');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    return Padding(
      padding: EdgeInsets.only(top: _imageHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          _buildContent(),
        ],
      ),
    );
  }

  Widget _buildContent() {
    var listModelLength = listModel.length;
    var listModelTagsLength = listModelTags.length;
    var listModelDictionaryLength = listModelDictionary.length;

    var resultEditsPerDay =
        listModelEditHistory.getEditsPerDayStats(Config.diagramNumDataSets);
    var resultWordsPerDay =
        listModel.getWordsPerDayStats(Config.diagramNumDataSets);
    var resultTagsPerDay =
        listModelTags.getTagsPerDayStats(Config.diagramNumDataSets);

    return Padding(
      padding: EdgeInsets.only(left: 16, top: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 48, bottom: 4.0),
                child: FutureBuilder<int>(
                  future: listModelLength,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        snapshot.data != null) {
                      return Text(
                          'Wortschatz: ${formatNumber(snapshot.data)} Einträge',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ));
                    } else {
                      return Text('Wortschatz: ...');
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 48, bottom: 4.0),
                child: FutureBuilder<int>(
                  future: listModelTagsLength,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        snapshot.data != null) {
                      return Text(
                          'Schlagwörter: ${formatNumber(snapshot.data)} Einträge',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ));
                    } else {
                      return Text('Schlagwörter: ...');
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 48, bottom: 4.0),
                child: FutureBuilder<int>(
                  future: listModelDictionaryLength,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        snapshot.data != null) {
                      return Text(
                          'Wörterbuch: ${formatNumber(snapshot.data)} Einträge',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ));
                    } else {
                      return Text('Wörterbuch: ...');
                    }
                  },
                ),
              ),
              FutureBuilder<List<EditsPerDay>>(
                future: resultEditsPerDay,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: _buildEditsPerDayChart(snapshot.data),
                    );
                  } else {
                    return Text('');
                  }
                },
              ),
              FutureBuilder<List<WordsPerDay>>(
                future: resultWordsPerDay,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: _buildWordsPerDayChart(snapshot.data),
                    );
                  } else {
                    return Text('');
                  }
                },
              ),
              FutureBuilder<List<TagsPerDay>>(
                future: resultTagsPerDay,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: _buildTagsPerDayChart(snapshot.data),
                    );
                  } else {
                    return Text('');
                  }
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 64.0, top: Config.simpleHeaderPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Statistiken',
            style: TextStyle(fontSize: 34.0),
          ),
          Text(
            'Statistiken für den aktuellen Benutzer',
            style: TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ],
      ),
    );
  }
}

Widget _buildEditsPerDayChart(List<EditsPerDay> data) {
  var series = [
    new charts.Series(
      id: 'Edits',
      domainFn: (EditsPerDay data, _) => formatDateShort(data.date),
      measureFn: (EditsPerDay data, _) => data.wordCount,
      colorFn: (EditsPerDay data, _) => charts.Color.fromHex(code: 'FFA000FF'),
      data: data,
    ),
  ];

  var chart = new charts.BarChart(
    series,
    animate: true,
  );
  var chartWidget = new Padding(
    padding: new EdgeInsets.only(bottom: 32),
    child: new SizedBox(
      height: 200.0,
      child: chart,
    ),
  );

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text('Aktionen pro Tag'),
      chartWidget,
    ],
  );
}

Widget _buildWordsPerDayChart(List<WordsPerDay> data) {
  var series = [
    new charts.Series(
      id: 'Words',
      domainFn: (WordsPerDay data, _) => formatDateShort(data.date),
      measureFn: (WordsPerDay data, _) => data.wordCount,
      colorFn: (WordsPerDay data, _) => charts.Color.fromHex(code: '1B5E20FF'),
      data: data,
    ),
  ];

  var chart = new charts.BarChart(
    series,
    animate: true,
  );
  var chartWidget = new Padding(
    padding: new EdgeInsets.only(bottom: 32),
    child: new SizedBox(
      height: 200.0,
      child: chart,
    ),
  );

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text('Neue Wörter pro Tag'),
      chartWidget,
    ],
  );
}

Widget _buildTagsPerDayChart(List<TagsPerDay> data) {
  var series = [
    new charts.Series(
      id: 'Tags',
      domainFn: (TagsPerDay data, _) => formatDateShort(data.date),
      measureFn: (TagsPerDay data, _) => data.tagCount,
      colorFn: (TagsPerDay data, _) => charts.Color.fromHex(code: 'FF536DFF'),
      data: data,
    ),
  ];

  var chart = new charts.BarChart(
    series,
    animate: true,
  );
  var chartWidget = new Padding(
    padding: new EdgeInsets.only(bottom: 32),
    child: new SizedBox(
      height: 200.0,
      child: chart,
    ),
  );

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text('Neue Schlagwörter pro Tag'),
      chartWidget,
    ],
  );
}
