// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "backup" : MessageLookupByLibrary.simpleMessage("Create Backup"),
    "filtered" : MessageLookupByLibrary.simpleMessage("(filtered)"),
    "headerDictionary" : MessageLookupByLibrary.simpleMessage("Dictionary"),
    "info" : MessageLookupByLibrary.simpleMessage("Info"),
    "invalidInputIgnored" : MessageLookupByLibrary.simpleMessage("Invalid input ignored"),
    "listTitle" : MessageLookupByLibrary.simpleMessage("Words"),
    "listTitleDictionary" : MessageLookupByLibrary.simpleMessage("Dictionary"),
    "newWord" : MessageLookupByLibrary.simpleMessage("New Word"),
    "title" : MessageLookupByLibrary.simpleMessage("Vocabulary"),
    "undo" : MessageLookupByLibrary.simpleMessage("Undo"),
    "undoWordDeleted" : MessageLookupByLibrary.simpleMessage("Undone: Delete Word:"),
    "warning" : MessageLookupByLibrary.simpleMessage("Warning"),
    "wordDeleted" : MessageLookupByLibrary.simpleMessage("Word has been deleted"),
    "wordMany" : MessageLookupByLibrary.simpleMessage("Words")
  };
}
