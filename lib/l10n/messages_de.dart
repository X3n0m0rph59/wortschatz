// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'de';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "backup" : MessageLookupByLibrary.simpleMessage("Backup erstellen"),
    "filtered" : MessageLookupByLibrary.simpleMessage("(gefiltert)"),
    "headerDictionary" : MessageLookupByLibrary.simpleMessage("Wörterbuch"),
    "info" : MessageLookupByLibrary.simpleMessage("Info"),
    "invalidInputIgnored" : MessageLookupByLibrary.simpleMessage("Ungültige Eingabe wurde ignoriert"),
    "listTitle" : MessageLookupByLibrary.simpleMessage("Wortschatz"),
    "listTitleDictionary" : MessageLookupByLibrary.simpleMessage("Wörterbuch"),
    "newWord" : MessageLookupByLibrary.simpleMessage("Neues Wort"),
    "title" : MessageLookupByLibrary.simpleMessage("Wortschatz"),
    "undo" : MessageLookupByLibrary.simpleMessage("Rückgängig machen"),
    "undoWordDeleted" : MessageLookupByLibrary.simpleMessage("Rückgängig: Wort gelöscht:"),
    "warning" : MessageLookupByLibrary.simpleMessage("Warnung"),
    "wordDeleted" : MessageLookupByLibrary.simpleMessage("Wort wurde gelöscht"),
    "wordMany" : MessageLookupByLibrary.simpleMessage("Wörter")
  };
}
