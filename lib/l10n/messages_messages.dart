// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "about" : MessageLookupByLibrary.simpleMessage("Über diese App"),
    "aboutPage" : MessageLookupByLibrary.simpleMessage("Über diese App"),
    "aboutText" : MessageLookupByLibrary.simpleMessage("Wortschatz Copyright (C) 2018-2019;\nThis program comes with ABSOLUTELY NO WARRANTY;\nThis is free software, and you are welcome to redistribute it under certain conditions."),
    "action" : MessageLookupByLibrary.simpleMessage("Action: "),
    "backup" : MessageLookupByLibrary.simpleMessage("Backup erstellen"),
    "created" : MessageLookupByLibrary.simpleMessage("Erzeugt:"),
    "dates" : MessageLookupByLibrary.simpleMessage("Daten"),
    "dictionaryPage" : MessageLookupByLibrary.simpleMessage("Wörterbuch"),
    "drawerHeader" : MessageLookupByLibrary.simpleMessage("Mein\nWortschatz"),
    "editHistoryPage" : MessageLookupByLibrary.simpleMessage("Historie"),
    "enableDarkMode" : MessageLookupByLibrary.simpleMessage("Dunkles UI-Design aktivieren"),
    "enterTag" : MessageLookupByLibrary.simpleMessage("Schlagwort eingeben..."),
    "enterWord" : MessageLookupByLibrary.simpleMessage("Wort eingeben..."),
    "entryMany" : MessageLookupByLibrary.simpleMessage("Einträge"),
    "entryOne" : MessageLookupByLibrary.simpleMessage("Eintrag"),
    "error" : MessageLookupByLibrary.simpleMessage("Fehler"),
    "filter" : MessageLookupByLibrary.simpleMessage("Filtern"),
    "filterHistory" : MessageLookupByLibrary.simpleMessage("Historie filtern"),
    "filterHistoryHint" : MessageLookupByLibrary.simpleMessage("Historie filtern..."),
    "filterTags" : MessageLookupByLibrary.simpleMessage("Schlagwörter filtern"),
    "filterTagsHeading" : MessageLookupByLibrary.simpleMessage("Nach Schlagwort filtern"),
    "filterTagsHint" : MessageLookupByLibrary.simpleMessage("Schlagwörter filtern..."),
    "filterWords" : MessageLookupByLibrary.simpleMessage("Wörter filtern"),
    "filterWordsHint" : MessageLookupByLibrary.simpleMessage("Wörter filtern..."),
    "filtered" : MessageLookupByLibrary.simpleMessage("(gefiltert) "),
    "filteredDuplicates" : MessageLookupByLibrary.simpleMessage("(Duplikate) "),
    "headerAbout" : MessageLookupByLibrary.simpleMessage("Wortschatz"),
    "headerDictionary" : MessageLookupByLibrary.simpleMessage("Wörterbuch"),
    "headerHistory" : MessageLookupByLibrary.simpleMessage("Historie"),
    "headerMain" : MessageLookupByLibrary.simpleMessage("Mein Wortschatz"),
    "headerPreferences" : MessageLookupByLibrary.simpleMessage("Einstellungen"),
    "headerSpellCheck" : MessageLookupByLibrary.simpleMessage("Rechtschreibung"),
    "headerTags" : MessageLookupByLibrary.simpleMessage("Schlagwörter"),
    "info" : MessageLookupByLibrary.simpleMessage("Info"),
    "infoAbout" : MessageLookupByLibrary.simpleMessage("Info über "),
    "invalidDatabaseState" : MessageLookupByLibrary.simpleMessage("Zustand der Datenbank ungültig"),
    "invalidInput" : MessageLookupByLibrary.simpleMessage("Ungültige Eingabe"),
    "invalidInputIgnored" : MessageLookupByLibrary.simpleMessage("Ungültige Eingabe wurde ignoriert"),
    "listTitle" : MessageLookupByLibrary.simpleMessage("Wortschatz"),
    "listTitleDictionary" : MessageLookupByLibrary.simpleMessage("Wörterbuch"),
    "listTitleHistory" : MessageLookupByLibrary.simpleMessage("Historie"),
    "loading" : MessageLookupByLibrary.simpleMessage("Wird geladen..."),
    "mainPage" : MessageLookupByLibrary.simpleMessage("Mein Wortschatz"),
    "modified" : MessageLookupByLibrary.simpleMessage("Geändert:"),
    "newWord" : MessageLookupByLibrary.simpleMessage("Neues Wort"),
    "preferencesPage" : MessageLookupByLibrary.simpleMessage("Einstellungen"),
    "profilePage" : MessageLookupByLibrary.simpleMessage("Benutzerprofil"),
    "properties" : MessageLookupByLibrary.simpleMessage("Eigenschaften"),
    "restartRequired" : MessageLookupByLibrary.simpleMessage("Erfordert möglicherweise einen Neustart der App"),
    "save" : MessageLookupByLibrary.simpleMessage("Speichern"),
    "selectColorHeading" : MessageLookupByLibrary.simpleMessage("Farbe wählen"),
    "sortedAToZ" : MessageLookupByLibrary.simpleMessage("(A-Z) "),
    "sortedZToA" : MessageLookupByLibrary.simpleMessage("(Z-A) "),
    "spellCheckPage" : MessageLookupByLibrary.simpleMessage("Rechtschreibung"),
    "statisticsPage" : MessageLookupByLibrary.simpleMessage("Statistiken"),
    "subHeaderAbout" : MessageLookupByLibrary.simpleMessage("Über diese App"),
    "subHeaderPreferences" : MessageLookupByLibrary.simpleMessage("Einstellungen der App ändern"),
    "subHeaderSpellCheck" : MessageLookupByLibrary.simpleMessage("Rechtschreibung eines Wortes prüfen"),
    "tagAdded" : MessageLookupByLibrary.simpleMessage("Schlagwort hinzugefügt"),
    "tagAssociated" : MessageLookupByLibrary.simpleMessage("Schlagwort hinzugefügt"),
    "tagColorChanged" : MessageLookupByLibrary.simpleMessage("Farbe geändert"),
    "tagDeleted" : MessageLookupByLibrary.simpleMessage("Schlagwort gelöscht"),
    "tagEdited" : MessageLookupByLibrary.simpleMessage("Schlagwort bearbeitet"),
    "tagFavoriteChanged" : MessageLookupByLibrary.simpleMessage("Favorit geändert"),
    "tagHasBeenDeleted" : MessageLookupByLibrary.simpleMessage("Schlagwort wurde gelöscht"),
    "tagUnAssociated" : MessageLookupByLibrary.simpleMessage("Schlagwort entfernt"),
    "tags" : MessageLookupByLibrary.simpleMessage("Schlagwörter"),
    "tagsModified" : MessageLookupByLibrary.simpleMessage("Schlagwörter geändert"),
    "tagsModifiedDictionary" : MessageLookupByLibrary.simpleMessage("Schlagwörter geändert (Wörterbuch)"),
    "tagsPage" : MessageLookupByLibrary.simpleMessage("Schlagwörter"),
    "title" : MessageLookupByLibrary.simpleMessage("Mein Wortschatz"),
    "toolsPage" : MessageLookupByLibrary.simpleMessage("Tools"),
    "undo" : MessageLookupByLibrary.simpleMessage("Rückgängig machen"),
    "undoCompleted" : MessageLookupByLibrary.simpleMessage("Änderung wurde rückgängig gemacht"),
    "undoError" : MessageLookupByLibrary.simpleMessage("Änderung konnte nicht rückgängig gemacht werden"),
    "undoTagAdded" : MessageLookupByLibrary.simpleMessage("Rückgängig: Schlagwort hinzugefügt:"),
    "undoTagColorChanged" : MessageLookupByLibrary.simpleMessage("Rückgängig: Farbe geändert"),
    "undoTagDeleted" : MessageLookupByLibrary.simpleMessage("Rückgängig: Schlagwort gelöscht"),
    "undoTagEdited" : MessageLookupByLibrary.simpleMessage("Rückgängig: Schlagwort bearbeitet:"),
    "undoTagFavoriteChanged" : MessageLookupByLibrary.simpleMessage("Rückgängig: Favorit geändert"),
    "undoWordAdded" : MessageLookupByLibrary.simpleMessage("Rückgängig: Wort hinzugefügt:"),
    "undoWordColorChanged" : MessageLookupByLibrary.simpleMessage("Rückgängig: Farbe geändert:"),
    "undoWordDeleted" : MessageLookupByLibrary.simpleMessage("Rückgängig: Wort gelöscht:"),
    "undoWordEdited" : MessageLookupByLibrary.simpleMessage("Rückgängig: Wort bearbeitet:"),
    "undoWordFavoriteChanged" : MessageLookupByLibrary.simpleMessage("Rückgängig: Favorit geändert:"),
    "undoWordTagAdded" : MessageLookupByLibrary.simpleMessage("Rückgängig: Schlagwort hinzugefügt:"),
    "undoWordTagChanged" : MessageLookupByLibrary.simpleMessage("Rückgängig: Schlagwörter geändert:"),
    "undoWordTagRemoved" : MessageLookupByLibrary.simpleMessage("Rückgängig: Schlagwort entfernt:"),
    "version" : MessageLookupByLibrary.simpleMessage("Version: "),
    "warning" : MessageLookupByLibrary.simpleMessage("Warnung"),
    "wordAdded" : MessageLookupByLibrary.simpleMessage("Wort hinzugefügt"),
    "wordAddedDictionary" : MessageLookupByLibrary.simpleMessage("Wort hinzugefügt (Wörterbuch)"),
    "wordCards" : MessageLookupByLibrary.simpleMessage("Wort Karten"),
    "wordColorChanged" : MessageLookupByLibrary.simpleMessage("Farbe geändert"),
    "wordColorChangedDictionary" : MessageLookupByLibrary.simpleMessage("Farbe geändert (Wörterbuch)"),
    "wordDeleted" : MessageLookupByLibrary.simpleMessage("Wort gelöscht"),
    "wordDeletedDictionary" : MessageLookupByLibrary.simpleMessage("Wort gelöscht (Wörterbuch)"),
    "wordEdited" : MessageLookupByLibrary.simpleMessage("Wort bearbeitet"),
    "wordEditedDictionary" : MessageLookupByLibrary.simpleMessage("Wort bearbeitet (Wörterbuch)"),
    "wordFavoriteChanged" : MessageLookupByLibrary.simpleMessage("Favorit geändert"),
    "wordFavoriteChangedDictionary" : MessageLookupByLibrary.simpleMessage("Favorit geändert (Wörterbuch)"),
    "wordHasBeenDeleted" : MessageLookupByLibrary.simpleMessage("Wort wurde gelöscht"),
    "wordMany" : MessageLookupByLibrary.simpleMessage("Wörter"),
    "wordOne" : MessageLookupByLibrary.simpleMessage("Wort")
  };
}
