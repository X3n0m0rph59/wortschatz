/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/pages/main_page.dart';
import 'package:wortschatz/util/l10n.dart';

/// Main program entry point
void main() async {
  final sharedPreferences = SharedPreferences.getInstance();
  var prefs = await sharedPreferences;

  bool darkUiMode = Config.defaultDarkUi;
  try {
    darkUiMode = prefs.getBool('dark_ui_mode') ?? Config.defaultDarkUi;
  } catch (e) {
    prefs.setBool('dark_ui_mode', Config.defaultDarkUi);
  }

  runApp(WortschatzApp(darkUiMode: darkUiMode));
}

/// Main Class
class WortschatzApp extends StatelessWidget {
  final bool darkUiMode;

  WortschatzApp({this.darkUiMode: Config.defaultDarkUi});

  // Dark visual theme
  final darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.grey[800],
    primaryColorLight: Colors.grey[800],
    primaryColorDark: Colors.grey[200],
    primarySwatch: Colors.grey,
    accentColor: Colors.grey[800],
    splashColor: Colors.grey[400],
    backgroundColor: Colors.grey[900],
    scaffoldBackgroundColor: Colors.grey[900],
    canvasColor: Colors.grey[900],
    dialogBackgroundColor: Colors.grey[900],
    fontFamily: 'Roboto',
    primaryTextTheme: Typography(platform: TargetPlatform.android).white,
    textTheme: Typography(platform: TargetPlatform.android).white,
  );

  // Light visual theme
  final lightTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.white,
    primaryColorLight: Colors.white,
    primaryColorDark: Colors.grey[800],
    primarySwatch: Colors.grey,
    accentColor: Colors.grey[800],
    splashColor: Colors.grey[300],
    backgroundColor: Colors.grey[200],
    scaffoldBackgroundColor: Colors.grey[200],
    canvasColor: Colors.grey[200],
    dialogBackgroundColor: Colors.grey[200],
    fontFamily: 'Roboto',
    primaryTextTheme: Typography(platform: TargetPlatform.android).black,
    textTheme: Typography(platform: TargetPlatform.android).black,
  );

  void _initializeLocalizations(BuildContext context, String localeName) async {
    try {
      // await initializeDateFormatting(localeName, null);
      // await initializeNumberFormatting(localeName, null);
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.dark,
      data: (brightness) {
        if (brightness == Brightness.dark) {
          return darkTheme;
        } else {
          return lightTheme;
        }
      },
      themedWidgetBuilder: (context, theme) {
        SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(
            systemNavigationBarColor:
                darkUiMode ? Colors.grey[900] : Colors.grey[200],
            systemNavigationBarIconBrightness:
                darkUiMode ? Brightness.light : Brightness.dark,
          ),
        );

        return MaterialApp(
          // locale: Locale('en'),
          supportedLocales: [
            const Locale('de', 'DE'), // German
            const Locale('en', 'US'), // US English
          ],
          localizationsDelegates: [
            CustomLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          localeListResolutionCallback:
              (List<Locale> locales, Iterable<Locale> supportedLocales) {
            for (final locale in locales) {
              final String localeName =
                  '${locale.languageCode}_${locale.countryCode}';

              _initializeLocalizations(context, localeName);
            }
          },
          onGenerateTitle: (BuildContext context) =>
              CustomLocalizations.of(context).title,
          theme: theme,
          home: new MainPage(),
        );
      },
    );
  }
}
