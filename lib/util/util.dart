/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:wortschatz/data/data_access_layer_base.dart';

/// Converts an int value to its boolean representation
/// whereby null and 0 == false, all other values == true
bool intToBool(int val) {
  if (val == null)
    return false;
  else if (val == 0)
    return false;
  else
    return true;
}

/// Checks whether the [MappableEntry] entry is contained in the [List] list
bool isEntryContainedIn<T extends MappableEntry, L extends List<MappableEntry>>(
    T entry, L list) {
  for (final T e in list) {
    if (e.id == entry.id) return true;
  }

  return false;
}

/// Get the index of the [MappableEntry] from a list
int getIndexOfEntry<T extends MappableEntry, L extends List<MappableEntry>>(
    T entry, L list) {
  int cnt = 0;

  for (final T e in list) {
    if (e.id == entry.id) return cnt;
    cnt++;
  }

  return -1;
}

Future<List<T>> cloneList<T>(Future<List<T>> other) async {
  return Future.value(List<T>.from(await other ?? <T>[]));
}
