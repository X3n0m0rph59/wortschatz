/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:wortschatz/data/data_access_layer_base.dart';
import 'package:wortschatz/data/data_access_layer_tags.dart';
import 'package:wortschatz/data/data_access_layer_words.dart';
import 'package:wortschatz/data/data_access_layer_words_tags.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/data/words_tags_entry.dart';
import 'package:wortschatz/util/date_time.dart';

bool atLeastOneRowAffected(rowsAffected) {
  for (var value in rowsAffected /*??= <int>[0]*/) {
    if (value != 0) return true;
  }

  return false;
}

void mergeDatabases(String destDb, String srcDb, progressCallback) async {
  const int STRIDE_SIZE = 5;

  DataAccessLayerWords destDal =
      DataAccessLayerWords(destDb, null, columnId, SortOrder.ascending);
  DataAccessLayerTags destDalTags =
      DataAccessLayerTags(destDb, null, columnId, SortOrder.ascending);
  DataAccessLayerWordsTags destDalWordsTags =
      DataAccessLayerWordsTags(destDb, null, columnId, SortOrder.ascending);

  DataAccessLayerWords srcDal =
      DataAccessLayerWords(srcDb, null, columnId, SortOrder.ascending);

  int numSrcEntries = await srcDal.getNumEntries();

  // Create 'imported at date' tag
  final DateTime createdAt = DateTime.now();
  final String shortDate = formatDateShort(createdAt);
  final String longDate = formatDateTime(createdAt);

  final TagEntry tagEntry = TagEntry(
    tag: '$shortDate',
    description: 'Importiert am: $longDate',
    color: Colors.lightGreenAccent,
    favorite: false,
    createdAt: createdAt,
  );
  final TagEntry tag = await destDalTags.insert(tagEntry);

  for (int index = 0; index < numSrcEntries; index += STRIDE_SIZE) {
    final List<WordEntry> srcEntries =
        await srcDal.getEntries(index, STRIDE_SIZE);

    for (var word in srcEntries) {
      // import the word
      word.id = null;
      // word.createdAt = DateTime.now();
      word.modifiedAt = DateTime.now();
      final WordEntry result = await destDal.insert(word);

      // associate 'imported at' tag with the new word
      final WordsTagsEntry wordsTagsEntry = WordsTagsEntry(
          wordId: result.id,
          tagId: tag.id,
          createdAt: DateTime.now(),
          modifiedAt: DateTime.now());
      destDalWordsTags.insert(wordsTagsEntry);

      try {
        progressCallback(index, numSrcEntries, word);
      } catch (e) {
        // eat exceptions here
      }
    }
  }
}

Future<void> copyDatabase(
    String srcDb, String destDb, List<String> tables) async {
  // to be on the safe side, ensure that we don't open an existing database
  deleteDatabase(destDb);

  final source = await openDatabase(srcDb);
  final dest = await openDatabase(destDb);

  // create schema in destination db
  for (var table in tables) {
    final result = await source
        .rawQuery('select sql from sqlite_master where name=?', [table]);

    print('$table:\n$result\n');

    final String sql = result[0]['sql'];

    try {
      await dest.rawQuery(sql);
    } on DatabaseException catch (e) {
      print(e);
    }
  }

  // copy all datasets to the destination db
  await dest.transaction((tx) async {
    final destBatch = tx.batch();

    for (final table in tables) {
      final result = await source.query(table);

      for (final row in result) {
        destBatch.insert(table, row);
      }
    }

    await destBatch.commit();
  });

  // finally, optimize the destination db
  await dest.rawQuery('pragma optimize');

  // source.close();
  dest.close();
}

Future<void> importDatabase(
    String backupDb, String destDb, List<String> tables) async {
  final source = await openDatabase(backupDb);
  final dest = await openDatabase(destDb);

  // create schema in destination db
  for (var table in tables) {
    final result = await source
        .rawQuery('select sql from sqlite_master where name=?', [table]);

    // print('$table:\n$result\n');

    final String sql = result[0]['sql'];

    try {
      await dest.rawQuery(sql);
    } on DatabaseException catch (e) {
      // eat exceptions; try to continue as long as possible
      print(e);
    }
  }

  // copy all datasets from the backup to the destination db
  await dest.transaction((tx) async {
    final destBatch = tx.batch();

    for (final table in tables) {
      destBatch.delete(table);

      final result = await source.query(table);

      for (final row in result) {
        destBatch.insert(table, row);
      }
    }

    await destBatch.commit();
  });

  // finally, optimize the destination db
  await dest.rawQuery('pragma optimize');

  source.close();
  // dest.close();
}

void optimizeDatabase(String filename) async {
  final db = await openDatabase(filename);

  await db.rawQuery('pragma optimize');

  db.close();
}
