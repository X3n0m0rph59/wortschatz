/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/exceptions/value_error.dart';

String sanitizeProfileName(String profileName) {
  String result = profileName;

  result = result.replaceAll('/', ' ');
  result = result.trim();

  if (result.length < 1) throw ValueError('Ungültiger Profilname');

  return result;
}

class DatabaseProfileManager {
  static final DatabaseProfileManager _singleton =
      new DatabaseProfileManager._internal();

  factory DatabaseProfileManager() {
    return _singleton;
  }

  DatabaseProfileManager._internal();

  Future<String> get currentDatabaseProfile async =>
      getCurrentDatabaseProfile();

  Future<String> getCurrentDatabaseProfile() async {
    final sharedPreferences = SharedPreferences.getInstance();
    var prefs = await sharedPreferences;

    final String databaseProfile = prefs.getString('database_profile');

    if (databaseProfile != null && databaseProfile.isNotEmpty) {
      return databaseProfile;
    } else {
      setCurrentDatabaseProfile(Config.defaultDatabaseProfile);
      return Config.defaultDatabaseProfile;
    }
  }

  Future<void> setCurrentDatabaseProfile(String profileName) async {
    print('Switching to database profile: $profileName');

    final sharedPreferences = SharedPreferences.getInstance();
    var prefs = await sharedPreferences;

    prefs.setString('database_profile', profileName);
  }

  Future<void> createDatabaseProfileIfMissing() async {
    final profileName = currentDatabaseProfile;
    final dbDir = getDatabasesPath();
    final Directory dir = Directory(join(await dbDir, await profileName));

    dir.createSync(recursive: true);
  }

  Future<List<String>> getDatabaseProfiles() async {
    final String dbDir = await getDatabasesPath();
    final Directory dir = Directory(dbDir);

    final entities = dir.listSync();
    final result = List<String>();

    entities.forEach((entity) {
      if (entity.statSync().type == FileSystemEntityType.directory) {
        final profile = basename(entity.path);
        result.add(profile);

        print('Found database profile: $profile');
      }
    });

    print('# of database profiles: ${result.length}');

    return result;
  }

  Future<void> addDatabaseProfile(String profileName) async {
    final String dbDir = await getDatabasesPath();
    final Directory dir = Directory(join(dbDir, profileName));

    dir.createSync(recursive: true);

    print('Added new database profile: $profileName');
  }

  Future<void> renameDatabaseProfile(
      String profileName, String newProfileName) async {
    final String dbDir = await getDatabasesPath();
    final Directory dir = Directory(join(dbDir, profileName));

    dir.renameSync(newProfileName);

    print('Renamed database profile: $profileName => $newProfileName');
  }

  Future<void> deleteDatabaseProfile(String profileName) async {
    final String dbDir = await getDatabasesPath();
    final Directory dir = Directory(join(dbDir, profileName));

    dir.deleteSync(recursive: true);

    print('Removed database profile: $profileName');
  }
}
