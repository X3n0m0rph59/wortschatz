/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:intl/intl.dart';

/// Map boolean values to its String representation
String boolToString(bool val) {
  return val ? 'ja' : 'nein';
}

/// Format val utilizing the current locale
String formatNumber(int val) {
  final formatter = NumberFormat.decimalPattern();

  return formatter.format(val);
}
