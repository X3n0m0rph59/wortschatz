/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:io';

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:path/path.dart';

void createZipArchive(String outFile, List<String> srcFiles) {
  var encoder = new ZipFileEncoder();

  encoder.create(outFile);

  for (var filename in srcFiles) {
    encoder.addFile(new File(filename));
  }

  encoder.close();
}

void unzipArchive(String zipFile, String outDir) {
  List<int> bytes = new File(zipFile).readAsBytesSync();

  // Decode the Zip file
  Archive archive = new ZipDecoder().decodeBytes(bytes);

  // Extract the contents of the Zip archive to storage
  for (ArchiveFile file in archive) {
    String filename = file.name;

    if (file.isFile) {
      List<int> data = file.content;
      new File(join(outDir, filename))
        ..createSync(recursive: true)
        ..writeAsBytesSync(data);
    } else {
      new Directory(join(outDir, filename))..create(recursive: true);
    }
  }
}
