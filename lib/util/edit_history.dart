/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:wortschatz/data/edit_history.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';

void logAddWordAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordAdded,
      LogPayload(before, now), context);
}

void logEditWordAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordEdited,
      LogPayload(before, now), context);
}

void logChangeWordColorAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordColorChanged,
      LogPayload(before, now), context);
}

void logChangeWordFavoriteAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordFavoriteChanged,
      LogPayload(before, now), context);
}

void logChangeWordTagAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordTagChanged,
      LogPayload(before, now), context);
}

void logDeleteWordAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordDeleted,
      LogPayload(before, now), context);
}

void logAddDictionaryWordAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordAddedDictionary,
      LogPayload(before, now), context);
}

void logEditDictionaryWordAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordEditedDictionary,
      LogPayload(before, now), context);
}

void logChangeDictionaryWordColorAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordColorChangedDictionary,
      LogPayload(before, now), context);
}

void logChangeDictionaryWordFavoriteAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordFavoriteChangedDictionary,
      LogPayload(before, now), context);
}

void logChangeDictionaryWordTagAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordTagChangedDictionary,
      LogPayload(before, now), context);
}

void logDeleteDictionaryWordAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordDeletedDictionary,
      LogPayload(before, now), context);
}

void logAddTagAction(
    String description, TagEntry before, TagEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.tagAdded,
      LogPayload(before, now), context);
}

void logEditTagAction(
    String description, TagEntry before, TagEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.tagEdited,
      LogPayload(before, now), context);
}

void logChangeTagColorAction(
    String description, TagEntry before, TagEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.tagColorChanged,
      LogPayload(before, now), context);
}

void logChangeTagFavoriteAction(
    String description, TagEntry before, TagEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.tagFavoriteChanged,
      LogPayload(before, now), context);
}

void logDeleteTagAction(
    String description, TagEntry before, TagEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.tagDeleted,
      LogPayload(before, now), context);
}

void logWordTagAddAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordTagAdded,
      LogPayload(before, now), context);
}

void logWordTagRemoveAction(
    String description, WordEntry before, WordEntry now, context) {
  final EditActionLogger logger = EditActionLogger();

  logger.logAction(description, EditHistoryAction.wordTagRemoved,
      LogPayload(before, now), context);
}
