import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:wortschatz/l10n/messages_all.dart';

class CustomLocalizations {
  BuildContext context;

  static Future<CustomLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return CustomLocalizations();
    });
  }

  static CustomLocalizations of(BuildContext context) {
    context = context;
    return Localizations.of<CustomLocalizations>(context, CustomLocalizations);
  }

  // Strings
  String get title =>
      Intl.message('Mein Wortschatz', name: 'title', desc: 'App title');

  String get drawerHeader => Intl.message('Mein\nWortschatz',
      name: 'drawerHeader', desc: 'Drawer header');

  String get mainPage => Intl.message('Mein Wortschatz',
      name: 'mainPage', desc: 'Drawer menu item: Main page');
  String get tagsPage => Intl.message('Schlagwörter',
      name: 'tagsPage', desc: 'Drawer menu item: Tags page');
  String get dictionaryPage => Intl.message('Wörterbuch',
      name: 'dictionaryPage', desc: 'Drawer menu item: Dictionary page');
  String get spellCheckPage => Intl.message('Rechtschreibung',
      name: 'spellCheckPage', desc: 'Drawer menu item: Spell check page');
  String get discoverPage => Intl.message('Entdecken',
      name: 'cardsPage', desc: 'Drawer menu item: Cards page');

  String get statisticsPage => Intl.message('Statistiken',
      name: 'statisticsPage', desc: 'Drawer menu item: Statistics page');
  String get profilePage => Intl.message('Benutzerprofil',
      name: 'profilePage', desc: 'Drawer menu item: Profile page');
  String get editHistoryPage => Intl.message('Historie',
      name: 'editHistoryPage', desc: 'Drawer menu item: History page');

  String get databaseSelectorPage => Intl.message('Datenbank Manager',
      name: 'databaseSelectorPage',
      desc: 'Drawer menu item: Database Selector page');
  String get toolsPage => Intl.message('Werkzeuge',
      name: 'toolsPage', desc: 'Drawer menu item: Tools page');
  String get preferencesPage => Intl.message('Einstellungen',
      name: 'preferencesPage', desc: 'Drawer menu item: Preferences page');
  String get helpPage => Intl.message('Hilfe & Erklärungen',
      name: 'helpPage', desc: 'Drawer menu item: Help page');
  String get aboutPage => Intl.message('Über diese App',
      name: 'aboutPage', desc: 'Drawer menu item: About page');

  String get headerMain => Intl.message('Mein Wortschatz',
      name: 'headerMain', desc: 'Title of the main page');
  String get headerTags => Intl.message('Schlagwörter',
      name: 'headerTags', desc: 'Title of the tags page');
  String get headerDictionary => Intl.message('Wörterbuch',
      name: 'headerDictionary', desc: 'Title of the dictionary page');
  String get headerHistory => Intl.message('Historie',
      name: 'headerHistory', desc: 'Title of the history page');
  String get headerSpellCheck => Intl.message('Rechtschreibung',
      name: 'headerSpellCheck', desc: 'Title of the spelling check page');
  String get headerPreferences => Intl.message('Einstellungen',
      name: 'headerPreferences', desc: 'Title of the preferences page');
  String get headerAbout => Intl.message('Wortschatz',
      name: 'headerAbout', desc: 'About page header');

  String get subHeaderSpellCheck =>
      Intl.message('Rechtschreibung eines Wortes prüfen',
          name: 'subHeaderSpellCheck', desc: 'Spelling check page sub-header');
  String get subHeaderAbout => Intl.message('Über diese App',
      name: 'subHeaderAbout', desc: 'About page sub-header');
  String get subHeaderPreferences =>
      Intl.message('Einstellungen der App ändern',
          name: 'subHeaderPreferences', desc: 'Preferences page sub-header');

  String get listTitle => Intl.message('Wortschatz',
      name: 'listTitle', desc: 'Title of the entry list on the main page');
  String get listTitleDictionary => Intl.message('Wörterbuch',
      name: 'listTitleDictionary',
      desc: 'Title of the entry list on the main page');
  String get listTitleHistory => Intl.message('Historie',
      name: 'listTitleHistory',
      desc: 'Title of the history list on the main page');

  String get error =>
      Intl.message('Fehler', name: 'error', desc: 'Error message title');
  String get warning =>
      Intl.message('Warnung', name: 'warning', desc: 'Warning message title');
  String get info =>
      Intl.message('Info', name: 'info', desc: 'Info message title');

  String get undo =>
      Intl.message('Rückgängig machen', name: 'undo', desc: 'Undo text');

  String get undoCompleted => Intl.message('Änderung wurde rückgängig gemacht',
      name: 'undoCompleted', desc: 'Undo completed message');

  String get undoError =>
      Intl.message('Änderung konnte nicht rückgängig gemacht werden',
          name: 'undoError', desc: 'Undo could not be completed message');

  String get invalidInput => Intl.message('Ungültige Eingabe',
      name: 'invalidInput', desc: 'Invalid input');

  String get invalidInputIgnored =>
      Intl.message('Ungültige Eingabe wurde ignoriert',
          name: 'invalidInputIgnored', desc: 'Invalid input message text');

  String get wordHasBeenDeleted => Intl.message('Wort wurde gelöscht',
      name: 'wordHasBeenDeleted', desc: 'Entry deleted text on main page');
  String get tagHasBeenDeleted => Intl.message('Schlagwort wurde gelöscht',
      name: 'tagHasBeenDeleted', desc: 'Entry deleted text on tags page');

  String get undoWordAdded => Intl.message('Rückgängig: Wort hinzugefügt:',
      name: 'undoWordAdded', desc: 'Deleted a newly added entry');
  String get undoWordEdited => Intl.message('Rückgängig: Wort bearbeitet:',
      name: 'undoWordEdited', desc: 'Restored previous revision of an entry');
  String get undoWordDeleted => Intl.message('Rückgängig: Wort gelöscht:',
      name: 'undoWordDeleted', desc: 'Recovered deleted entry');
  String get undoWordColorChanged => Intl.message('Rückgängig: Farbe geändert:',
      name: 'undoWordColorChanged',
      desc: 'Recovered previous color of an entry');
  String get undoWordFavoriteChanged =>
      Intl.message('Rückgängig: Favorit geändert:',
          name: 'undoWordFavoriteChanged',
          desc: 'Recovered previous favorite state of an entry');

  String get undoWordTagChanged =>
      Intl.message('Rückgängig: Schlagwörter geändert:',
          name: 'undoWordTagChanged',
          desc: 'Recovered previous state of associated tags');
  String get undoWordTagAdded =>
      Intl.message('Rückgängig: Schlagwort hinzugefügt:',
          name: 'undoWordTagAdded', desc: 'Tag de-associated');
  String get undoWordTagRemoved =>
      Intl.message('Rückgängig: Schlagwort entfernt:',
          name: 'undoWordTagRemoved', desc: 'Tag associated');

  String get undoTagAdded => Intl.message('Rückgängig: Schlagwort hinzugefügt:',
      name: 'undoTagAdded', desc: 'Delete a newly added tag');
  String get undoTagEdited => Intl.message('Rückgängig: Schlagwort bearbeitet:',
      name: 'undoTagEdited', desc: 'Restored previous revision of a tag');
  String get undoTagDeleted => Intl.message('Rückgängig: Schlagwort gelöscht',
      name: 'undoTagDeleted', desc: 'Recovered deleted tag');
  String get undoTagColorChanged => Intl.message('Rückgängig: Farbe geändert',
      name: 'undoTagColorChanged', desc: 'Recovered previous color of a tag');
  String get undoTagFavoriteChanged =>
      Intl.message('Rückgängig: Favorit geändert',
          name: 'undoTagFavoriteChanged',
          desc: 'Recovered previous favorite state of a tag');

  String get filtered => Intl.message('(gefiltert)',
      name: 'filtered', desc: 'Entry list filtered indicator');
  String get filteredDuplicates => Intl.message('(zeige Duplikate)',
      name: 'filteredDuplicates', desc: 'Entry list dupes filtered indicator');

  String get sortedAToZ => Intl.message('(A->Z)',
      name: 'sortedAToZ', desc: 'Entry list sorted indicator');
  String get sortedZToA => Intl.message('(Z->A)',
      name: 'sortedZToA', desc: 'Entry list sorted indicator');

  String get newWord => Intl.message('Neues Wort',
      name: 'newWord', desc: 'Quick Action new word');
  String get backup => Intl.message('Backup erstellen',
      name: 'backup', desc: 'Quick Action backup');

  // TODO: Fix this as soon as intl.plural code works again
  String get wordOne =>
      Intl.message('Wort', name: 'wordOne', desc: 'Word/Words');
  String get wordMany =>
      Intl.message('Wörter', name: 'wordMany', desc: 'Word/Words');

  String get entryOne =>
      Intl.message('Eintrag', name: 'entryOne', desc: 'Entry/Entries');
  String get entryMany =>
      Intl.message('Einträge', name: 'entryMany', desc: 'Entry/Entries');

  String get about =>
      Intl.message('Über diese App', name: 'about', desc: 'About');

  String get loading =>
      Intl.message('Wird geladen...', name: 'loading', desc: 'Loading');

  String get discoverWords =>
      Intl.message('Entdecken', name: 'discoverWords', desc: 'Discover words');

  String get version =>
      Intl.message('Version: ', name: 'version', desc: 'Version');
  String get infoAbout =>
      Intl.message('Info über ', name: 'infoAbout', desc: 'Info about');

  String get aboutText => Intl.message('''Wortschatz Copyright (C) 2018-2019;
This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it under certain conditions.''',
      name: 'aboutText', desc: 'About Text');

  String get wordAdded => Intl.message('Wort hinzugefügt',
      name: 'wordAdded', desc: 'A new word has been added');
  String get wordEdited => Intl.message('Wort bearbeitet',
      name: 'wordEdited', desc: 'An existing word has been edited');
  String get wordDeleted => Intl.message('Wort gelöscht',
      name: 'wordDeleted', desc: 'An existing word has been deleted');
  String get tagsModified => Intl.message('Schlagwörter geändert',
      name: 'tagsModified', desc: 'Associated tags have been modified');
  String get wordColorChanged => Intl.message('Farbe geändert',
      name: 'wordColorChanged', desc: 'Color of a word has been changed');
  String get wordFavoriteChanged => Intl.message('Favorit geändert',
      name: 'wordFavoriteChanged',
      desc: 'Favorite state of a word has been changed');

  String get wordAddedDictionary =>
      Intl.message('Wort hinzugefügt (Wörterbuch)',
          name: 'wordAddedDictionary', desc: 'A new word has been added');
  String get wordEditedDictionary =>
      Intl.message('Wort bearbeitet (Wörterbuch)',
          name: 'wordEditedDictionary',
          desc: 'An existing word has been edited');
  String get wordDeletedDictionary => Intl.message('Wort gelöscht (Wörterbuch)',
      name: 'wordDeletedDictionary', desc: 'An existing word has been deleted');
  String get tagsModifiedDictionary =>
      Intl.message('Schlagwörter geändert (Wörterbuch)',
          name: 'tagsModifiedDictionary',
          desc: 'Associated tags have been modified');
  String get wordColorChangedDictionary =>
      Intl.message('Farbe geändert (Wörterbuch)',
          name: 'wordColorChangedDictionary',
          desc: 'Color of a word has been changed');
  String get wordFavoriteChangedDictionary =>
      Intl.message('Favorit geändert (Wörterbuch)',
          name: 'wordFavoriteChangedDictionary',
          desc: 'Favorite state of a word has been changed');

  String get tagAdded => Intl.message('Schlagwort hinzugefügt',
      name: 'tagAdded', desc: 'A new tag has been added');
  String get tagEdited => Intl.message('Schlagwort bearbeitet',
      name: 'tagEdited', desc: 'An existing tag has been edited');
  String get tagDeleted => Intl.message('Schlagwort gelöscht',
      name: 'tagDeleted', desc: 'An existing tag has been deleted');
  String get tagColorChanged => Intl.message('Farbe geändert',
      name: 'tagColorChanged', desc: 'Color of a tag has been changed');
  String get tagFavoriteChanged => Intl.message('Favorit geändert',
      name: 'tagFavoriteChanged',
      desc: 'Favorite state of a tag has been changed');

  String get tagAssociated => Intl.message('Schlagwort hinzugefügt',
      name: 'tagAssociated', desc: 'A tag has been associated to a word');
  String get tagUnAssociated => Intl.message('Schlagwort entfernt',
      name: 'tagUnAssociated', desc: 'A tag has been un-associated of a word');

  String get invalidDatabaseState =>
      Intl.message('Zustand der Datenbank ungültig',
          name: 'invalidDatabaseState', desc: 'Invalid database state');

  String get action => Intl.message('Action: ', name: 'action', desc: 'action');

  String get enableDarkMode => Intl.message('Dunkles UI-Design aktivieren',
      name: 'enableDarkMode', desc: 'Enable dark UI mode');
  String get restartRequired =>
      Intl.message('Erfordert möglicherweise einen Neustart der App',
          name: 'restartRequired', desc: 'Restart maybe required');

  String get enterWord => Intl.message('Wort eingeben...',
      name: 'enterWord', desc: 'Hint text: Enter word');
  String get enterTag => Intl.message('Schlagwort eingeben...',
      name: 'enterTag', desc: 'Hint text: Enter tag');

  String get properties =>
      Intl.message('Eigenschaften', name: 'properties', desc: 'Properties');

  String get dates => Intl.message('Daten', name: 'dates', desc: 'Daten');
  String get created =>
      Intl.message('Erzeugt:', name: 'created', desc: 'Created');
  String get modified =>
      Intl.message('Geändert:', name: 'modified', desc: 'Modified');

  String get tags => Intl.message('Schlagwörter', name: 'tags', desc: 'Tags');

  String get filterWords =>
      Intl.message('Wörter filtern', name: 'filterWords', desc: 'Filter words');

  String get selectColorHeading => Intl.message('Farbe wählen',
      name: 'selectColorHeading', desc: 'Select color heading');

  String get filterTagsHeading => Intl.message('Nach Schlagwort filtern',
      name: 'filterTagsHeading', desc: 'Filter tags heading');

  String get filterTags => Intl.message('Schlagwörter filtern',
      name: 'filterTags', desc: 'Filter tags');

  String get filterHistory => Intl.message('Historie filtern',
      name: 'filterHistory', desc: 'Filter history');

  String get filterWordsHint => Intl.message('Wörter filtern...',
      name: 'filterWordsHint', desc: 'Hint text: Filter by word');

  String get filterTagsHint => Intl.message('Schlagwörter filtern...',
      name: 'filterTagsHint', desc: 'Hint text: Filter by tag');

  String get filterHistoryHint => Intl.message('Historie filtern...',
      name: 'filterHistoryHint', desc: 'Hint text: Filter history');

  String get filter =>
      Intl.message('Filtern', name: 'filter', desc: 'Button label: Filter');

  String get save =>
      Intl.message('Speichern', name: 'save', desc: 'Button label: Save');
}

class CustomLocalizationsDelegate
    extends LocalizationsDelegate<CustomLocalizations> {
  const CustomLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['de', 'en'].contains(locale.languageCode);

  @override
  Future<CustomLocalizations> load(Locale locale) =>
      CustomLocalizations.load(locale);

  @override
  bool shouldReload(CustomLocalizationsDelegate old) => false;
}
