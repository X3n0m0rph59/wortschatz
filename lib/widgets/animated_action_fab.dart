/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';

/// An animated floating action button
class AnimatedActionFab extends StatefulWidget {
  final VoidCallback onClick;

  const AnimatedActionFab({
    Key key,
    this.onClick,
  }) : super(key: key);

  @override
  AnimatedActionFabState createState() => AnimatedActionFabState();
}

class AnimatedActionFabState extends State<AnimatedActionFab>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  CurvedAnimation _curvedAnimation;
  Animation<Color> _colorAnimation;

  final double expandedSize = 180.0;
  final double hiddenSize = 20.0;

  VoidCallback onClick;

  AnimatedActionFabState();

  @override
  void initState() {
    super.initState();

    onClick = widget.onClick;

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));

    _curvedAnimation = CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeInOut,
    );

    _colorAnimation = ColorTween(begin: Colors.pink, end: Colors.pink[800])
        .animate(_animationController);
  }

  @override
  void dispose() {
    _animationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: expandedSize,
      height: expandedSize,
      child: AnimatedBuilder(
        animation: _curvedAnimation,
        builder: (BuildContext context, Widget child) {
          return Stack(
            alignment: Alignment.center,
            children: <Widget>[
              _buildFabCore(),
            ],
          );
        },
      ),
    );
  }

  Widget _buildFabCore() {
    double scaleFactor = 2 * (_curvedAnimation.value - 0.5).abs();
    return FloatingActionButton(
      heroTag: null,
      onPressed: onClick,
      child: Transform(
        alignment: Alignment.center,
        transform: Matrix4.identity()..scale(1.0, scaleFactor),
        child: Icon(
          _curvedAnimation.value > 0.5 ? Icons.close : Icons.add,
          color: Colors.white,
          size: 26.0,
        ),
      ),
      backgroundColor: _colorAnimation.value,
    );
  }
}
