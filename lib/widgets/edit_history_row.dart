/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wortschatz/data/edit_history.dart';
import 'package:wortschatz/data/edit_history_entry.dart';
import 'package:wortschatz/util/date_time.dart';

import 'marquee_widget.dart';

/// Implements a data row, that displays an [EditHistoryEntry] data set.
/// Suitable for usage in a ListView or [CustomAnimatedList]
class EditHistoryRow extends StatelessWidget {
  final EditHistoryEntry entry;
  final double dotSize = 24.0;
  final Animation<double> animation;

  final onTap;
  final onLongPress;
  final onUndoAction;

  const EditHistoryRow({
    Key key,
    @required this.entry,
    @required this.animation,
    this.onTap,
    this.onLongPress,
    this.onUndoAction,
  })  : assert(animation != null),
        super(key: key);

  factory EditHistoryRow.waiting({
    Key key,
    @required animation,
    onTap,
    onLongPress,
  }) =>
      EditHistoryRow(
        key: key,
        entry: null,
        animation: animation,
        onTap: onTap,
        onLongPress: onLongPress,
      );

  @override
  Widget build(BuildContext context) {
    if (entry == null) {
      return SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {},
            onLongPress: () {},
            splashColor: Theme.of(context).splashColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: (32.0 - dotSize / 2) + 1),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey,
                      highlightColor: Colors.white,
                      child: Container(
                        height: dotSize,
                        width: dotSize,
                        child: Icon(
                          FontAwesomeIcons.history,
                          color: Colors.white30,
                          size: dotSize,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: SizedBox(
                            width: 250,
                            height: 10,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey,
                              highlightColor: Colors.white,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white30,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: SizedBox(
                            width: 250,
                            height: 4,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey,
                              highlightColor: Colors.white,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white30,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      var actions = <Widget>[];

      var secondaryActions = <Widget>[
//              new IconSlideAction(
//                caption: 'Mehr',
//                color: Colors.black45,
//                icon: Icons.more_horiz,
//                onTap: () => _showMessageBar('Mehr', context),
//              ),
      ];

      if (onUndoAction != null)
        secondaryActions.add(IconSlideAction(
          caption: 'Rückgängig',
          color: Colors.indigoAccent,
          icon: FontAwesomeIcons.undo,
          onTap: () => onUndoAction(entry),
        ));

      return SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Slidable(
          actionPane: new SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => onTap(entry),
              onLongPress: () => onLongPress(entry),
              splashColor: Theme.of(context).splashColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {},
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: (32.0 - dotSize / 2) + 1),
                        child: Container(
                          height: dotSize,
                          width: dotSize,
                          child: Icon(
                            FontAwesomeIcons.history,
                            color: EditHistoryAction(entry.action, context)
                                .toColor(),
                            size: dotSize,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 4, left: 0),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                EditHistoryAction(entry.action, context)
                                    .toString(),
                                style: TextStyle(fontSize: 18.0),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, left: 0),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                formatDateTime(entry.createdAt),
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.grey),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, left: 0),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                entry.description,
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.grey),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          actions: actions,
          secondaryActions: secondaryActions,
        ),
      );
    }
  }

//  void _showMessageBar(String s, context) {
//    MessageBar.error(title: 'Aktion nicht implementiert', message: s)
//        ..show(context);
//  }
}
