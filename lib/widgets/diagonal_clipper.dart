/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';

/// CustomClipper that implements diagonal clipping.
/// This is used to clip the header image on most of the apps's pages,
/// when the content view has been scrolled down.
class DiagonalClipper extends CustomClipper<Path> {
  final double height;

  DiagonalClipper({this.height: 0.0});

  @override
  Path getClip(Size size) {
    Path path = Path();

    // Diagonal clipping
    path.lineTo(0.0, size.height - height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
