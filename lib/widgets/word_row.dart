/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/data/word_entry.dart';
import 'package:wortschatz/exceptions/intent_launch_error.dart';
import 'package:wortschatz/exceptions/share_error.dart';
import 'package:wortschatz/widgets/favorite_widget.dart';
import 'package:wortschatz/widgets/message_bar.dart';

import 'marquee_widget.dart';

/// Implements a data row, that displays a [WordEntry] data set.
/// Suitable for usage in a ListView or [CustomAnimatedList]
class WordRow extends StatelessWidget {
  final WordEntry word;
  final double dotSize = 22.0;
  final Animation<double> animation;

  final onTap;
  final onLongPress;
  final onEditAction;
  final onEditTagsAction;
  final onArchiveAction;
  final onDeleteAction;
  final onFavoriteAction;
  final onChangeColorAction;

  const WordRow({
    Key key,
    @required this.word,
    @required this.animation,
    this.onTap,
    this.onLongPress,
    this.onEditAction,
    this.onEditTagsAction,
    this.onArchiveAction,
    this.onDeleteAction,
    this.onFavoriteAction,
    this.onChangeColorAction,
  })  : assert(animation != null),
        super(key: key);

  factory WordRow.waiting(
          {Key key,
          @required animation,
          onTap,
          onLongPress,
          onEditAction,
          onEditTagsAction,
          onArchiveAction,
          onDeleteAction,
          onFavoriteAction,
          onChangeColorAction}) =>
      WordRow(
        key: key,
        word: null,
        animation: animation,
        onTap: onTap,
        onLongPress: onLongPress,
        onEditAction: onEditAction,
        onEditTagsAction: onEditTagsAction,
        onArchiveAction: onArchiveAction,
        onDeleteAction: onDeleteAction,
        onFavoriteAction: onFavoriteAction,
        onChangeColorAction: onChangeColorAction,
      );

  @override
  Widget build(BuildContext context) {
    if (word == null) {
      return SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {},
            onLongPress: () {},
            splashColor: Theme.of(context).splashColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: (32.0 - dotSize / 2) + 1),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey,
                      highlightColor: Colors.white,
                      child: Container(
                        height: dotSize,
                        width: dotSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.grey),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0, top: 4),
                          child: SizedBox(
                            width: 250,
                            height: 18,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey,
                              highlightColor: Colors.white,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white30,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0, top: 4),
                          child: SizedBox(
                            width: 250,
                            height: 12,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey,
                              highlightColor: Colors.white,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white30,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 12.0),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey,
                      highlightColor: Colors.white,
                      child: FavoriteWidget(
                        favorite: true,
                        enabled: false,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      var actions = <Widget>[
        IconSlideAction(
          caption: 'Suchen',
          color: Colors.green,
          icon: FontAwesomeIcons.search,
          onTap: () => _searchWord(word, context),
        ),
        IconSlideAction(
          caption: 'Teilen',
          color: Colors.indigo,
          icon: FontAwesomeIcons.shareAlt,
          onTap: () => _shareWord(word, context),
        ),
      ];

      if (onEditAction != null)
        actions.add(IconSlideAction(
          caption: 'Bearbeiten',
          color: Colors.blue,
          icon: FontAwesomeIcons.edit,
          onTap: () => onEditAction(word),
        ));

      var secondaryActions = <Widget>[
//              new IconSlideAction(
//                caption: 'Mehr',
//                color: Colors.black45,
//                icon: Icons.more_horiz,
//                onTap: () => _showMessageBar('Mehr', context),
//              ),
      ];

      if (onEditTagsAction != null)
        secondaryActions.add(IconSlideAction(
          caption: 'Schlagwörter',
          color: Colors.indigo,
          icon: FontAwesomeIcons.tags,
          onTap: () async => await onEditTagsAction(word),
        ));

//      if (onArchiveAction != null)
//        secondaryActions.add(IconSlideAction(
//          caption: 'Archiv',
//          color: Colors.blue,
//          icon: Icons.archive,
//          onTap: () => _showMessageBar('Archiv', context),
//        ));

      if (onDeleteAction != null)
        secondaryActions.add(IconSlideAction(
          caption: 'Löschen',
          color: Colors.red,
          icon: FontAwesomeIcons.trash,
          onTap: () => onDeleteAction(word),
        ));

      final tagsList = buildTagsList(word);

      return SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => onTap(word),
              onLongPress: () => onLongPress(word),
              splashColor: Theme.of(context).splashColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => onChangeColorAction(word),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: (32.0 - dotSize / 2) + 1),
                        child: Container(
                          height: dotSize,
                          width: dotSize,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: word.color),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 0.0),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                word.word,
                                style: TextStyle(fontSize: 18.0),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 0.0, top: 4),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                word.description,
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.grey),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => onEditTagsAction(word),
                            child: MarqueeWidget(
                                      child: Row(
                                        children: tagsList,
                                      ),
                                    ),
                            ),
                          ],
                      ),
                    ),
//                  Padding(
//                    padding: const EdgeInsets.only(right: 16.0),
//                    child: Text(
//                      status,
//                      style: TextStyle(fontSize: 12.0, color: Colors.grey),
//                    ),
//                  ),
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: FavoriteWidget(
                        favorite: word.favorite,
                        onTap: () => onFavoriteAction(word),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          actions: actions,
          secondaryActions: secondaryActions,
        ),
      );
    }
  }

  void _searchWord(WordEntry word, BuildContext context) async {
    final url = Uri.encodeFull('https://www.google.com/search?q=${word.word}');

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      MessageBar.error(
          title: 'Fehler',
          message: 'Browser Intent konnte nicht gestartet werden')
        ..show(context);

      throw IntentLaunchError('Could not launch url: "$url"');
    }
  }

  void _shareWord(WordEntry word, BuildContext context) async {
    try {
      await Share.share('Ein Wort aus meinem Wortschatz: ${word.word}');
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
        ..show(context);

      throw ShareError('Could not share data');
    }
  }

  List<Widget> buildTagsList(WordEntry word) {
    return <Widget>[
      FutureBuilder<List<TagEntry>>(
        future: word.tags,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            final len = snapshot.data.length;
            if (len > 0) {
              var tagsList = <Widget>[];

              snapshot.data.forEach((t) {
                tagsList.add(
                  Icon(
                    FontAwesomeIcons.tag,
                    size: 8,
                    color: Colors.grey,
                  ),
                );
                tagsList.add(
                  Padding(
                    padding: const EdgeInsets.only(left: 2, right: 4),
                    child: Text(
                      '${t.tag}',
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontWeight:
                            t.favorite ? FontWeight.bold : FontWeight.normal,
                        fontSize: 12.0,
                        color: t.color,
                      ),
                    ),
                  ),
                );
              });

              return Padding(
                padding: EdgeInsets.only(top: 4, left: 0),
                child: Row(
                  children: tagsList,
                ),
              );
            } else {
              return Container();
//              return Padding(
//                padding: EdgeInsets.only(top: 4, left: 0),
//                child: Row(
//                  children: <Widget>[
//                    Text(
//                      'Keine Schlagwörter verknüpft',
//                      style: TextStyle(
//                        fontStyle: FontStyle.italic,
//                        fontSize: 12.0,
//                        color: Colors.grey,
//                      ),
//                    ),
//                  ],
//                ),
//              );
            }
          } else {
            return Container(
              width: 0,
              height: 0,
            );
          }
        },
      ),
    ];
  }
}
