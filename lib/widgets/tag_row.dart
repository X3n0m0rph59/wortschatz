/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wortschatz/data/tag_entry.dart';
import 'package:wortschatz/exceptions/intent_launch_error.dart';
import 'package:wortschatz/exceptions/share_error.dart';
import 'package:wortschatz/widgets/favorite_widget.dart';
import 'package:wortschatz/widgets/message_bar.dart';

import 'marquee_widget.dart';

/// Implements a data row, that displays a [TagEntry] data set.
/// Suitable for usage in a ListView or [CustomAnimatedList]
class TagRow extends StatelessWidget {
  final TagEntry tag;
  final double dotSize = 24.0;
  final Animation<double> animation;

  final onTap;
  final onLongPress;
  final onEditAction;
  final onArchiveAction;
  final onDeleteAction;
  final onFavoriteAction;
  final onChangeColorAction;

  const TagRow({
    Key key,
    @required this.tag,
    @required this.animation,
    this.onTap,
    this.onLongPress,
    this.onEditAction,
    this.onArchiveAction,
    this.onDeleteAction,
    this.onFavoriteAction,
    this.onChangeColorAction,
  })  : assert(animation != null),
        super(key: key);

  factory TagRow.waiting(
          {Key key,
          @required animation,
          onTap,
          onLongPress,
          onEditAction,
          onArchiveAction,
          onDeleteAction,
          onFavoriteAction,
          onChangeColorAction}) =>
      TagRow(
        key: key,
        tag: null,
        animation: animation,
        onTap: onTap,
        onLongPress: onLongPress,
        onEditAction: onEditAction,
        onArchiveAction: onArchiveAction,
        onDeleteAction: onDeleteAction,
        onFavoriteAction: onFavoriteAction,
        onChangeColorAction: onChangeColorAction,
      );

  @override
  Widget build(BuildContext context) {
    if (tag == null) {
      return SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {},
            onLongPress: () {},
            splashColor: Theme.of(context).splashColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: (32.0 - dotSize / 2) + 1),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey,
                      highlightColor: Colors.white,
                      child: Container(
                        height: dotSize,
                        width: dotSize,
                        child: Icon(
                          FontAwesomeIcons.tag,
                          color: Colors.white30,
                          size: dotSize,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: SizedBox(
                            width: 250,
                            height: 10,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey,
                              highlightColor: Colors.white,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white30,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: SizedBox(
                            width: 250,
                            height: 4,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey,
                              highlightColor: Colors.white,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.white30,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 12.0),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey,
                      highlightColor: Colors.white,
                      child: FavoriteWidget(
                        favorite: true,
                        enabled: false,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      var actions = <Widget>[
        IconSlideAction(
          caption: 'Suchen',
          color: Colors.green,
          icon: FontAwesomeIcons.search,
          onTap: () => _searchTag(tag, context),
        ),
        IconSlideAction(
          caption: 'Teilen',
          color: Colors.indigo,
          icon: FontAwesomeIcons.shareAlt,
          onTap: () => _shareTag(tag, context),
        ),
      ];

      if (onEditAction != null)
        actions.add(IconSlideAction(
          caption: 'Bearbeiten',
          color: Colors.blue,
          icon: FontAwesomeIcons.edit,
          onTap: () => onEditAction(tag),
        ));

      var secondaryActions = <Widget>[
//              new IconSlideAction(
//                caption: 'Mehr',
//                color: Colors.black45,
//                icon: Icons.more_horiz,
//                onTap: () => _showMessageBar('Mehr', context),
//              ),
      ];

//      if (onArchiveAction != null)
//        secondaryActions.add(IconSlideAction(
//          caption: 'Archiv',
//          color: Colors.blue,
//          icon: Icons.archive,
//          onTap: () => _showMessageBar('Archiv', context),
//        ));

      if (onDeleteAction != null)
        secondaryActions.add(IconSlideAction(
          caption: 'Löschen',
          color: Colors.red,
          icon: FontAwesomeIcons.trash,
          onTap: () => onDeleteAction(tag),
        ));

      return SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Slidable(
          actionPane: new SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => onTap(tag),
              onLongPress: () => onLongPress(tag),
              splashColor: Theme.of(context).splashColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => onChangeColorAction(tag),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: (32.0 - dotSize / 2) + 1),
                        child: Container(
                          height: dotSize,
                          width: dotSize,
                          child: Icon(
                            FontAwesomeIcons.tag,
                            color: tag.color,
                            size: dotSize,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 4, left: 0),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                tag.tag,
                                style: TextStyle(fontSize: 18.0),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, left: 0),
                            child: MarqueeWidget(
                              child: AutoSizeText(
                                tag.description,
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.grey),
                                minFontSize: 8.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: FavoriteWidget(
                        favorite: tag.favorite,
                        onTap: () => onFavoriteAction(tag),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          actions: actions,
          secondaryActions: secondaryActions,
        ),
      );
    }
  }
//
//  void _showMessageBar(String s, context) {
//    MessageBar.error(title: 'Aktion nicht implementiert', message: s)
//        ..show(context);
//  }

  void _searchTag(TagEntry tag, BuildContext context) async {
    final url = Uri.encodeFull('https://www.google.com/search?q=${tag.tag}');

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      MessageBar.error(
          title: 'Fehler',
          message: 'Browser Intent konnte nicht gestartet werden')
        ..show(context);

      throw IntentLaunchError('Could not launch url: "$url"');
    }
  }

  void _shareTag(TagEntry tag, BuildContext context) async {
    try {
      await Share.share('Ein Schlagwort aus meinem Wortschatz: ${tag.tag}');
    } catch (e) {
      MessageBar.error(
          title: 'Fehler', message: 'Konnte den Inhalt nicht teilen')
        ..show(context);

      throw ShareError('Could not share data');
    }
  }
}
