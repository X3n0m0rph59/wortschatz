/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:wortschatz/config.dart';

/// A custom Button convenience class, based on MaterialButton
/// with support for icons
class CustomIconButton extends StatelessWidget {
  final Key key;
  final icon;
  final Widget child;
  final Color color;
  final onPressed;
  final padding;

  CustomIconButton({
    this.key,
    this.icon,
    this.child,
    this.color,
    this.onPressed,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: Config.defaultButtonHeight,
      color: color == null ? Theme.of(context).primaryColor : color,
      textColor: Theme.of(context).primaryTextTheme.button.color,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Icon(
              icon,
              color: Theme.of(context).primaryTextTheme.button.color,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: padding == null
                  ? const EdgeInsets.only(left: 18, top: 4)
                  : padding,
              child: child,
            ),
          ),
        ],
      ),
      onPressed: onPressed,
    );
  }
}
