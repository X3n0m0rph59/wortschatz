/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Convenience wrapper around the 'Flushbar' snack bar plugin
class MessageBar<T extends Object> {
  Flushbar<T> flushbar;

  String title;
  String message;
  Widget icon;
  Color color;
  Duration duration;

  VoidCallback onAction;
  Widget actionChild;

  bool showProgressIndicator;

  AnimationController controller;

  MessageBar({
    this.title,
    this.message,
    this.icon,
    this.color,
    this.duration,
    this.onAction,
    this.actionChild,
    this.showProgressIndicator,
    this.controller,
  }) {
    flushbar = Flushbar(
      title: title,
      message: message,
      icon: icon,
      backgroundColor: color,
      duration: duration,
      mainButton: onAction != null
          ? FlatButton(onPressed: () => onAction(), child: actionChild)
          : null,
      showProgressIndicator: showProgressIndicator ?? false,
      progressIndicatorController: controller,
    );
  }

  factory MessageBar.info({
    title,
    message,
    duration,
  }) {
    return MessageBar(
      title: title,
      message: message,
      icon: Icon(Icons.info_outline),
      color: Colors.green,
      duration: duration,
    );
  }

  factory MessageBar.infoWithAction({
    title,
    message,
    duration,
    onAction,
    actionChild,
  }) {
    return MessageBar(
      title: title,
      message: message,
      icon: Icon(Icons.info_outline),
      color: Colors.green,
      duration: duration,
      onAction: onAction,
      actionChild: actionChild,
    );
  }

  factory MessageBar.infoWithProgress({
    title,
    message,
    onAction,
    actionChild,
    controller,
  }) {
    return MessageBar(
      title: title,
      message: message,
      icon: Icon(Icons.info_outline),
      color: Colors.green,
      onAction: onAction,
      actionChild: actionChild,
      showProgressIndicator: true,
      controller: controller,
    );
  }

  factory MessageBar.warning({title, message, duration}) {
    return MessageBar(
      title: title,
      message: message,
      icon: Icon(Icons.warning),
      color: Colors.orange,
      duration: duration,
    );
  }

  factory MessageBar.error({
    title,
    message,
    duration,
  }) {
    return MessageBar(
      title: title,
      message: message,
      icon: Icon(Icons.error),
      color: Colors.red,
      duration: duration,
    );
  }

  Future<T> show(BuildContext context) async {
    return flushbar.show(context);
  }

  Future<T> dismiss([T result]) async {
    return flushbar.dismiss(result);
  }
}
