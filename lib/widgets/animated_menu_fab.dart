/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:math' as math;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/// An animated floating action button with support for multiple
/// pre-defined actions
class AnimatedMenuFab extends StatefulWidget {
  final bool isOpened;

  final VoidCallback onClick;
  final VoidCallback onFavoritesClick;
  final VoidCallback onAddClick;
  final VoidCallback onSortClick;
  final VoidCallback onFilterTagsClick;
  final VoidCallback onSearchClick;
  final VoidCallback onSpecialClick;
  final VoidCallback onReloadClick;

  const AnimatedMenuFab({
    Key key,
    this.isOpened,
    this.onClick,
    this.onFavoritesClick,
    this.onAddClick,
    this.onSortClick,
    this.onFilterTagsClick,
    this.onSearchClick,
    this.onSpecialClick,
    this.onReloadClick,
  }) : super(key: key);

  @override
  AnimatedMenuFabState createState() => AnimatedMenuFabState();
}

class AnimatedMenuFabState extends State<AnimatedMenuFab>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  CurvedAnimation _curvedAnimation;
  Animation<Color> _colorAnimation;

  final double expandedSize = 180.0;
  final double hiddenSize = 20.0;

  bool isOpened;
  VoidCallback onFavoritesClick;
  VoidCallback onAddClick;
  VoidCallback onSortClick;
  VoidCallback onFilterTagsClick;
  VoidCallback onSearchClick;
  VoidCallback onSpecialClick;
  VoidCallback onReloadClick;

  AnimatedMenuFabState();

  @override
  void initState() {
    super.initState();

    onFavoritesClick = widget.onFavoritesClick;
    onAddClick = widget.onAddClick;
    onSortClick = widget.onSortClick;
    onFilterTagsClick = widget.onFilterTagsClick;
    onSearchClick = widget.onSearchClick;
    onSpecialClick = widget.onSpecialClick;
    onReloadClick = widget.onReloadClick;

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));

    _curvedAnimation = CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeInOut,
    );

    _colorAnimation = ColorTween(begin: Colors.pink, end: Colors.pink[800])
        .animate(_animationController);

    isOpened = widget.isOpened;
    isOpened ??= false;

    if (!isOpened) {
      close();
    } else if (isOpened) {
      open();
    }
  }

  @override
  void dispose() {
    _animationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: expandedSize,
      height: expandedSize,
      child: AnimatedBuilder(
        animation: _curvedAnimation,
        builder: (BuildContext context, Widget child) {
          return Stack(
            alignment: Alignment.center,
            children: <Widget>[
              _buildExpandedBackground(),
              _buildOption(FontAwesomeIcons.solidStar, 0.0, onFavoritesClick),
              _buildOption(
                  FontAwesomeIcons.plusCircle, -math.pi / 3, onAddClick),
              _buildOption(
                  FontAwesomeIcons.sortAlphaDown, -math.pi / 3, onSortClick),
              _buildOption(
                  FontAwesomeIcons.tags, -2 * math.pi / 3, onFilterTagsClick),
              _buildOption(FontAwesomeIcons.sync, math.pi, onReloadClick),
              _buildOption(FontAwesomeIcons.search, math.pi, onSearchClick),
              _buildOption(FontAwesomeIcons.angleDoubleUp, 4.5 * math.pi / 6,
                  onSpecialClick),
              _buildFabCore(),
            ],
          );
        },
      ),
    );
  }

  Widget _buildOption(IconData icon, double angle, VoidCallback callback) {
    if (callback == null) {
      return Container();
    }

    if (_animationController.isDismissed) {
      return Container();
    }

    double iconSize = 0.0;
    if (_curvedAnimation.value > 0.8) {
      iconSize = max(0, 26.0 * (_animationController.value - 0.8) * 5);
    }

    return Transform.rotate(
      angle: angle,
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: IconButton(
            onPressed: () {
              callback();
              close();
            },
            icon: Transform.rotate(
              angle: -angle,
              child: Icon(
                icon,
                color: Colors.white,
              ),
            ),
            iconSize: iconSize,
            alignment: Alignment.center,
            padding: EdgeInsets.all(0.0),
          ),
        ),
      ),
    );
  }

  Widget _buildExpandedBackground() {
    double size =
        hiddenSize + (expandedSize - hiddenSize) * _curvedAnimation.value;
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.pink),
    );
  }

  Widget _buildFabCore() {
    double scaleFactor = 2 * (_curvedAnimation.value - 0.5).abs();
    return FloatingActionButton(
      heroTag: null,
      onPressed: _onFabTap,
      child: Transform(
        alignment: Alignment.center,
        transform: Matrix4.identity()..scale(1.0, scaleFactor),
        child: Icon(
          _curvedAnimation.value > 0.5 ? Icons.close : Icons.filter_list,
          color: Colors.white,
          size: 26.0,
        ),
      ),
      backgroundColor: _colorAnimation.value,
    );
  }

  open() {
    if (_animationController.isDismissed) {
      _animationController.forward();
    }
  }

  close() {
    if (_animationController.isCompleted) {
      _animationController.reverse();
    }
  }

  _onFabTap() {
    if (_animationController.isDismissed) {
      open();
    } else {
      close();
    }
  }
}
