/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/widgets.dart';

class MarqueeWidget extends StatefulWidget {
  final Widget child;
  final Axis direction;
  final Duration animationDuration, backDuration, pauseDuration;

  MarqueeWidget({
    @required this.child,
    this.direction: Axis.horizontal,
    this.animationDuration: const Duration(milliseconds: 3500),
    this.backDuration: const Duration(milliseconds: 1600),
    this.pauseDuration: const Duration(milliseconds: 1600),
  });

  @override
  _MarqueeWidgetState createState() => _MarqueeWidgetState();
}

class _MarqueeWidgetState extends State<MarqueeWidget> {
  ScrollController scrollController =
      ScrollController(initialScrollOffset: 0.0, keepScrollOffset: false);

  @override
  void initState() {
    super.initState();

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    scroll();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: widget.child,
      scrollDirection: widget.direction,
      controller: scrollController,
      physics: NoImplicitScrollPhysics(),
    );
  }

  void scroll() async {
    while (true) {
      if (scrollController.hasClients) {
        await Future.delayed(widget.pauseDuration);

        try {
          await scrollController.animateTo(
              scrollController?.position?.maxScrollExtent ?? 0,
              duration: widget.animationDuration,
              curve: Curves.easeIn);
        } catch (e) {
          // eat exceptions
        }

        await Future.delayed(widget.pauseDuration);

        try {
          await scrollController.animateTo(0.0,
              duration: widget.backDuration, curve: Curves.easeOut);
        } catch (e) {
          // eat exceptions
        }
      } else {
        await Future.delayed(widget.pauseDuration);
      }
    }
  }
}

class NoImplicitScrollPhysics extends AlwaysScrollableScrollPhysics {
  @override
  bool get allowImplicitScrolling => false;
}
