/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef bool BoolCallback();

/// Implements a favorite indicator Widget
class FavoriteWidget extends StatefulWidget {
  final bool favorite;
  final bool enabled;
  final BoolCallback onTap;

  FavoriteWidget({
    @required this.favorite,
    this.enabled: true,
    this.onTap,
  });

  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool favorite;
  bool enabled;
  BoolCallback onTap;

  @override
  void initState() {
    super.initState();

    favorite = widget.favorite;
    enabled = widget.enabled;
    onTap = widget.onTap;

    initAsyncState();
  }

  Future<void> initAsyncState() async {
    return Future.value();
  }

  void _toggleFavorite() {
    if (enabled) {
      setState(() {
        if (onTap != null) {
          favorite = onTap();
        } else {
          favorite = !favorite;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0.0),
          child: IconButton(
            icon: (favorite ? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Colors.orange,
            onPressed: _toggleFavorite,
          ),
        ),
      ],
    );
  }
}
