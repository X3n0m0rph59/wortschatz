/*
 * This file is part of Wortschatz.
 * Wortschatz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wortschatz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wortschatz.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wortschatz/config.dart';
import 'package:wortschatz/pages/about_page.dart';
import 'package:wortschatz/pages/database_selector_page.dart';
import 'package:wortschatz/pages/dictionary_page.dart';
import 'package:wortschatz/pages/discover_page.dart';
import 'package:wortschatz/pages/edit_history_page.dart';
import 'package:wortschatz/pages/main_page.dart';
import 'package:wortschatz/pages/onboarding_page.dart';
import 'package:wortschatz/pages/preferences_page.dart';
import 'package:wortschatz/pages/spell_check_page.dart';
import 'package:wortschatz/pages/statistics_page.dart';
import 'package:wortschatz/pages/tags_page.dart';
import 'package:wortschatz/pages/tools_page.dart';
import 'package:wortschatz/pages/user_profile_page.dart';
import 'package:wortschatz/util/l10n.dart';

enum DrawerItemIndex {
  mainPage,
  tagsPage,
  dictionaryPage,
  spellCheckPage,
  queryPage,
  discoverPage,
  problemWordsPage,
  statisticsPage,
  profilePage,
  editHistoryPage,
  toolsPage,
  databaseSelectorPage,
  preferencesPage,
  helpPage,
  aboutPage,
}

/// Build an App specific drawer Widget
Widget buildDrawer(BuildContext context, [DrawerItemIndex selIndex]) {
  final Color activeColor = Colors.pinkAccent[400];
  final Color inactiveColor = Theme.of(context).primaryColorDark;

  final Color activeIconColor = Colors.pinkAccent[400];
  final Color inactiveIconColor =
      Theme.of(context).brightness == Brightness.dark
          ? Colors.grey[300]
          : Colors.grey[600];

  final TextStyle activeTextStyle = TextStyle(
    color: activeColor,
    fontWeight: FontWeight.bold,
//    shadows: <Shadow>[
//      Shadow(color: Colors.grey, blurRadius: 3, offset: Offset(2, 2)),
//    ],
  );

  final TextStyle inactiveTextStyle = TextStyle(color: inactiveColor);

  final String assetName = Theme.of(context).brightness == Brightness.dark
      ? Config.assetsHeaderDark
      : Config.assetsHeaderLight;

  return Drawer(
    child: Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        DrawerHeader(
          child: Row(
            children: <Widget>[
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Icon(
//                  FontAwesomeIcons.home,
//                  color: Colors.white,
//                ),
//              ),
              Text(
                CustomLocalizations.of(context).drawerHeader,
                style: TextStyle(
                  fontSize: 38,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  shadows: <Shadow>[
                    Shadow(
                      color: Colors.grey[800],
                      offset: Offset(2, 2),
                      blurRadius: 3,
                    )
                  ],
                ),
              ),
            ],
          ),
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(assetName),
            ),
          ),
        ),
        MediaQuery.removePadding(
          context: context,
          // DrawerHeader consumes top MediaQuery padding.
          removeTop: true,
          child: Expanded(
            child: ListView(
              // padding: const EdgeInsets.only(top: 8.0),
              children: <Widget>[
                Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.home,
                        size: 22,
                        color: selIndex == DrawerItemIndex.mainPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).mainPage,
                        style: selIndex == DrawerItemIndex.mainPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MainPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.tags,
                        size: 22,
                        color: selIndex == DrawerItemIndex.tagsPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).tagsPage,
                        style: selIndex == DrawerItemIndex.tagsPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => TagsPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.book,
                        size: 27,
                        color: selIndex == DrawerItemIndex.dictionaryPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).dictionaryPage,
                        style: selIndex == DrawerItemIndex.dictionaryPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DictionaryPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.spellcheck,
                        size: 27,
                        color: selIndex == DrawerItemIndex.spellCheckPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).spellCheckPage,
                        style: selIndex == DrawerItemIndex.spellCheckPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SpellCheckPage()),
                        );
                      },
                    ),
//                    ListTile(
//                      leading: Icon(
//                        FontAwesomeIcons.questionCircle,
//                        size: 22,
//                        color: selIndex == DrawerItemIndex.queryPage
//                            ? activeIconColor
//                            : inactiveIconColor,
//                      ),
//                      title: Text(
//                        'Abfrage',
//                        style: selIndex == DrawerItemIndex.queryPage
//                            ? activeTextStyle
//                            : inactiveTextStyle,
//                      ),
//                      onTap: () {
//                        Navigator.pop(context);
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(builder: (context) => QueryPage()),
//                        );
//                      },
//                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.fileAlt,
                        size: 22,
                        color: selIndex == DrawerItemIndex.discoverPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).discoverPage,
                        style: selIndex == DrawerItemIndex.discoverPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DiscoverPage()),
                        );
                      },
                    ),
//                    ListTile(
//                      leading: Icon(
//                        FontAwesomeIcons.certificate,
//                        size: 22,
//                        color: selIndex == DrawerItemIndex.problemWordsPage
//                            ? activeIconColor
//                            : inactiveIconColor,
//                      ),
//                      title: Text(
//                        'Problemwörter',
//                        style: selIndex == DrawerItemIndex.problemWordsPage
//                            ? activeTextStyle
//                            : inactiveTextStyle,
//                      ),
//                      onTap: () {
//                        Navigator.pop(context);
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => ProblemWordsPage()),
//                        );
//                      },
//                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.chartBar,
                        size: 22,
                        color: selIndex == DrawerItemIndex.statisticsPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).statisticsPage,
                        style: selIndex == DrawerItemIndex.statisticsPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StatisticsPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.addressCard,
                        size: 22,
                        color: selIndex == DrawerItemIndex.profilePage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).profilePage,
                        style: selIndex == DrawerItemIndex.profilePage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UserProfilePage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.history,
                        size: 22,
                        color: selIndex == DrawerItemIndex.editHistoryPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).editHistoryPage,
                        style: selIndex == DrawerItemIndex.editHistoryPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditHistoryPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.toolbox,
                        size: 22,
                        color: selIndex == DrawerItemIndex.toolsPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).toolsPage,
                        style: selIndex == DrawerItemIndex.toolsPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ToolsPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.database,
                        size: 22,
                        color: selIndex == DrawerItemIndex.databaseSelectorPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).databaseSelectorPage,
                        style: selIndex == DrawerItemIndex.databaseSelectorPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DatabaseSelectorPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.cogs,
                        size: 22,
                        color: selIndex == DrawerItemIndex.preferencesPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).preferencesPage,
                        style: selIndex == DrawerItemIndex.preferencesPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PreferencesPage()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.handsHelping,
                        size: 22,
                        color: selIndex == DrawerItemIndex.helpPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).helpPage,
                        style: selIndex == DrawerItemIndex.helpPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => onboarding(context)),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.infoCircle,
                        size: 22,
                        color: selIndex == DrawerItemIndex.aboutPage
                            ? activeIconColor
                            : inactiveIconColor,
                      ),
                      title: Text(
                        CustomLocalizations.of(context).aboutPage,
                        style: selIndex == DrawerItemIndex.aboutPage
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AboutPage()),
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
